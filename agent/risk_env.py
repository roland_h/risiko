from typing import List
from dataclasses import dataclass, field

import json
import random

from statistics import mean
import numpy as np
import gym


@dataclass
class Area:
  id: str
  idx: int
  continent: str
  owner_idx: int = -1
  owner_idx_init: int = -1
  armies: int = 0

  def __post_init__(self):
    self.owner_idx_init = self.owner_idx
  
  def owner(self, env):
    return env.players[self.owner_idx].id

  def count_enemies(self, env):
    cnt_enemies = 0
    for m in env.moves:
      if m.src_idx == self.idx and \
          env.areas[m.dst_idx].owner_idx != env.areas[self.idx].owner_idx:
        cnt_enemies += env.areas[m.dst_idx].armies
    return cnt_enemies


@dataclass
class Continent:
  id: str
  value: int
  areas: list = field(default_factory=list)

  def is_owned_by(self, player_idx):
    cnt_areas = len(self.areas)
    return cnt_areas > 0 and \
      sum([1 if a.owner_idx == player_idx else 0 for a in self.areas]) == cnt_areas


@dataclass
class Player:
  id: str
  idx: int
  type: str
  reserve: int = 0
  reserve_before_action: int = 0
  cnt_areas: int = 0
  cnt_move: int = 0
  cnt_attack: int = 0
  rewards_total: int = 0
  last_reinforcment: int = 0
  cards: dict = field(default_factory=dict)
  cards_value: int = 0
  cards_value_str: str = ''

  def reset(self):
    self.reserve = 0
    self.reserve_before_action = 0
    self.cnt_move = 0
    self.cnt_attack = 0
    self.rewards_total = 0
    self.last_reinforcment = 0
    self.cards = {}
    self.cards_value = 0
    self.cards_value_str = ''

  def get_reinforcement(self, env):
    reinforcement = max(int(self.cnt_areas / 3), env.reinforcement_min)
    for continent in env.continents.values():
      if continent.is_owned_by(self.id):
        reinforcement += continent.value
    return reinforcement

  def add_reinforcement(self, env):
    self.last_reinforcment = self.get_reinforcement(env)
    self.reserve += self.last_reinforcment

  def add_card(self, env):
    if env.round not in self.cards:
      x = random.random()
      thresh = 0.0
      for card, prob in env.card_probs.items():
        thresh += prob
        if x <= thresh:
          break
      self.cards[env.round] = card
      self.update_cards_value(env)

  def trade_cards(self, env):
    cards_value_ = self.cards_value
    cards_value_str_ = self.cards_value_str[:]
    self.reserve += self.cards_value
    for i in range(3):
      for round, card in self.cards.items():
        if card == cards_value_str_[i]:
          break
      del self.cards[round]
    self.update_cards_value(env)
    return cards_value_str_, cards_value_

  def update_cards_value(self, env):
    if len(self.cards) < 3:
      self.cards_value_str, self.cards_value = '', 0
    else:
      cards_str = "".join(self.cards.values())
      cnt_i = cards_str.count('i')
      cnt_c = cards_str.count('c')
      cnt_a = cards_str.count('a')
      if cnt_i >= 1 and cnt_c >= 1 and cnt_a >= 1:
        self.cards_value_str, self.cards_value = 'ica', env.card_values['ica']
      elif cnt_a >= 3:
        self.cards_value_str, self.cards_value = 'aaa', env.card_values['aaa']
      elif cnt_c >= 3:
        self.cards_value_str, self.cards_value = 'ccc', env.card_values['ccc']
      elif cnt_i >= 3:
        self.cards_value_str, self.cards_value = 'iii', env.card_values['iii']
      else:
        self.cards_value_str, self.cards_value = '', 0


@dataclass
class Reinforce:
  idx: int
  dst_idx: int

  def execute(self, env):
    env.active_player.reserve -= 1
    env.areas[self.dst_idx].armies += 1

  def is_executable(self, env):
    return env.active_player.reserve > 0 and \
           env.areas[self.dst_idx].owner_idx == env.player_active_idx

  def reward(self, env):
    return 1 if env.areas[self.dst_idx].count_enemies(env) > 0 else 0

  def __repr__(self):
    return f"enforce {self.dst_idx}"

  def id(self):
    return f"r{self.dst_idx}"


@dataclass
class Trade:
  idx: int
  cards : str = ""
  value : int = 0

  def execute(self, env):
    self.cards, self.value = env.active_player.trade_cards(env)
  
  def is_executable(self, env):
    return env.active_player.reserve == 0 and \
           env.active_player.cards_value > 0

  def reward(self, env):
    val_min = min(env.card_values.values())
    val_max = max(env.card_values.values())
    return 1+(self.value-val_min)+(val_max-val_min)

  def __repr__(self):
    if self.value > 0:
      return f"trade {self.cards}={self.value}"
    else:
      return "trade"

  def id(self):
    return f"t"


@dataclass
class Move:
  src_id: str
  dst_id: str
  idx: int = -1
  src_idx: int = -1
  dst_idx: int = -1

  def execute(self, env):
    env.areas[self.src_idx].armies -= 1
    env.areas[self.dst_idx].armies += 1
    env.active_player.cnt_move += 1

  def is_executable(self, env):
    return (env.cnt_moves_max < 0 or env.active_player.cnt_move < env.cnt_moves_max) and \
           env.active_player.reserve == 0 and \
           len(env.active_player.cards) < 5 and \
           env.areas[self.src_idx].owner_idx == env.player_active_idx and \
           env.areas[self.dst_idx].owner_idx == env.player_active_idx and \
           env.areas[self.src_idx].armies > 1

  def reward(self, env):
    return 1 if env.areas[self.dst_idx].count_enemies(env) > 0 else 0

  def __repr__(self):
    return f"move {self.dst_idx} < {self.src_idx}"

  def id(self):
    return f"m{self.src_idx}-{self.dst_idx}"


class Attack:
  src_id: str
  dst_id: str
  idx: int = -1
  src_idx: int = -1
  dst_idx: int = -1
  success: bool = False

  def __init__(self, idx, move):
    self.idx = idx
    self.src_id = move.src_id
    self.dst_id = move.dst_id
    self.src_idx = move.src_idx
    self.dst_idx = move.dst_idx

  def execute(self, env):
    area_src = env.areas[self.src_idx]
    area_dst = env.areas[self.dst_idx]
    dices_attack = sorted([random.randrange(6) for _ in range(1, min(area_src.armies, 3))], reverse=True)
    dices_defend = sorted([random.randrange(6) for _ in range(0, min(area_dst.armies, 2))], reverse=True)
    cnt_winners = 0
    cnt_fights = min(len(dices_attack), len(dices_defend))
    assert(cnt_fights > 0)
    for i in range(cnt_fights):
      if dices_attack[i] > dices_defend[i]:
        area_dst.armies -= 1
        cnt_winners += 1
      else:
        area_src.armies -= 1
    if area_dst.armies == 0:
      self.success = True
      area_dst.owner_idx = env.player_active_idx
      area_dst.armies = cnt_winners
      area_src.armies -= cnt_winners
      env.active_player.add_card(env)
    env.active_player.cnt_attack += 1

  def is_executable(self, env):
    return env.active_player.reserve == 0 and \
           env.active_player.cnt_move == 0 and \
           len(env.active_player.cards) < 5 and \
           env.areas[self.src_idx].owner_idx == env.player_active_idx and \
           env.areas[self.dst_idx].owner_idx != env.player_active_idx and \
           env.areas[self.src_idx].armies > 1

  def reward(self, env):
    return 2 if self.success else 0

  def __repr__(self):
    return f"attack {self.dst_idx} < {self.src_idx}"

  def id(self):
    return f"a{self.src_idx}-{self.dst_idx}"


@dataclass
class Pass:
  idx : int

  def execute(self, env):
    env.player_active_idx = (env.player_active_idx+1) % env.cnt_players
    env.active_player.add_reinforcement(env)
    env.active_player.cnt_move = 0
    env.active_player.cnt_attack = 0
    if env.player_active_idx == 0:
      env.round += 1

  def is_executable(self, env):
    return env.active_player.reserve == 0 and \
            len(env.active_player.cards) < 5

  def reward(self, env):
    return 0

  def __repr__(self):
    return "pass"

  def id(self):
    return f"p"


class RiskEnv(gym.Env):

    player_active_idx: int = -1
    last_action = ''
    prev_player = None
    reinforcement_min = 3
    round = 1
    last_round = 0
    last_reward = 0
    cnt_moves_max = 3
    winner = None
    game_over = False


    def __init__(self, game_cfg_filename):
      super(RiskEnv, self).__init__()

      with open(game_cfg_filename) as game_cfg_file:
        #try:
        cfg = json.load(game_cfg_file)
        #except json.JSONDecodeError as e:
        #  print("Error: "+str(e))
        #  raise Exception("Error: "+str(e))

        # read game settings:
        if 'settings' in cfg:
          if 'reinforcement_min' in cfg['settings']:
            self.reinforcement_min = cfg['settings']['reinforcement_min']
          if 'cnt_moves_max' in cfg['settings']:
            self.cnt_moves_max = cfg['settings']['cnt_moves_max']

          if "game_end" in cfg['settings']:
            if "armies_per_country_max" in cfg['settings']['game_end']: 
              self.armies_per_country_max = cfg['settings']['game_end']["armies_per_country_max"]
            if "armies_per_country_min" in cfg['settings']['game_end']: 
              self.armies_per_country_min = cfg['settings']['game_end']["armies_per_country_min"]
            if "rounds_max" in cfg['settings']['game_end']: 
              self.rounds_max = cfg['settings']['game_end']["rounds_max"]
        
        self.card_values = cfg['card_values']
        self.card_probs = {k: float(v)/sum(cfg['card_numbers'].values()) for k,v in cfg['card_numbers'].items()}
        
        # create areas:
        area_ids = sorted(list(cfg['areas'].keys()))

        def neighbours(id):
            return sorted(cfg['areas'][id]['neighbours'])
        self.areas = [Area(id, i, cfg['areas'][id]['continent'])
                      for i, id in enumerate(area_ids)]
        areas_dict = {a.id: a for a in self.areas}
        self.cnt_areas = len(self.areas)

        self.continents = {id: Continent(id, continent['value']) 
                           for id, continent in cfg['continents'].items()}
        for area in self.areas:
            if area.continent in self.continents:
                self.continents[area.continent].areas.append(area)

        # create move actions between connected countries:
        self.moves = [Move(id, n_id)
                      for id in area_ids for n_id in neighbours(id)]
        for i, move in enumerate(self.moves):
          move.idx = i
          move.src_idx = areas_dict[move.src_id].idx
          move.dst_idx = areas_dict[move.dst_id].idx

        # create players
        players_ids = sorted(list(cfg['players'].keys()))
        def get_type(id):
          return cfg['players'][id]['type'] if 'type' in cfg['players'][id] else 'random'
        self.players = [Player(id, i, get_type(id)) 
                        for i, id in enumerate(players_ids)]
        players_dict = {p.id: p for p in self.players}
        self.cnt_players = len(self.players)

        # set pre-defined owners of areas:
        for player_id in players_ids:
          if 'areas' in cfg['players'][player_id]:
            for area_id in cfg['players'][player_id]['areas']:
              if area_id in areas_dict:
                area = areas_dict[area_id]
                area.owner_idx_init = players_dict[player_id].idx
                area.armies = 1

        # init observation space:
        game_state_dim = len(self.update_game_state())
        self.game_state = None
        self.observation_space = gym.spaces.Box(low=0.0, high=1.0, shape=(game_state_dim,),
                                                dtype=np.float32)

        self.reset()

    @property
    def player_names(self):
        return [p.id for p in self.players]

    @property
    def active_player_name(self):
      return self.active_player.id

    @property
    def active_player_type(self):
      return self.active_player.type

    @property
    def active_player(self):
      return self.players[self.player_active_idx]

    def reset(self):
      """
      :return: (np.array) representing the observation 
      """
      # reset owners to pre-defined areas:
      for area in self.areas:
        area.owner_idx = area.owner_idx_init if area.owner_idx_init >= 0 else -1

      # assign owners to free areas randomly:
      areas_unassigned = np.random.permutation([a for a in self.areas if a.owner_idx < 0])
      for i, area in enumerate(areas_unassigned):
        area.owner_idx = i % self.cnt_players

      # reset player states:
      for p in self.players:
        p.reset()
      self.update_player_areas()

      # init action space:
      self.actions = [
        Pass(0), 
        Trade(1)
      ]
      for i, area in enumerate(self.areas):
          self.actions.append(Reinforce(len(self.actions), area.idx))
      for move in self.moves:
          move.idx = len(self.actions)
          self.actions.append(move)
      for move in self.moves:
          self.actions.append(Attack(len(self.actions), move))
      self.action_space = gym.spaces.Discrete(len(self.actions))
      self.action_space_mask = [0]*len(self.actions)

      # init active player:
      self.player_active_idx = np.argmin([p.cnt_areas for p in self.players])
      self.prev_player = self.active_player
      self.active_player.add_reinforcement(self)
      self.active_player.reserve_before_action = self.active_player.reserve

      # create observation from game state:
      self.update_game_state()

      # create action space:
      self.update_actions()
      # for i, action in enumerate(self.actions):
      #   print(i, str(action))
      
      # return game state as observation:
      return np.array(self.game_state).astype(np.float32)

    def update_player_areas(self):
      for player in self.players:
        player.cnt_areas = 0
      for area in self.areas:
        if area.owner_idx >= 0:
          self.players[area.owner_idx].cnt_areas += 1

    def update_game_state(self):
      self.game_state = []
      for player in self.players:
        self.game_state += [1 if player.reserve > 0 else 0]
        self.game_state += [(1 if area.owner_idx == player.idx else 0) for area in self.areas]
      return self.game_state

    def update_actions(self):
      for i, action in enumerate(self.actions):
        self.action_space_mask[i] = 1 if action.is_executable(self) else 0

    # @property
    # def action_map(self):
    #   return [action.id() for action in self.actions]

    @property
    def action_map(self):
      return {a.id(): a.idx for a in self.actions if a.is_executable(self)}

    def step(self, action_idx):
      action = self.actions[action_idx]
      if action.is_executable(self):
        player_active = self.active_player
        player_active.reserve_before_action = player_active.reserve
        self.last_action = action
        self.prev_player = player_active
        self.last_round = self.round

        action.execute(self)        # self.active_player may have changed
        
        self.update_player_areas()

        # create observation from game state:
        self.update_game_state()

        # create action space:
        self.update_actions()

        is_winner = player_active.cnt_areas == self.cnt_areas
        if is_winner:
            self.winner = player_active
            reward = 100
        else:
            #reinforcement_mine = player_active.get_reinforcement(self)
            #reinforcement_others = max([p.get_reinforcement(self) for p in self.players if p.idx != player_active.idx])
            #reward = reinforcement - reinforcement_others + \
            reward = action.reward(self)
        
        player_active.rewards_total += reward
        self.last_reward = reward

        cnt_armies = [a.armies for a in self.areas]
        is_draw = (max(cnt_armies) > self.armies_per_country_max and \
                   min(cnt_armies) > self.armies_per_country_min) or \
                  self.round > self.rounds_max
        
        self.game_over = is_winner or is_draw
        is_executable = True
      else:
        reward = -10
        self.game_over = False
        is_executable = False

      return np.array(self.game_state).astype(np.float32), reward, self.game_over, {'legal': is_executable}

    def render(self, mode='console', state_only=False):
      if mode != 'console':
          raise NotImplementedError()

      if self.last_round == 0 and not state_only:
        print("round | active player(reserve) | ... area idx:owner(armies) ... | action    => reward (total rewards) | nbr of areas of player, cards")
      
      if state_only:
        print(f"{self.last_round} | {self.active_player.id}({self.active_player.reserve}) |", end='')
      else:
        print(f"{self.last_round} | {self.prev_player.id}({self.prev_player.reserve_before_action}) |", end='')
      for a in self.areas:
        print(f" {a.idx}:{a.owner(self)}({a.armies})", end='')
      if not state_only:
        print(f" | {str(self.last_action).ljust(12)} => "
              f"{str(self.last_reward)} ({self.prev_player.rewards_total}) | "
              f"{self.prev_player.cnt_areas}", end='')
        s = "".join(self.prev_player.cards.values())
        if s:
          print(",", s)
        else:
          print("")

        if self.last_round != self.round:
          print("-"*110)
          # print("   areas   |", end='')
          # for a in self.areas:
          #   print(f"  {a.id.ljust(5)}", end='')
          # print("")

      else:
        print("")

      if self.game_over:
        print("-"*110)
        if self.winner is not None:
          print("GAME OVER! Winner: "+self.winner.id)
        else:
          print("GAME OVER! Draw")

    def print_actions(self):
      print('possible actions: ', end="")
      prev_type = ''
      for action in self.actions:
        if action.is_executable(self):
          type = action.id()[0:1]
          # if prev_type and type != prev_type:
          #   print("")
          print(f"{action.idx}:{action.id()} ", end="")
          prev_type = type
      print("")
          
    def close(self):
      pass

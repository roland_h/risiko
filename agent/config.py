DEBUG = 10
INFO = 20
WARN = 30
ERROR = 40
DISABLED = 50

SEED = -1

LOGDIR = "logs"
RESULTSPATH = 'viz/results.csv'
TMPMODELDIR = "tmp"
MODELDIR = "models"

GAME = "game_test1.json"

from typing import List
from dataclasses import dataclass, field

import sys
import os
import random
import numpy as np
from risk_env import RiskEnv
from utils.agents import create_agents

import gym

from stable_baselines3.common.env_checker import check_env


game_cfg = "game_test1.json"
N_games = 1


env = RiskEnv(game_cfg)
#check_env(env, warn=True)

agents = create_agents(env)

for i_games in range(N_games):
  
  observation = env.reset()
  env.render(state_only=True)
  
  done = False
  while not done:

    action = agents[env.active_player_name].choose_action(env)

    observation, reward, done, info = env.step(action)
    
    if info['legal']:
      env.render()

env.close()


# env = gym.make('CartPole-v0')
# env.reset()
# for _ in range(1000):
#     env.render()
#     env.step(env.action_space.sample()) # take a random action
# env.close()


# from stable_baselines3 import A2C

# env = gym.make('CartPole-v1')

# model = A2C('MlpPolicy', env, verbose=1)
# model.learn(total_timesteps=10000)

# obs = env.reset()
# for i in range(1000):
#     action, _state = model.predict(obs, deterministic=True)
#     obs, reward, done, info = env.step(action)
#     env.render()
#     #if done:
#       # obs = env.reset()
package risiko.game.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeoPos implements Cloneable {
	float lon;
	float lat;

	public GeoPos(GeoPos pos) {
		this.lon = pos.lon;
		this.lat = pos.lat;
	}

	public GeoPos(float lon, float lat) {
		this.lon = lon;
		this.lat = lat;
	}


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
	public String toString() {
		return String.valueOf(lon)+','+String.valueOf(lat);
	}

	public float getLon() {
		return lon;
	}

	public float getLat() {
		return lat;
	}

	public List<Float> getLonLatCopy() {
		return new ArrayList<Float>(Arrays.asList(lon,lat));
	}

	public GeoPos movedTo(float degree, float dist) {
		return new GeoPos(this.lon + (float)Math.sin(Math.toRadians(degree)),  	
						  this.lat + (float)Math.cos(Math.toRadians(degree))); 
	}
}

package risiko.game.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import risiko.game.domain.GamePhase;

public abstract class GameInterface implements GameState {
	
	public GameInterface() throws Exception {
	}

	// state:
	
	public abstract UUID getId();
	public abstract int getIndex();
    public abstract int getIndexUpdate();
    public abstract String getTitle();
    public abstract LocalDateTime getCreatedOn();

    public abstract String getBoardName();
    
    public abstract List<PlayerState> getPlayers();
	public abstract String getActivePlayerName();

    public abstract GamePhase getPhase();
	public abstract int getGameRound();

	public abstract List<ArmyInterface> getArmies();
    public abstract ArmyInterface getArmyReserve();

    public abstract List<MoveInterface> getArmyMoves();

    public abstract String getAreaAttacked();
    public abstract AttackInterface getAttack();

    
    // interface: 

    public abstract void setIndex(int index);

}

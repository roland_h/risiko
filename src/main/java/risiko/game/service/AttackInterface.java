package risiko.game.service;

import java.util.List;

public interface AttackInterface 
{
	public static interface DicesThrow {
	    public List<Integer> getDicesAttacking();
	    public List<Integer> getDicesDefending();
	    public List<ArmyInterface> getArmiesLostAttacking();
	    public List<ArmyInterface> getArmiesLostDefending();
	    public List<ArmyInterface> getArmiesNotLost();
	}

	public String getAreaId();
	public String getAttackingPlayerName();
	public String getDefendingPlayerName();
	public List<DicesThrow> getDicesThrows();
	public int getArmyStrengthInAttackingArea();
	public int getArmyStrengthInDefendingArea();
}

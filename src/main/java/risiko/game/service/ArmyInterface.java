package risiko.game.service;

import java.util.UUID;

import risiko.game.common.GeoPos;

public interface ArmyInterface {
	
	public GeoPos getPos();
	public String getAreaId();
	public UUID getId();
	public String getOriginAreaId();
	public String getOwnerColor();
	public int getGroup();
	public int getStrength();

}

package risiko.game.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import risiko.game.common.GeoPos;
import risiko.game.controller.dto.GameOverview;
import risiko.game.controller.mapper.GameOverviewMapper;
import risiko.game.domain.Game;
import risiko.game.domain.GamePhase;
import risiko.game.domain.Player;
import risiko.game.repository.GameRepository;
import risiko.game.service.GameService;
import risiko.game.service.GameInterface;
import risiko.game.service.GameState;
import risiko.game.service.PlayerInterface;
import risiko.game.service.UserInterface;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;


@Service
public class GameServiceImpl implements GameService {
	
	private final Map<UUID, Game> gamesRuning = new HashMap<UUID, Game>();
	
	private final Queue<DeferredResult<List<GameOverview>>> updateRequestsGameOverview = new ConcurrentLinkedQueue<>();

	private final GameRepository gameRepo = new GameRepository();
	
	
	GameServiceImpl() {
		gameRepo.addUser("Roland", "");
		gameRepo.addUser("Walter", "");
		gameRepo.addUser("Heimo", "");
		try {

			{
				Game game = new Game("Default - 2 Spieler", Arrays.asList("Roland", "Walter"), "default", 22);
				gamesRuning.put(game.getId(), game);
				gameRepo.addGameState(game);
			}
			{
				
				Game game = new Game("Default - 3 Spieler", Arrays.asList("Roland", "Walter", "Heimo"), "default", 15);
				gamesRuning.put(game.getId(), game);
				gameRepo.addGameState(game);
			}
			{
				Game game = new Game("Test-1 - 2 Spieler", Arrays.asList("Roland", "Walter"), "test1", 0);
				gamesRuning.put(game.getId(), game);
				gameRepo.addGameState(game);						
			}
			{
				Game game = new Game("Test-2 - 3 Spieler", Arrays.asList("Roland", "Walter", "Heimo"), "test2", 0);
				gamesRuning.put(game.getId(), game);
				gameRepo.addGameState(game);						
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
    @Override
	public UserInterface getAdmin(String password) 
	{
		return gameRepo.getAdmin(password);
	}

    @Override
	public boolean removeGame(UUID adminToken, UUID gameId) {
    	if (gameRepo.removeGame(adminToken, gameId)) {
    		gamesRuning.remove(gameId);
    		return true;
    	}
    	return false;
    }

    @Override
	public boolean removePlayer(UUID adminToken, UUID gameId, String playerName) {
    	if (gameRepo.removePlayer(adminToken, gameId, playerName)) {
    		gamesRuning.remove(gameId); // re-instantiate the game, if required
    		return true;
    	}
    	return false;
    }

    @Override
	public boolean resetPassword(UUID adminToken, String playerName) {
    	return gameRepo.resetPassword(adminToken, playerName);    	
    }

	
	
    @Override
    public GameState newGame(String title) 
    {
    	if (title.isEmpty())
    		return null;
    	
    	for (GameState game : getAllGames())
    		if (game.getTitle().contentEquals(title))
    			return game;
    	
		try {
			Game game = new Game(title);
	        gamesRuning.put(game.getId(), game);
	        archiveGameState(game);
	        return game;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }

    
    @Override
	public GameState getGameState(UUID gameId, UUID playerId, int index) 
    {	
    	if (index == -1) // get current game state: 
    	{
            Game game = getGame(gameId);
            if(game != null) {
            	Player player = game.getPlayer(playerId);
            	if (player != null)
            		return game;
            }
        }
	    else // get game state from game history: 
	    {
	        Game game = getGame(gameId);
	        if(game != null) {
	        	Player player = game.getPlayer(playerId);
	        	if (player != null) {
        			GameState state = loadGameStateHistory(gameId, index);
        			if (state != null)
        				return state;
        			else
        				return game;
	        	}
	        }
	    }
        return null;
    }

    
    @Override
    public PlayerInterface addPlayer(UUID gameId, String playerName, String password) {
        Game game = getGame(gameId);
        if(game != null)
        {
        	UserInterface user = gameRepo.getUser(playerName);
        	if (user != null) {
        		if (user.hasResetPassword())
                	user = gameRepo.setUserPassword(playerName, password);
        		else
                	user = gameRepo.authenticateUser(playerName, password);
        	}
        	else
        		user = gameRepo.addUser(playerName, password);
        	
        	if (user != null) {
        		Player player = game.addPlayer(playerName, user.getId());
        		if (player != null)
        			archiveGameState(game);
        		return player;
        	}
        }
        return null;
    }

    
    @Override
    public UserInterface getUser(UUID gameId, String playerName) {
        Game game = getGame(gameId);
        if(game != null)
        {
        	return gameRepo.getUser(playerName);
        }
        return null;
    }
    
    
    @Override
    public UserInterface resetUserPassword(UUID gameId, String playerName) {
        Game game = getGame(gameId);
        if(game != null)
        {
        	return gameRepo.resetUserPassword(playerName);
        }
        return null;
    }
    
    
    @Override
    public PlayerInterface joinGame(UUID gameId, String playerName, String password) {
        Game game = getGame(gameId);
        if(game != null)
        {
        	UserInterface userAuthenticated = gameRepo.authenticateUser(playerName, password);
        	if (userAuthenticated != null) {
        		Player player = game.addPlayer(playerName, userAuthenticated.getId());
        		if (player != null)
        			archiveGameState(game);
        		return player;
        	}
        }
        return null;
    }


    @Override
    public GameInterface startGame(UUID gameId, UUID playerId) {
        Game game = getGame(gameId);
        if (game != null)
        {      	
        	game.startGame(playerId);
        	if (game != null)
        		archiveGameState(game);
        	return game;
        }
    	return null;
    }

    @Override
    public GameInterface nextPlayer(UUID gameId, UUID playerId) {
        Game game = getGame(gameId);
        if(game != null) {
        	if (game.nextPlayer(playerId))
        		archiveGameState(game);
    		return game;
        }
    	return null;
    }

    
    @Override
    public GameInterface moveArmy(UUID gameId, UUID playerId, UUID armyId, String area, GeoPos pos, int count) {
        Game game = getGame(gameId);
        if (game != null)
        {
        	if (game.moveArmy(playerId, armyId, area, pos, count))
        		return archiveGameState(game);
        }
        return null;    	
    }


    @Override
    public GameInterface executeAttack(UUID gameId, UUID playerId, float deltaGeoPos) {
        Game game = getGame(gameId);
        if(game != null) {
        	if (game.executeAttack(playerId, deltaGeoPos))
        		return archiveGameState(game);
        }
    	return null;
    }

    
    @Override
    public GameInterface redeemCards(UUID gameId, UUID playerId) {
        Game game = getGame(gameId);
        if(game != null) {
    		if (game.redeemCards(playerId)) {
        		archiveGameState(game);
    			return game;
    		}
        }
    	return null;    	
    }

    
    @Override
    public GameInterface groupArmies(UUID gameId, UUID playerId, UUID armyId, int count) {
        Game game = getGame(gameId);
        if (game != null) {
        	if (game.groupArmies(playerId, armyId, count))
        		return archiveGameState(game);
        }
    	return null;    	    	
    }

    
    @Override
    public GameInterface ungroupArmies(UUID gameId, UUID playerId, UUID armyId, int strength, float deltaGeoPos) {
        Game game = getGame(gameId);
        if (game != null) {
        	if (game.ungroupArmies(playerId, armyId, strength, deltaGeoPos))
        		return archiveGameState(game);
        }
    	return null;    	    	
    }

    
    @Override
    public PlayerInterface getPlayer(UUID gameId, UUID playerId) {
        Game game = getGame(gameId);
        if(game != null)
        {
        	return game.getPlayer(playerId);
        }
        return null;    	
    }

    
    @Override
    public Collection<GameState> getAllGames() 
    {
		Map<UUID,GameState> games = gameRepo.getGames();

		for (Iterator<Entry<UUID, Game>> it = gamesRuning.entrySet().iterator(); it.hasNext();) {
			Entry<UUID, Game> game = it.next();
			if (games.get(game.getKey()) == null)
				games.put(game.getKey(), game.getValue());
		}
			
        return games.values();
    }

    
    //-------------------------------------------------------------------------------------

    
    private Game getGame(UUID gameId) 
    {
        Game game = gamesRuning.get(gameId);
        if(game == null)
        {
        	game = loadGame(gameId);
        }
        return game;
    }

    
    private Game loadGame(UUID gameId) 
    {
		GameState gameState = gameRepo.loadGameState(gameId);
		if (gameState != null) {
			try {
				Game game = new Game(gameState);
				if (game != null) {
					gamesRuning.put(gameId, game);
					return game;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    	return null;
    }
    
    
    private GameState loadGameStateHistory(UUID gameId, int index) 
    {	
		int cntGameStates = gameRepo.getCountGameStates(gameId);    		
		if (index < 0) {
			if (-index <= cntGameStates)
				return gameRepo.loadGameState(gameId, cntGameStates+index);
		}
		else {
			if (index < cntGameStates)
				return gameRepo.loadGameState(gameId, index);
		}    	
		return null;
    }
    
    
    private GameInterface archiveGameState(Game game)
    {
    	GameState prevGameState = gameRepo.loadGameState(game.getId());
    	if (prevGameState != null) 
    	{
    		GameState mergedGameState = mergeGameStates(game, prevGameState);
    		if (mergedGameState != null) 
    		{
        		game.setIndex(prevGameState.getIndex());
        		game.setIndexUpdate(prevGameState.getIndexUpdate()+1);
        		System.out.println("update game state: "+game.getIndex()+", update: "+game.getIndexUpdate());
    			gameRepo.updateGameState(mergedGameState);
    		}
    		else 
    		{
        		game.setIndex(prevGameState.getIndex()+1);
        		game.setIndexUpdate(0);
        		System.out.println("add game state: "+game.getIndex());
    			gameRepo.addGameState(game);    			
    		}
    	}
    	else 
    	{
    		game.setIndex(0);
    		gameRepo.addGameState(game);
    	}
    	return game;
    }

    
    private GameState mergeGameStates(Game game, GameState gamePrev)
    {
    	if (gamePrev.getPhase() == GamePhase.UNDEFINED || gamePrev.getPhase() == GamePhase.JOIN) 
    	{
    		return game;    		
    	}
    	else if (gamePrev.getPhase() == game.getPhase()) 
    	{
    		if (game.getAttack() == null && 
    				(game.armyMovesWithinArea() || game.hasSameSrcAndDst(gamePrev.getArmyMoves()))) 
    		{
    			System.out.println("group moves: "+game.getArmyMoves().size()+" + "+gamePrev.getArmyMoves().size());
    			game.insertArmyMoves(gamePrev.getArmyMoves());
    			return game;
    		}
    	}
    	return null;
    }

    
    //-------------------------------------------------------------------------------------
    
    
	@Override
	public void addGameOverviewUpdateRequest(DeferredResult<List<GameOverview>> request)
	{
		this.updateRequestsGameOverview.add(request);
		
		request.onCompletion(new Runnable() {
			@Override
			public void run() {
				updateRequestsGameOverview.remove(request);
			}
		});
	}
	
	public class GaveOverviewNotifier extends Thread {
	    public void run() {
			Iterator<DeferredResult<List<GameOverview>>> it = updateRequestsGameOverview.iterator();
			while (it.hasNext())
			{
				it.next().setResult(GameOverviewMapper.map(getAllGames()));
			}
	    }
	}
	
	@Override
	public void updateGameOverviewListeners() {
		GaveOverviewNotifier notifier = new GaveOverviewNotifier();
		notifier.start();
	}

}

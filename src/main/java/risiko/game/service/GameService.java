package risiko.game.service;

import risiko.game.common.GeoPos;
import risiko.game.controller.dto.GameOverview;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.springframework.web.context.request.async.DeferredResult;

public interface GameService 
{
	UserInterface getAdmin(String password);
	boolean removeGame(UUID adminToken, UUID gameId);
	boolean removePlayer(UUID adminToken, UUID gameId, String playerName);
	boolean resetPassword(UUID adminToken, String playerName);
	
    UserInterface getUser(UUID gameId, String playerName);

    GameState getGameState(UUID gameId, UUID playerId, int index);
    PlayerInterface getPlayer(UUID gameId, UUID playerId);
    PlayerInterface addPlayer(UUID gameId, String playerName, String password);
    UserInterface resetUserPassword(UUID gameId, String playerName);
    PlayerInterface joinGame(UUID gameId, String playerName, String password);
	
    GameState newGame(String title);
    GameInterface startGame(UUID gameId, UUID playerId);
    GameInterface nextPlayer(UUID gameId, UUID playerId);

    GameInterface moveArmy(UUID gameId, UUID userId, UUID armyId, String area, GeoPos pos, int count);
    GameInterface executeAttack(UUID gameId, UUID playerId, float deltaGeoPos);
    GameInterface redeemCards(UUID gameId, UUID playerId);
    GameInterface groupArmies(UUID gameId, UUID userId, UUID armyId, int strength);
    GameInterface ungroupArmies(UUID gameId, UUID playerId, UUID armyId, int strength, float deltaGeoPos);

    Collection<GameState> getAllGames(); 

    void updateGameOverviewListeners();
    void addGameOverviewUpdateRequest(DeferredResult<List<GameOverview>> request);
}

package risiko.game.service;

import java.util.List;
import java.util.UUID;

public abstract class PlayerInterface implements PlayerState {	
	
	public abstract String getName();
	public abstract UUID getId();
	public abstract String getColor();
	public abstract int getArmyReserveCount();
    public abstract String getMissionId();
	public abstract List<Integer> getVictoryCards();

	// interface:
	public abstract String getMissionDescription();
	public abstract int getVictoryCardsValue();
	public abstract GameInterface getGame();
}

package risiko.game.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import risiko.game.domain.GamePhase;

public interface GameState {

    public UUID getId();
    public int getIndex();
    public int getIndexUpdate();
    public LocalDateTime getCreatedOn();
    
    public String getTitle();

    public String getBoardName();
    
    public List<PlayerState> getPlayers();
	public String getActivePlayerName();

    public GamePhase getPhase();
    public int getGameRound();
    
	public List<ArmyInterface> getArmies();
	public ArmyInterface getArmyReserve();
	
    public String getAreaAttacked();
    public AttackInterface getAttack();
    public String getAreaConqueredId();
    public String getAreaConqueredFromId();

    public List<MoveInterface> getArmyMoves();
    
    public String getWinner();
    public String getWinnerMission();
    
    public String getMessage();
}

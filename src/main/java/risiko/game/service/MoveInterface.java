package risiko.game.service;

import java.util.UUID;

import risiko.game.common.GeoPos;

public interface MoveInterface {

	public String getMessage();
	public UUID getArmyId();
	public String getArmyOldAreaId();
	public String getArmyNewAreaId();
	public GeoPos getArmyOldPos();
	public GeoPos getArmyNewPos();
	public int getArmyNewStrength();
	public int getArmyOldStrength();

	public boolean withinArea();
	public boolean sameSrcDst(MoveInterface move);

}

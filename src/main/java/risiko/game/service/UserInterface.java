package risiko.game.service;

import java.util.UUID;

public interface UserInterface {

	public String getName();
	public UUID getId();
	public boolean authenticate(String userName, String userPwd);
	public boolean requirePassword();
	public boolean hasResetPassword();

}

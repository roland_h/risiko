package risiko.game.service;

import java.util.List;
import java.util.UUID;

public interface PlayerState {
	public String getName();
	public UUID getId();
	public String getColor();
	public int getArmyReserveCount();
    public String getMissionId();
	public List<Integer> getVictoryCards();
}

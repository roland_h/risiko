package risiko.game.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import risiko.game.service.GameInterface;
import risiko.game.service.PlayerInterface;
import risiko.game.service.PlayerState;

public class Player extends PlayerInterface implements Cloneable {
	
	static String[] colors = {
		"#EE0000", 
		"#00EE00", 
		"#0000EE", 
		"#00EEEE", 
		"#000000"
	};
	
	int index = -1; 
    UUID id;
    String name;
    String color = "";
    int armiesReserve = 0;
    Mission mission = null;
    boolean isWinner = false;
    Map<Integer,Integer> victoryCards = new HashMap<Integer,Integer>();
    Game game = null;
    
    Random rand = new Random();


    public Player(PlayerState player, int index, Mission mission, Game game) {
    	this.index = index;
    	this.id = player.getId();
    	this.name = player.getName();
    	this.color = player.getColor();
    	this.armiesReserve = player.getArmyReserveCount();
    	this.mission = mission;
    	for (Iterator<Integer> it = player.getVictoryCards().iterator(); it.hasNext();)
    		this.victoryCards.put(-1-this.victoryCards.size(), it.next());
    	this.game = game;
    }
    
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
	public Player(String name, UUID userId, int idx, Game game) {
		this.index = idx;
        this.id = userId;
        this.name = name;
        this.game = game;
    }

	public int getIndex() {
		return index;
	}
	
    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
		return color;
	}
    
    public void setColor() {
    	if (index >= 0 && color.isEmpty()) {
    		if (index < colors.length)
    			color = colors[index];
    		else
    			color = "#FFFFFF";
    	}
    }
    
    public int decArmyReserve(int cnt) {
    	armiesReserve = Math.max(0, armiesReserve-cnt);
    	return armiesReserve;
    }

    public int decArmyReserve() {
    	return decArmyReserve(1);
    }

    public int incArmyReserve(int cnt) {
    	armiesReserve += cnt;
    	return armiesReserve;
    }

    public int getArmyReserveCount() {
    	return armiesReserve;
    }
    
    public void setMission(Mission mission) {
    	this.mission = mission;
    }
    
    public Mission getMission() {
    	return this.mission;
    }
    
    
    public void setWinner() {
    	this.isWinner = true;
    }

    public boolean isWinner() {
    	return this.isWinner;
    }

	public void addVictoryCard(int round) 
	{
		if (!victoryCards.containsKey(round))
		{
			int rand_nbr = rand.nextInt(40+12+8);
			if (rand_nbr < 40)
				victoryCards.put(round, 0);
			else if (rand_nbr < 40+12)
				victoryCards.put(round, 1);
			else
				victoryCards.put(round, 2);
		}
	}

	public List<Integer> getVictoryCards() {
		return new ArrayList<Integer>(victoryCards.values());
	}
	
	public boolean removeVictoryCard(int type)
	{
		Integer round = null;
	    for (Map.Entry<Integer, Integer> entry : victoryCards.entrySet()) {
	        if (entry.getValue().equals(type)) {
	            round = entry.getKey();
	            break;
	        }
	    }
	    if (round != null) {
	    	victoryCards.remove(round);
	    	return true;
	    }
	    return false;
	}	
	
	public int redeemVictoryCards()
	{
		int cardsValue = getVictoryCardsValue();
		
		switch(cardsValue) {
		case 10:
			armiesReserve += cardsValue;
			removeVictoryCard(0);
			removeVictoryCard(1);
			removeVictoryCard(2);
			return 10;
		case 8:
			armiesReserve += cardsValue;
			removeVictoryCard(2);
			removeVictoryCard(2);
			removeVictoryCard(2);
			return 8;
		case 6:
			armiesReserve += cardsValue;
			removeVictoryCard(1);
			removeVictoryCard(1);
			removeVictoryCard(1);
			return 6;
		case 4:
			armiesReserve += cardsValue;
			removeVictoryCard(0);
			removeVictoryCard(0);
			removeVictoryCard(0);
			return 4;
		}
		return 0;
	}
	
	public int getVictoryCardsValue() 
	{
		if (victoryCards.size() >= 3)
		{
			int[] cardTypesCount = new int[] { 0, 0, 0 }; 
			Iterator<Integer> it = getVictoryCards().iterator();
			while (it.hasNext())
				cardTypesCount[it.next()] += 1;
			
			if (cardTypesCount[0] >= 1 && cardTypesCount[1] >= 1 && cardTypesCount[2] >= 1)
				return 10;
			if (cardTypesCount[2] >= 3)
				return 8;
			if (cardTypesCount[1] >= 3)
				return 6;
			if (cardTypesCount[0] >= 3)
				return 4;
		}		
		return 0;
	}
	
    public String getMissionId() {
    	return mission != null ? mission.getId() : "";
    }

	@Override
	public String getMissionDescription() {
		return mission != null ? mission.getDescription() : "";
	}

	public GameInterface getGame() {
		return game;
	}


}

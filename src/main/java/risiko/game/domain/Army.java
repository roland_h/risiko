package risiko.game.domain;

import java.util.UUID;

import risiko.game.common.GeoPos;
import risiko.game.service.ArmyInterface;

public class Army implements ArmyInterface, Cloneable
{
	GeoPos pos = null;
	Area area = null;
	UUID id;
	Area originArea = null;
	Player owner = null;
	int group = -1;
	int strength = 1;

	
	public Army(GeoPos pos, Area area) {
		this.pos = pos;
		this.area = area;
		this.originArea = area;
		this.id = UUID.randomUUID();
	}

	public Army(Army army, Player owner) {
		this.pos = army.pos;
		this.area = army.area;
		this.originArea = area;
		this.id = UUID.randomUUID();
		this.owner = owner;
	}

	public Army(GeoPos pos, Area area, Area origin, Player owner) {
		this.pos = pos;
		this.area = area;
		this.originArea = origin;
		this.id = UUID.randomUUID();
		this.owner = owner;
	}
	
	public Army(ArmyInterface army, Area area, Area originArea, Player owner) {
		this.pos = army.getPos();
		this.area = area;
		this.originArea = originArea;
		this.id = army.getId();
		this.owner = owner;
	}

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    

	public GeoPos getPos() {
		return pos;
	}

	public Area getArea() {
		return area;
	}

	public String getAreaId() {
		if (area != null)
			return area.getId();
		else
			return "";
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public void setOriginArea(Area originArea) {
		this.originArea = originArea;
	}

	public UUID getId() {
		return id;
	}

	public Area getOriginArea() {
		return originArea;
	}

	public String getOriginAreaId() {
		if (originArea != null)
			return originArea.getId();
		else
			return "";
	}

	public void setPos(GeoPos pos) {
		this.pos = pos;
	}

	public Player getOwner() {
		return owner;
	}
	
	public String getOwnerColor() {
		return owner.getColor();
	}
	
	private float squ(float a) { return a*a; }
	
	public float distLonLatSqu(Army army) 
	{
		return (squ(army.pos.getLat()-this.pos.getLat()) + squ(army.pos.getLon()-this.pos.getLon())); 
	}
	
	public void setGroup(int group) {
		this.group = group;
	}
	
	public int getGroup() {
		return this.group;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getStrength() {
		return this.strength;
	}
	
}

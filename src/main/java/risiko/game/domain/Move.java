package risiko.game.domain;

import java.util.UUID;

import risiko.game.common.GeoPos;
import risiko.game.service.ArmyInterface;
import risiko.game.service.MoveInterface;


public class Move implements MoveInterface, Cloneable {

	public String message = "";
	public Army army = null;
	public String armyOldAreaId = "";
	public GeoPos armyOldPos = null;
	public int armyOldStrength = -1;

	public Move(Army armyMoved, Area oldArea, GeoPos oldPos, int oldStrength) {
    	this.army = armyMoved;
    	this.armyOldAreaId = oldArea != null ? oldArea.getId() : "";
		this.armyOldPos = new GeoPos(oldPos);
		this.armyOldStrength = oldStrength;
    }

	public Move(MoveInterface move, Board board) {
		this.message = move.getMessage();
		this.army = board.getArmy(move.getArmyId());
		this.armyOldAreaId = move.getArmyOldAreaId();
		this.armyOldPos = move.getArmyOldPos();
		this.armyOldStrength = move.getArmyOldStrength();
	}
	
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
	public String getMessage() {
		return message;
	}

	public ArmyInterface getArmy() {
		return army;
	}

	public String getArmyOldAreaId() {
		return armyOldAreaId;
	}

	public GeoPos getArmyOldPos() {
		return armyOldPos;
	}

	public int getArmyOldStrength() {
		return armyOldStrength;
	}

	@Override
	public String getArmyNewAreaId() {
		return this.army.getAreaId();
	}


	@Override
	public UUID getArmyId() {
		return this.army.getId();
	}


	@Override
	public GeoPos getArmyNewPos() {
		return this.army.getPos();
	}


	@Override
	public int getArmyNewStrength() {
		return this.army.getStrength();
	}
	
	public boolean withinArea() {
		return armyOldAreaId.equals(army.getAreaId());
	}

	public boolean sameSrcDst(MoveInterface move) {
		return armyOldAreaId.equals(move.getArmyOldAreaId()) && army.getAreaId().equals(move.getArmyNewAreaId());
	}

}

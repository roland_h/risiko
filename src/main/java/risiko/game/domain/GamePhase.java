package risiko.game.domain;

public enum GamePhase {
	UNDEFINED, 
    JOIN,
    INIT, 
    UPGRADE,
    REDEEM,
    ATTACK,
    MOVE,
    END
}

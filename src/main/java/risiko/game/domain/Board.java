package risiko.game.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import risiko.game.common.GeoPos;
import risiko.game.service.ArmyInterface;

import java.io.IOException;
import java.io.InputStream;


public class Board 
{
	private String boardFileName;
	private List<Army> armies = new ArrayList<Army>();
	private List<Army> armiesInit = new ArrayList<Army>();
	private Map<String,Area> areas = new HashMap<>();
	private Map<String,List<Area>> continents = new HashMap<String,List<Area>>();
	private Map<String,Integer> continentsValues = new HashMap<String,Integer>();
	public  List<Mission> missionsPool = new ArrayList<Mission>();
	
	private Map<String,String> initialAreaAssignments = new HashMap<String,String>();
	private Map<String,List<Integer>> initalCardAssignments = new HashMap<String,List<Integer>>();
	private Map<String,String> initalMissionAssignments = new HashMap<String,String>();

	private Area areaAttacked = null;
	private Area areaAttackOrigin = null;
	private final Area areaReserve = new Area("");
    private Army armyReserve = null;
		
    Random rand = new Random();

    
	public Board(String boardName) throws Exception, IOException 
	{
		boardFileName = "/static/boards/"+boardName+".json";

		@SuppressWarnings("resource")
		InputStream jsonInput = Board.class.getResourceAsStream(boardFileName);
		if (jsonInput == null) {
			throw new Exception("Resource file does not exist or is not readable: "+boardFileName);
		}
		String jsonData = new Scanner(jsonInput, "UTF-8").useDelimiter("\\A").next();

		System.out.println("read game board: "+boardFileName);
		
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> jsonTree = objectMapper.readValue(jsonData, new TypeReference<Map<String,Object>>(){});
		
		@SuppressWarnings("unchecked")
		Iterator<Entry<String, Object>> itArea = ((Map<String,Object>)(jsonTree.get("areas"))).entrySet().iterator();
		while(itArea.hasNext()) {
			Entry<String, Object> area = itArea.next();
			String areaId = area.getKey();
			//System.out.println("area "+areaId);
			if (!areas.containsKey(areaId))
				areas.put(areaId, new Area(areaId));
		}
		
		System.out.println("game board areas:");
		itArea = ((Map<String,Object>)(jsonTree.get("areas"))).entrySet().iterator();
		while(itArea.hasNext()) {
			Entry<String, Object> areaEntry = itArea.next();
			
			String areaId = areaEntry.getKey();
			Area area = areas.get(areaId);
			
			LinkedHashMap<String, Object> areaProps = (LinkedHashMap<String, Object>)areaEntry.getValue();

			String areaName = (String)areaProps.get("name");
			area.setName(areaName);
			System.out.println("  "+areaId+": "+areaName);

			String continentId = (String)areaProps.get("continent");
			area.setContinent(continentId);
			List<Area> contientAreas = continents.get(continentId);
			if (contientAreas == null) {
				contientAreas = new ArrayList<Area>();
				continents.put(continentId, contientAreas);
			}
			contientAreas.add(area);

			Iterator<String> itNeighbour = ((List<String>)areaProps.get("neighbours")).iterator();
			while(itNeighbour.hasNext()) {
				String neighbourAreaId = itNeighbour.next();
				if (areas.containsKey(neighbourAreaId)) {
					Area areaNeighbour = areas.get(neighbourAreaId); 
					areaNeighbour.addNeighbour(area);
					area.addNeighbour(areaNeighbour);
				}
			}

			@SuppressWarnings("unchecked")
			Iterator<Double> itPos = ((List<Double>)areaProps.get("pos")).iterator();
			float lon = itPos.next().floatValue();
			float lat = itPos.next().floatValue();
			//System.out.println("    add area "+areaId+": lon: "+lon+", lat: "+lat);
			armiesInit.add(new Army(new GeoPos(lon, lat), area));
		}
		
		System.out.println("continents:");
		@SuppressWarnings("unchecked")
		Iterator<Entry<String, Object>> itContinents = ((Map<String, Object>)(jsonTree.get("continents"))).entrySet().iterator();
		while(itContinents.hasNext()) {
			Entry<String, Object> continentEntry = itContinents.next();

			String continentId = continentEntry.getKey();

			@SuppressWarnings("unchecked")
			LinkedHashMap<String, Object> continentProps = (LinkedHashMap<String, Object>)continentEntry.getValue();
			
			Integer value = (Integer)continentProps.get("value");
			System.out.println("  "+continentId+": "+value);
			
			continentsValues.put(continentId, value);
		}

		System.out.println("missions:");
		@SuppressWarnings("unchecked")
		Iterator<Entry<String, Object>> itMission = ((Map<String, Object>)(jsonTree.get("missions"))).entrySet().iterator();
		while(itMission.hasNext()) {
			Entry<String, Object> missionEntry = itMission.next();

			String missionId = missionEntry.getKey();

			@SuppressWarnings("unchecked")
			LinkedHashMap<String, Object> mission = (LinkedHashMap<String, Object>)missionEntry.getValue();

			String missionDescr = (String)mission.get("description");
			System.out.println("  "+missionId+": "+missionDescr);

			List<String> continents = new ArrayList<String>();
			@SuppressWarnings("unchecked")
			Iterator<String> it = ((List<String>)mission.get("continents")).iterator();
			while(it.hasNext())
				continents.add((String)it.next());

			int cntContinents = (int)mission.get("continents_count");

			String playerColor = (String)mission.get("player_to_destroy_color");

			int cntAreas = (int)mission.get("areas_count");

			int cntArmies = (int)mission.get("armies_per_area");

			int cntPlayers = (int)mission.get("min_players");

			missionsPool.add(new Mission(missionId,
										 missionDescr, 
										 continents, 
										 cntContinents, 
										 cntAreas,
										 playerColor, 
										 cntArmies, 
										 cntPlayers));
		}
		
		
		// area assignments (optional):	
		if (jsonTree.get("players") != null) 
		{
			System.out.println("players:");
			@SuppressWarnings("unchecked")
			Iterator<Entry<String, Object>> itPlayer = ((Map<String, Object>)(jsonTree.get("players"))).entrySet().iterator();
			while(itPlayer.hasNext()) {
				Entry<String, Object> playerEntry = itPlayer.next();

				String playerName = playerEntry.getKey();

				@SuppressWarnings("unchecked")
				LinkedHashMap<String, Object> player = (LinkedHashMap<String, Object>)playerEntry.getValue();

				@SuppressWarnings("unchecked")
				Iterator<String> it = ((List<String>)player.get("areas")).iterator();
				while(it.hasNext()) 
					initialAreaAssignments.put(it.next(), playerName);

				List<Integer> cards = new ArrayList<Integer>();
				@SuppressWarnings("unchecked")
				Iterator<Integer> it2 = ((List<Integer>)player.get("cards")).iterator();
				while(it2.hasNext())
					cards.add((Integer)it2.next());
				initalCardAssignments.put(playerName, cards);

				String mission = (String)player.get("mission");
				initalMissionAssignments.put(playerName, mission);
				
				System.out.println("  "+playerName+": "+cards.size()+" cards, mission: "+mission);
			}
		}			
	}

    public Army getArmyReserve() {
    	return armyReserve;
    }

	
	public void assignMissions(List<Player> players) 
	{
		Map<String, Mission> missionsMap = new HashMap<>();
		for (Mission mission: missionsPool)
			missionsMap.put(mission.getId(), mission);

		int N = missionsPool.size()-1;
		if (N > 0) {
			for (int i=0; i < players.size(); ++i)
			{
				Player player = players.get(i);
				if (initalMissionAssignments.containsKey(player.getName()))	{
					String missionId = initalMissionAssignments.get(player.getName());
					if (missionsMap.containsKey(missionId)) {
						Mission mission = missionsMap.get(missionId);
						player.setMission(mission);
						System.out.println("player "+player.getName()+": pre-set mission: "+player.getMission().getId());
						missionsPool.remove(mission);
					}
				}
				if (player.getMission() == null) {
					int j = 0;
					do { j = Math.abs(rand.nextInt(N)); } while (players.size() < missionsPool.get(j).getCountPlayerMin());
					player.setMission(missionsPool.get(j));
					System.out.println("player "+player.getName()+": mission: "+player.getMission().getDescription());
					missionsPool.remove(j);
				}
			}
		}
	}
	
	public Mission getMissionById(String missionId) {
		if (missionId != null)
			for (Iterator<Mission> it = missionsPool.iterator(); it.hasNext();) {
				Mission mission = it.next();
				if (mission.getId().contentEquals(missionId))
					return mission;
			}
		return null;
	}
	
	
	public boolean checkMissionAchieved(Player player)
	{
		Mission mission = player.getMission();
		if (mission == null) 
			return false;
		
		Set<String> playerColors = new HashSet<>();
		for (Army army: armies)
			playerColors.add(army.getOwnerColor());
		
		List<String> playerContinents = getContinents(player);
		Map<String,Integer> armyStrengthsPerArea = getArmyStrengthsPerArea(player);
		int armyStrengthPerAreaMin = 1;
		int cntAreas = 0;
		if (mission.getCountArmiesPerArea() > 1 && armyStrengthsPerArea.size() >= mission.getCountAreas()) {
			for (Integer strength: armyStrengthsPerArea.values())
				if (strength >= mission.getCountArmiesPerArea())
					++cntAreas;
			List<Integer> strengths = new ArrayList<>(armyStrengthsPerArea.values());
			Collections.sort(strengths, Collections.reverseOrder());
			armyStrengthPerAreaMin = strengths.get(mission.getCountAreas()-1);
		}
		else
			cntAreas = armyStrengthsPerArea.size();
		
		if (mission.hasContinents(playerContinents) &&
			playerContinents.size() >= mission.getCountContinents() &&
			cntAreas >= mission.getCountAreas() &&
			armyStrengthPerAreaMin >= mission.getCountArmiesPerArea() &&
			(mission.getPlayerToDestroyColor().isEmpty() || !playerColors.contains(mission.getPlayerToDestroyColor())))
		{
			player.setWinner();
			return true;
		}
		
		return false;
	}
	

	public void initPlayerArmies(ArrayList<Player> players) 
	{
		for (Player player: players) {			
			List<Integer> initialCards = initalCardAssignments.get(player.getName());
			if (initialCards != null)
				for (int card: initialCards)
					player.victoryCards.put(-player.victoryCards.size(), card);
			
			for (int i=0; i < armiesInit.size(); ++i) {
				Army army = armiesInit.get(i);
				if (initialAreaAssignments.get(army.getAreaId()).equals(player.getName()))
					army.owner = player;
			}
		}
		
		Collections.shuffle(armiesInit);
		int N = players.size();
		System.out.println("initPlayerArmies: #players: "+N+", #armies: "+armiesInit.size());
		for (int i=0; i < armiesInit.size(); ++i) 
		{
			Army initArmy = armiesInit.get(i);
			if (initArmy.getOwner() != null)
			{
				Player player = initArmy.getOwner();
				Army army = new Army(initArmy, player);
				army.getArea().setOwner(player);
				armies.add(army);
				player.decArmyReserve();				
			}
			else 
			{
				Player player = players.get(i%N);
				if (player.getArmyReserveCount() > 0) 
				{
					Army army = new Army(armiesInit.get(i), player);
					army.getArea().setOwner(player);
					armies.add(army);
					player.decArmyReserve();
				}				
			}
		}
	}
	
	public Army createArmy(GeoPos pos, Player player, Area area, Area origin) 
	{
		Army army = new Army(pos, area, origin, player);
		armies.add(army);
		return army;
	}
	
	public Army createArmy(GeoPos pos, Player player, Area area, Area origin, int strength) 
	{
		Army army = createArmy(pos, player, area, origin);
		army.setStrength(strength);
		return army;
	}
	
	public Army createArmyReserve(Player player) 
	{
		if (player.getArmyReserveCount() > 0)
		{
			this.armyReserve = createArmy(new GeoPos(0.f, -90.f), player, areaReserve, null);
			System.out.println("add reserve: "+armyReserve.getId());
			return this.armyReserve;
		}
		return null;
	}
	
	public String getFileName() {
		return boardFileName;
	}

	public List<Army> getArmies() {
		return armies;
	}
	
	public List<Army> getArmiesOf(Player player) {
		ArrayList<Army> armiesOfPlayer = new ArrayList<Army>();
		for (Army army: armies)
			if (army.getOwner() == player)
				armiesOfPlayer.add(army);
		return armiesOfPlayer;	
	}
	
	public Collection<Area> getAreas() {
		return areas.values();
	}
	
	public int getNextFreeGroup() 
	{
		int groupMax = -1;
		Iterator<Army> it = armies.iterator();
		while (it.hasNext())
			groupMax = Math.max(it.next().getGroup(), groupMax);
		return groupMax+1;
	}
	
	public List<Float> getArmyPositions() 
	{
		ArrayList<Float> positions = new ArrayList<Float>();
		Iterator<Army> it = armies.iterator();
		while (it.hasNext()) {
			GeoPos pos = it.next().getPos();
			positions.add(pos.getLon());
			positions.add(pos.getLat());
		}
		return positions;
	}
	
	public Army addArmy(Player player, UUID armyId, String newAreaId, GeoPos newPos) 
	{
		Iterator<Army> it = armies.iterator();
		while (it.hasNext()) {
			Army army = it.next();
			if (army.getId().equals(armyId)) 
			{
				Area newArea = areas.get(newAreaId);
				if (newArea != null && newArea.getOwner() == player)
				{
					army.setArea(newArea);
					army.setPos(newPos);
					return army;			
				}
			}
		}
		return null;
	}
	
	public Army addArmy(ArmyInterface army, Player owner) 
	{
		Army a = new Army(army, getArea(army.getAreaId()), getArea(army.getOriginAreaId()), owner);
		armies.add(a);
		return a;
	}
	

	public Army getArmy(UUID armyId) 
	{
		Iterator<Army> it = armies.iterator();
		while (it.hasNext()) {
			Army army = it.next();
			if (army.getId().equals(armyId))
				return army;
		}
		return null;
	}
	
	public boolean isArmyReserve(Army army)
	{
		return (army.getArea() == null || army.getArea() == areaReserve);
	}
	
	
	public Move moveArmy(Army army, 
						String dstAreaId, GeoPos newPos, 
						boolean allowMoveToNeigbour, boolean allowAttacks) 
	{
		Area srcArea = army.getOriginArea();
		Area dstArea = areas.get(dstAreaId);
		if (dstArea == null) 
		{
			return null; // unknown destination 
		}
		
		printArmiesPerCountry(getArmiesInCountryList()); // todo
		System.out.println("move army: "+army.getAreaId()+" > "+dstAreaId+" (orig: "+army.getOriginAreaId()+")");
		
		boolean isValidArea = (isArmyReserve(army) && dstArea.getOwner() == army.getOwner()) || // reserve army 
							  (army.getArea() == dstArea) || 									// move within country borders   
							  (dstArea.isNeighbour(army.getArea()) && allowMoveToNeigbour);		// move to neighbour country
		if (!isValidArea)
		{
			System.out.println("invalid area");
			return null; // invalid move
		}
		boolean isValidMove = isArmyReserve(army) ||											// place reserve army   
							  army.getArea() == dstArea || 										// move within country borders   
							  army.getOriginArea() == dstArea ||		 						// drawback from attack 
							  getArmiesIn(army.getArea(), army.getOwner()).size() > 1;   		// more than one army in source country
		if (!isValidMove)
		{
			System.out.println("invalid move");
			return null; // invalid move
		}
		
		boolean isAttacking = (dstArea.getOwner() != army.getOwner());
		if (isAttacking)
		{
			if (!allowAttacks ||  											// attacks not allowed
				(areaAttacked != null && (dstArea != areaAttacked ||		// attacking only of one area at a time
										  srcArea != areaAttackOrigin)))	// attacking only from one area at a time
			{
				System.out.println("invalid attack");
				return null; // invalid attack
			}
			areaAttacked = dstArea;
			areaAttackOrigin = srcArea;
		}
		
		if (areaAttacked != null && (dstArea != army.getOriginArea() && dstArea != areaAttacked))	// if attack is ongoing, only moves going  
		{																							// out or into the attacked country are allowed
			System.out.println("invalid move during attack");
			return null;				
		}
	
		GeoPos oldPos = new GeoPos(army.getPos());
		
		if (army.getGroup() >= 0)
		{
			List<Army> armiesInGroup = getArmiesOfGroup(army.getGroup());
			for (Iterator<Army> it=armiesInGroup.iterator(); it.hasNext();) {
				Army a = it.next();
				if (isArmyReserve(a))
					a.setOriginArea(dstArea);
				a.setArea(dstArea);
				a.setPos(newPos);
			}
		}
		else
		{
			if (isArmyReserve(army)) {
				army.setOriginArea(dstArea);
				this.armyReserve = null;
			}
			army.setArea(dstArea);
			army.setPos(newPos);			
		}
		
		if (areaAttacked != null  && getAttackingArmiesIn(areaAttacked).size() == 0)	// draw back from attack
		{
			resetAttack();
		}

		return new Move(army, srcArea, oldPos, army.strength);
	}
	
	public Area getArea(String areaId)
	{
		return areas.get(areaId);
	}
	
	public List<Area> getAreas(Player player) 
	{
		ArrayList<Area> playerAreas = new ArrayList<Area>();
		Iterator<Area> it = areas.values().iterator();
		while (it.hasNext()) {
			Area area = it.next();
			if (area.getOwner() == player)
				playerAreas.add(area);
		}
		return playerAreas;
	}
	
	public Map<String,Integer> getArmyStrengthsPerArea(Player player) 
	{
		Map<String,Integer> armyStrengthsPerArea = new HashMap<String,Integer>();
		for (Army army: armies)
			if (army.getOwner() == player) 
				if (armyStrengthsPerArea.containsKey(army.getAreaId()))
					armyStrengthsPerArea.put(army.getAreaId(), (int)(armyStrengthsPerArea.get(army.getAreaId()))+1);
				else
					armyStrengthsPerArea.put(army.getAreaId(), 1);
		return armyStrengthsPerArea;
	}
	
	public List<String> getContinents(Player player)
	{
		ArrayList<String> playerContinents = new ArrayList<String>();
		Iterator<Entry<String, List<Area>>> itContinent = continents.entrySet().iterator();
		while(itContinent.hasNext()) {
			Entry<String, List<Area>> continentEntry = itContinent.next();
			String continent = continentEntry.getKey();
			List<Area> continentAreas = continentEntry.getValue();
			Iterator<Area> itArea = continentAreas.iterator();
			int cnt = 0;
			while (itArea.hasNext())
				if (itArea.next().getOwner() == player) 
					++cnt;
			if (cnt == continentAreas.size())
				playerContinents.add(continent);
		}
		return playerContinents;
	}
	
	public int getContinentValue(String continent)
	{
		if (continentsValues.containsKey(continent))
			return continentsValues.get(continent).intValue();
		else
			return 0;
	}
	
	public List<Army> getArmiesOfGroup(int group) 
	{
		ArrayList<Army> armiesInGroup = new ArrayList<Army>();
		Iterator<Army> it = armies.iterator();
		while (it.hasNext())
		{
			Army army = it.next();
			if (army.getGroup() == group)
				armiesInGroup.add(army);
		}
		return armiesInGroup;
	}

	public List<Army> getArmiesIn(Area area) 
	{
		ArrayList<Army> armiesInArea = new ArrayList<Army>();
		Iterator<Army> it = armies.iterator();
		while (it.hasNext())
		{
			Army army = it.next();
			if (army.getArea() == area)
				armiesInArea.add(army);
		}
		return armiesInArea;
	}

	public List<Army> getArmiesIn(Area area, Player player) 
	{
		ArrayList<Army> armiesInArea = new ArrayList<Army>();
		Iterator<Army> it = armies.iterator();
		while (it.hasNext())
		{
			Army army = it.next();
			if (army.getArea() == area && army.getOwner() == player)
				armiesInArea.add(army);
		}
		return armiesInArea;
	}

	public int getArmyStrengthIn(Area area, Player player) 
	{
		Iterator<Army> it = armies.iterator();
		int strength = 0;
		while (it.hasNext())
		{
			Army army = it.next();
			if (army.getArea() == area && army.getOwner() == player)
				strength += army.getStrength();
		}
		return strength;
	}

	public List<Army> getAttackingArmiesIn(Area area) 
	{
		ArrayList<Army> attackArmiesInArea = new ArrayList<Army>();
		Iterator<Army> it = armies.iterator();
		while (it.hasNext())
		{
			Army army = it.next();
			if (army.getArea() == area && army.getOwner() != area.getOwner())
				attackArmiesInArea.add(army);
		}
		return attackArmiesInArea;
	}
	
	
	public Map<String,Map<String,List<Army>>> getArmiesInCountryList()
	{
		HashMap<String,Map<String,List<Army>>> armiesPerCountryAndUser = new HashMap<String, Map<String,List<Army>>>();
		Iterator<Army> it = armies.iterator();
		while (it.hasNext())
		{
			Army army = it.next();
			String armyArea = army.getAreaId();
			String armyOwner = army.getOwner().getName();
			Map<String,List<Army>> armiesPerUserMap = armiesPerCountryAndUser.get(armyArea);
			if (armiesPerUserMap == null) {
				armiesPerUserMap = new HashMap<String,List<Army>>();
				armiesPerUserMap.put(armyOwner, new ArrayList<Army>(Arrays.asList(army)));
				armiesPerCountryAndUser.put(armyArea, armiesPerUserMap);
			} else {
				List<Army> armiesOfUser = armiesPerUserMap.get(armyOwner);
				if (armiesOfUser == null)
					armiesPerUserMap.put(armyOwner, new ArrayList<Army>(Arrays.asList(army)));
				else
					armiesOfUser.add(army);
			}
		}	
		return armiesPerCountryAndUser;
	}
	

	public void printArmiesPerCountry(Map<String,Map<String,List<Army>>> armiesPerCountryAndUserMap)
	{
		System.out.print("\n");
		Iterator<Entry<String, Map<String, List<Army>>>> itCountries = armiesPerCountryAndUserMap.entrySet().iterator();
		while (itCountries.hasNext())
		{
			Entry<String, Map<String, List<Army>>> armiesPerCountryAndUserEntry = itCountries.next();
			System.out.print(armiesPerCountryAndUserEntry.getKey()+": ");
			Iterator<Entry<String, List<Army>>> itUsers = armiesPerCountryAndUserEntry.getValue().entrySet().iterator();
			while (itUsers.hasNext())
			{
				Entry<String, List<Army>> armiesPerUserEntry = itUsers.next();
				System.out.print(armiesPerUserEntry.getKey()+": "+armiesPerUserEntry.getValue().size());
				if (itUsers.hasNext())
					System.out.print(", ");
				else
					System.out.print("\n");
			}
		}
		System.out.print("\n");
	}

	
	public void printArmiesPerPlayer(List<Player> players) 
	{
		System.out.println("");
		Iterator<Player> itPlayer = players.iterator();
		while (itPlayer.hasNext())
		{
			Player player = itPlayer.next();
			System.out.println(player.getName()+":");
			Iterator<Army> it = armies.iterator();
			while (it.hasNext())
			{
				Army army = it.next();
				if (army.getOwner() == player)
					System.out.println("  "+army.getAreaId()+" "+army.getId()+" "+army.getStrength());
			}			
		}
	}
	
//	public List<Army> getDefendingArmiesIn(Area area) 
//	{
//		ArrayList<Army> attackArmiesInArea = new ArrayList<Army>();
//		Iterator<Army> it = armies.iterator();
//		while (it.hasNext())
//		{
//			Army army = it.next();
//			if (army.getArea() == area && army.getOwner() == area.getOwner())
//				attackArmiesInArea.add(army);
//		}
//		return attackArmiesInArea;
//	}
	

	public Area getAreaAttacked() 
	{
		return areaAttacked;
	}

	public Area getAreaAttackOrigin() 
	{
		return areaAttackOrigin;
	}

	public void resetAttack()
	{
		areaAttacked = null;
		areaAttackOrigin = null;
	}
	
	public boolean isAttackOngoing()
	{
		return (areaAttacked != null && getAttackingArmiesIn(areaAttacked).size() > 0);		
	}
	
	public boolean deleteArmy(Army army) 
	{
		return armies.remove(army);			
	}
	
}

package risiko.game.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.Random;
import risiko.game.common.GeoPos;
import risiko.game.domain.Attack.DicesThrow;
import risiko.game.service.ArmyInterface;
import risiko.game.service.AttackInterface;
import risiko.game.service.GameInterface;
import risiko.game.service.GameState;
import risiko.game.service.MoveInterface;
import risiko.game.service.PlayerState;

public class Game extends GameInterface implements GameState
{
    private UUID id;
    private LocalDateTime createdOn;
    private String title;
    private String boardName = "default";

    private ArrayList<Player> players = new ArrayList<>();
    private Board board = null;

    int index = -1;
    int indexUpdate = 0;
    int gameRound = 0;
    private int activePlayerIdx = -1;
    private GamePhase phase = GamePhase.JOIN;
    String message = null;    
    ArrayList<MoveInterface> moves = new ArrayList<MoveInterface>();
    Attack attack = null;
    Area areaConquered = null;
    Area areaConqueredFrom = null;

    
    Random rand = new Random();

    
    private final int   maxNumberOfPlayer = 5;
    private final int[] initialNumberOfArmiesDefault = { 0, 0, 
		40, // two players
		35, // three players
		30, // four players
    	25  // five players
//		22, // two players
//		15, // three players
//		10, // four players
//    	5  // five player
	};
    private int initialNumberOfArmies = -1;


    public Game(String title) throws Exception {
        this.title = title;
        this.id = UUID.randomUUID();
        this.phase = GamePhase.JOIN;
        this.createdOn = LocalDateTime.now();
        this.board = new Board(boardName);
    }

    public Game(String title, List<String> players, String boardName, int initialNumberOfArmies) throws Exception {
        this.title = title;
        this.id = UUID.randomUUID();
        this.phase = GamePhase.JOIN;
        this.createdOn = LocalDateTime.now();
        this.board = new Board(boardName);
        for (Iterator<String> it = players.iterator(); it.hasNext();)
        	addPlayer(it.next(), UUID.randomUUID());
        this.activePlayerIdx = 0;
        this.initialNumberOfArmies = initialNumberOfArmies;
    }

    
    public Game(GameState state) throws Exception {
        this.id = state.getId();
        this.index = state.getIndex();
        this.indexUpdate = state.getIndexUpdate();
        this.title = state.getTitle();
        this.createdOn = state.getCreatedOn();
        this.boardName = state.getBoardName();
        
        this.phase = state.getPhase();
        this.gameRound = state.getGameRound();

        this.board = new Board(boardName);
        
        for (Iterator<PlayerState> it=state.getPlayers().iterator(); it.hasNext();) {
        	PlayerState playerState = it.next();
        	this.players.add(new Player(playerState, 
        								this.players.size(), 
        								board.getMissionById(playerState.getMissionId()), 
        								this));
        }
        this.activePlayerIdx = getPlayerByName(state.getActivePlayerName()).getIndex();

        for (ArmyInterface army: state.getArmies())
        	this.board.addArmy(army, getPlayerByColor(army.getOwnerColor()));        	
        
        if (state.getArmyReserve() != null)
        	this.board.addArmy(state.getArmyReserve(), getPlayerByColor(state.getArmyReserve().getOwnerColor()));
        
        this.message = state.getMessage();
        this.areaConquered = board.getArea(state.getAreaConqueredId());
        this.areaConqueredFrom = board.getArea(state.getAreaConqueredFromId());

        for (MoveInterface move: state.getArmyMoves())
        	this.moves.add(new Move(move, board));
        
        if (state.getAttack() != null)
        	this.attack = new Attack(state.getAttack(), board, this);
    }

    
    
    public int getIndex() {
    	return this.index;
    }
    
    public int getIndexUpdate() {
    	return this.indexUpdate;
    }
    
    public void setIndex(int index) {
    	this.index = index;
    }
    
    public void setIndexUpdate(int indexUpdate) {
    	this.indexUpdate = indexUpdate;
    }

    public String getBoardName() {
    	return boardName;
    }

    public UUID getId() {
        return id;
    }

    public int getGameRound() {
    	return gameRound;
    }
    
    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public GamePhase getPhase() {
        return phase;
    }

    public String getTitle() {
        return title;
    }
        
    public String getActivePlayerName() {
    	Player player = getActivePlayer();
    	if (player != null)
    		return player.getName();
    	else
    		return "";
    }

    public Player getPlayer(UUID playerId) {
    	for (Player player : players) 
    		if (player.getId().equals(playerId))
    			return player;
		return null;
	}

    public Player getPlayerByColor(String color) {
    	for (Player player : players) 
    		if (player.getColor().equals(color))
    			return player;
		return null;
	}

    public Player getPlayerByName(String name) {
    	for (Player player : players) 
    		if (player.getName().equals(name))
    			return player;
		return null;
	}

    public Player getActivePlayer() {
    	if (activePlayerIdx >= 0 && activePlayerIdx < players.size())
    		return players.get(activePlayerIdx);
    	else
    		return null;
	}

    public List<String> getPlayerNames() {
    	ArrayList<String> playerNames = new ArrayList<String>(); 
    	for (Player player : players)
    		playerNames.add(player.getName());
        return playerNames;
    }
    
    public List<PlayerState> getPlayers() {
        return Collections.unmodifiableList(players);
    }

    public AttackInterface getAttack() {
    	return this.attack;
    }
    
    public String getAreaAttacked() {
    	return board.getAreaAttacked() != null ? board.getAreaAttacked().getId() : ""; 
    }
    
    public String getMessage() {
    	return message;
    }

    public String getWinner() {
    	if (phase == GamePhase.END)
    		return getActivePlayer().getName();
    	else
    		return "";
    }
    
    public String getWinnerMission() {
    	if (phase == GamePhase.END)
    		return getActivePlayer().getMissionDescription();
    	else
    		return "";
    }
    
	@Override
	public List<ArmyInterface> getArmies() {
		return new ArrayList<ArmyInterface>(board.getArmies());
	}

    public Army getArmyReserve() {
    	return board.getArmyReserve();
    }
    
    public List<MoveInterface> getArmyMoves() {
		return moves;
    }
    
    public void insertArmyMoves(List<MoveInterface> moves) {
    	ArrayList<MoveInterface> movesNew = new ArrayList<MoveInterface>();
    	movesNew.addAll(moves);
    	movesNew.addAll(this.moves);
		this.moves = movesNew;
    }

    public String getAreaConqueredId() {
    	return areaConquered != null ? areaConquered.getId() : "";
    }
    
    public String getAreaConqueredFromId() {
    	return areaConqueredFrom != null ? areaConqueredFrom.getId() : "";    	
    }

    public boolean armyMovesWithinArea() {
    	if (!moves.isEmpty()) {
    		for (MoveInterface move: this.moves)
    			if (!move.withinArea()) return false;
    		return true;
    	}
		return false;
    }
    
    public boolean hasSameSrcAndDst(List<MoveInterface> otherMoves) {
    	if (!moves.isEmpty() && !otherMoves.isEmpty()) {
    		String srcAreaId = null;
    		String dstAreaId = null;
    		for (MoveInterface move: this.moves) {
    			if (dstAreaId == null) {
    				srcAreaId = move.getArmyOldAreaId();
    				dstAreaId = move.getArmyNewAreaId();
    			}
    			else {
    				if (!srcAreaId.equals(move.getArmyOldAreaId()) || !dstAreaId.equals(move.getArmyNewAreaId())) 
    					return false;
    			}
    		}
    		for (MoveInterface move: otherMoves) {
    			if (dstAreaId == null) {
    				srcAreaId = move.getArmyOldAreaId();
    				dstAreaId = move.getArmyNewAreaId();
    			}
    			else {
    				if (!srcAreaId.equals(move.getArmyOldAreaId()) || !dstAreaId.equals(move.getArmyNewAreaId())) 
    					return false;
    			}
    		}
    		return true;
    	}
		return false;    	
    }

    //------------------------------------------------------------------------------------------------------
    
    

    public Player addPlayer(String playerName, UUID playerId) 
    {
    	if (playerName.isEmpty())
    		return null;
    	
    	for (Player player : players)
    		if (player.getName().equals(playerName))
    			return player;
	    	
        if(GamePhase.JOIN == phase && players.size() < maxNumberOfPlayer)
        {
	        Player player = new Player(playerName, playerId, players.size(), this);
	        players.add(player);
	        
	        return player;
        }
        return null;
    }

    
    public boolean startGame(UUID playerId) 
    {
    	if (phase != GamePhase.JOIN)
    		return false;

    	int cntStarted = 0;
    	for (Player player : players) 
    	{
    		if (!player.getColor().isEmpty()) {
    			++cntStarted; 
    		}
    		else if (player.getId().equals(playerId)) {
    			++cntStarted; 
    			player.setColor();
    		}
    	}
    	
    	if (cntStarted == players.size() && players.size() >= 2) 
    	{
    		if (initialNumberOfArmies < 0)
    			initialNumberOfArmies = initialNumberOfArmiesDefault[players.size()];
	    	for (Player player : players)
	    		player.incArmyReserve(initialNumberOfArmies);

            board.initPlayerArmies(players);
			board.assignMissions(players);
			
            activePlayerIdx = Math.abs(rand.nextInt(players.size()));
            
        	if (countArmyReserves() == 0)
        	{
        		System.out.println("all armies placed; start game");
				gameRound = 1;
				addArmyReserveFor(getActivePlayer());
        	}
        	else 
        	{
    			do {
    				activePlayerIdx = (++activePlayerIdx) % players.size();
    			}
    			while(getActivePlayer().getArmyReserveCount() == 0);
    			
                phase = GamePhase.INIT;
                index = 0;
                board.createArmyReserve(players.get(activePlayerIdx));        		
        	}

        	
            return true;
    	}
    	
    	return false;
    }

    
    void setGameEnd(Player player) 
    {
		this.phase = GamePhase.END;
		this.message = player.getName()+" hat seine Mission erfüllt!";
		player.setWinner();
		System.out.println("\nGame End! "+this.message);    	
    }
    
    
    public boolean nextPlayer(UUID playerId)
    {
    	Player player = getPlayer(playerId);
    	if (player == null) 
    	{
    		return false; // unknown player
    	}
    	
    	if (player != getActivePlayer()) 
    	{
    		System.out.println("next failed: not active player");
    		return false; // only active player can pass on to the next player
    	}
    	    	
    	if (!(phase == GamePhase.REDEEM || phase == GamePhase.ATTACK || phase == GamePhase.MOVE))
		{
    		System.out.println("next failed: invalid phase");
			return false; 	// placing reserve not finished
		}
    	
    	if (phase == GamePhase.REDEEM && player.getVictoryCards().size() >= 5) {
    		System.out.println("next failed: force redeem cards");
    		return false;
    	}
    	
    	if (board.isAttackOngoing())
    	{
    		System.out.println("next failed: attack not finished");
    		return false; 	// attack not finished
    	}
    	
    	this.message = null;
    	
		this.attack = null;
    	resetAttack();
    	
		int prevPlayerIdx = activePlayerIdx;
		Player nextPlayer = null;
		for(;;) {
        	activePlayerIdx = (++activePlayerIdx) % players.size();
    		nextPlayer = getActivePlayer();
			if (nextPlayer.index == 0)
				++gameRound;
    					
			if (board.getArmiesOf(nextPlayer).size() > 0) 
    			break;

    		if (activePlayerIdx == prevPlayerIdx) {
    			// current player is the last remaining player
    			setGameEnd(player);
    			return true;
    		}
		}

    	if (board.checkMissionAchieved(nextPlayer))
    	{
    		setGameEnd(nextPlayer);
    		return true;
    	}

    	addArmyReserveFor(nextPlayer);    		
		System.out.println("next player: "+nextPlayer.getName()+", round: "+gameRound);
		return true;
    }

    
    public boolean redeemCards(UUID playerId)
    {
    	Player player = getPlayer(playerId);
    	if (player != null) 
    	{
    		int armyUpgrade = player.redeemVictoryCards();
    		if (armyUpgrade > 0 && phase == GamePhase.REDEEM) 
    		{
    			message = player.getName()+" tauscht Gewinnkarten: +"+armyUpgrade+" Armeen"; 
    			board.createArmyReserve(player);
    			phase = GamePhase.UPGRADE;
        		return true;
    		}
    	}
		return false;
    }

    
    public boolean executeAttack(UUID playerId, float deltaGeoPos)
    {
    	Player player = getPlayer(playerId);
    	if (player == null) 
    	{
    		return false; // unknown player
    	}
    	
    	if (player != getActivePlayer()) 
    	{
    		return false; // only active player can pass on to the next player
    	}
    	
    	if (phase != GamePhase.ATTACK || !board.isAttackOngoing())
		{
			return false; // no attack ongoing
		}

    	Area areaAttacked = board.getAreaAttacked();
    	Player playerAttacking = player;
    	Player playerDefending = areaAttacked.getOwner();
    	
    	this.moves.clear();
    	
    	
    	// ungroup armies before attack:
    	
    	for (Army army: board.getArmiesIn(areaAttacked, playerAttacking))
    		if (army.getStrength() > 1)
    			this.moves.addAll(ungroupArmies(army, 1, deltaGeoPos));
    	for (Army army: board.getArmiesIn(areaAttacked, playerDefending))
    		if (army.getStrength() > 1)
    			this.moves.addAll(ungroupArmies(army, 1, deltaGeoPos));

    	
    	// get attacking armies:
    	
        this.attack = new Attack(areaAttacked.getId(), player.getName(), areaAttacked.getOwner().getName());
    	
    	List<Army> armiesAttacking = board.getArmiesIn(areaAttacked, playerAttacking);
    	List<Army> armiesDefending = board.getArmiesIn(areaAttacked, playerDefending);
    	attack.armyStrengthInAttackingArea = board.getArmyStrengthIn(areaAttacked, playerAttacking);
    	attack.armyStrengthInDefendingArea = board.getArmyStrengthIn(areaAttacked, playerDefending);
    	{
    		System.out.println("attacking: ");
    		Iterator<Army> itAtk = armiesAttacking.iterator(); 
    		while (itAtk.hasNext()) { Army a = itAtk.next(); System.out.println(a.getOwner().getName()+", "+a.getId()); }  		
    		System.out.println("defending: ");
    		Iterator<Army> itDef = armiesDefending.iterator();
    		while (itDef.hasNext()) { Army a = itDef.next(); System.out.println(a.getOwner().getName()+", "+a.getId()); }		
    	}
    	
    	// execute attack:
    	do {
    		DicesThrow dicesThrow = new DicesThrow();
    		attack.dicesThrows.add(dicesThrow);
    		dicesThrow.dicesDefending = throwDices(Math.min(2, armiesDefending.size()));
    		dicesThrow.dicesAttacking = throwDices(Math.min(3, armiesAttacking.size()));
    		List<Army> armiesDefendingThrow = new ArrayList<Army>();
    		List<Army> armiesAttackingThrow = new ArrayList<Army>();
    		for (int i=0; i < Math.min(dicesThrow.dicesDefending.size(), dicesThrow.dicesAttacking.size()); ++i)
    		{
    			// get two closest armies (just for fun):
    			Army armyDefender = null;
    			Army armyAttacker = null;
    			float distMin = Float.MAX_VALUE;
        		Iterator<Army> itDef = armiesDefending.iterator();
        		while (itDef.hasNext()) {
        			Army def = itDef.next();
        			if (!armiesDefendingThrow.contains(def)) {
	        			Iterator<Army> itAtk = armiesAttacking.iterator();
	        			while (itAtk.hasNext()) {
	        				Army atk = itAtk.next();
	        				if (!armiesAttackingThrow.contains(atk)) {
		        				float dist = atk.distLonLatSqu(def);
		            			if (dist < distMin) {
		            				armyDefender = def;
		            				armyAttacker = atk;
		            				distMin = dist;
		            			}
	        				}
	            		}
        			}
        		}
        		armiesDefendingThrow.add(armyDefender);
        		armiesAttackingThrow.add(armyAttacker);
    		
    			if (dicesThrow.dicesAttacking.get(i) > dicesThrow.dicesDefending.get(i))
    			{
    				// attacker wins:
    				dicesThrow.armiesLostDefending.add(armyDefender);
    				dicesThrow.armiesNotLost.add(armyAttacker);
    				armiesDefending.remove(armyDefender);
    				board.deleteArmy(armyDefender);
    				System.out.println("delete defender army: "+armyDefender.getId()+", attacker: "+armyAttacker.getId());
    			}
    			else
    			{
    				// defender wins:
    				dicesThrow.armiesLostAttacking.add(armyAttacker);
    				dicesThrow.armiesNotLost.add(armyDefender);
    				armiesAttacking.remove(armyAttacker);
    				board.deleteArmy(armyAttacker);
    				System.out.println("delete attacker army: "+armyAttacker.getId()+", defender: "+armyDefender.getId());
    			}
    		}

    		for (int i=dicesThrow.dicesDefending.size(); i < dicesThrow.dicesAttacking.size(); ++i) 
    		{
    			Army armyAttacker = null;
    			float distMin = Float.MAX_VALUE;
    			Iterator<Army> itAtk = armiesAttacking.iterator();
    			while (itAtk.hasNext()) {
    				Army atk = itAtk.next();
    				if (!armiesAttackingThrow.contains(atk) && !dicesThrow.armiesNotLost.contains(atk)) {
	            		Iterator<Army> itDef = armiesDefendingThrow.iterator();
	            		while (itDef.hasNext()) {
	            			Army def = itDef.next();
	        				float dist = atk.distLonLatSqu(def);
	            			if (dist < distMin) {
	            				armyAttacker = atk;
	            				distMin = dist;
	            			}
	            		}
    				}
        		}
    			if (armyAttacker != null)
    				dicesThrow.armiesNotLost.add(armyAttacker);
			}
    		for (int i=dicesThrow.dicesAttacking.size(); i < dicesThrow.dicesDefending.size(); ++i) 
    		{
    			Army armyDefender = null;
    			float distMin = Float.MAX_VALUE;
        		Iterator<Army> itDef = armiesDefending.iterator();
        		while (itDef.hasNext()) {
        			Army def = itDef.next();
        			if (!armiesDefendingThrow.contains(def) && !dicesThrow.armiesNotLost.contains(def)) {
		    			Iterator<Army> itAtk = armiesAttackingThrow.iterator();
		    			while (itAtk.hasNext()) {
		    				Army atk = itAtk.next();
	        				float dist = atk.distLonLatSqu(def);
	            			if (dist < distMin) {
	            				armyDefender = def;
	            				distMin = dist;
	            			}
	            		}
        			}
        		}
    			if (armyDefender != null)
    				dicesThrow.armiesNotLost.add(armyDefender);
			}

    		System.out.println("");
    		System.out.println("throw "+attack.dicesThrows.size()+": attacking: "+dicesThrow.dicesAttacking.size()+", defending: "+dicesThrow.dicesDefending.size());
    		System.out.println("  armiesLostDefending:"); for (Iterator<ArmyInterface> it = dicesThrow.armiesLostDefending.iterator(); it.hasNext(); ) System.out.println("    "+it.next().getId());
    		System.out.println("  armiesLostAttacking:"); for (Iterator<ArmyInterface> it = dicesThrow.armiesLostAttacking.iterator(); it.hasNext(); ) System.out.println("    "+it.next().getId());
    		System.out.println("  armiesNotLost:"); for (Iterator<ArmyInterface> it = dicesThrow.armiesNotLost.iterator(); it.hasNext(); ) System.out.println("    "+it.next().getId());
    		System.out.println("");
    	}
    	while (armiesAttacking.size() > 0 && armiesDefending.size() > 0);

    	if (armiesAttacking.size() > 0)
    	{
    		// attacker wins -> change owner of area:
    		areaAttacked.setOwner(player);
    		Iterator<Army> itArmy = armiesAttacking.iterator();
    		while (itArmy.hasNext())
    			itArmy.next().setOriginArea(areaAttacked);
    		
    		player.addVictoryCard(gameRound);
    		
    		areaConquered = areaAttacked;
    		areaConqueredFrom = board.getAreaAttackOrigin();
    	}
    	
    	board.resetAttack();
    	this.message = null;
    	
    	return true;
    }
    
    
    
    private List<Integer> throwDices(int nbr)
    {
    	List<Integer> dices = new ArrayList<Integer>();
    	for (int i=0; i < nbr; ++i)
    		dices.add(rand.nextInt(6)+1);
    	Collections.sort(dices, Collections.reverseOrder());
    	return dices;
    }
    
        
    private void resetAttack()
    {
    	areaConquered = null;
    	areaConqueredFrom = null;
    }
    
    
	private void addArmyReserveFor(Player player)
	{
		// assign new reserve armies to player:
		int cntAreas = board.getAreas(player).size();
		System.out.println("army reserve for "+player.getName()+":");
		int cntUpgrade = Math.max(cntAreas/3, 3);
		System.out.println("  for areas: "+cntUpgrade);
		for (String continent: board.getContinents(player)) {
			int continentValue = board.getContinentValue(continent);
			System.out.println("  for continent "+continent+": "+continentValue);			
			cntUpgrade += continentValue; 
		}
		player.incArmyReserve(cntUpgrade);
		System.out.println("  total: "+player.getArmyReserveCount());
		
		// add next reserve army to be placed
		board.createArmyReserve(player);

		phase = GamePhase.UPGRADE;
	}
    
        
    private int countArmyReserves() {
        int cnt = 0;
    	for (Player player : players)
    		cnt += player.getArmyReserveCount();
    	return cnt;
    }


    public boolean groupArmies(UUID playerId, UUID armyId, int strength)
    {
    	Player activePlayer = getActivePlayer();

    	Army army = board.getArmy(armyId);
		Player player = getPlayer(playerId);
		boolean isReserve = board.isArmyReserve(army);		
    	if (!(phase == GamePhase.INIT || phase == GamePhase.UPGRADE || phase == GamePhase.REDEEM || phase == GamePhase.ATTACK || phase == GamePhase.MOVE) || 
    		activePlayer == null || army == null || player == null || player != activePlayer ||
    		army.getOwner() != player || 
    		strength <= 1 || 
    		isReserve || army.getStrength() != 1)
    	{
    		return false; // game not started, army or player unknown, or modifying someone else' army, or army is reserve army
    	}

    	List<Army> armiesInArea = board.getArmiesIn(army.getArea());
		if (strength-1 > armiesInArea.size())
			return false;	// too few armies in area for group size

		List<Army> armiesInGroup = new ArrayList<Army>();
		for (int i=1; i < strength; ++i)
		{
			// find closest army
			Army armyClosest = null;
			float distMin = Float.MAX_VALUE; 
			Iterator<Army> it = armiesInArea.iterator();
			while (it.hasNext()) {
				Army a = it.next();
				if (a != army && !armiesInGroup.contains(a) && a.getGroup() < 0 && a.getStrength() == 1) {
					float dist = army.distLonLatSqu(a);
					if (dist < distMin) {
						distMin = dist;
						armyClosest = a;
					}
				}
			}
			// add to group
			if (armyClosest != null)
				armiesInGroup.add(armyClosest);
		}

		if (armiesInGroup.size() < strength-1) // too few to group to strength
			return false;

		this.attack = null;
		this.moves.clear();
		army.setStrength(strength);
		System.out.println("upgrade army: "+army.getId()+": strength: "+army.getStrength());
		for (Iterator<Army> it = armiesInGroup.iterator(); it.hasNext();) {
			Army a = it.next();
			moves.add(new Move(a, a.getArea(), a.getPos(), a.getStrength()));
			a.setPos(army.getPos());
			a.setArea(null);
			a.setStrength(0);
			System.out.println("remove army: "+a.getId());
			board.deleteArmy(a);
		}
		
		return true;
    }
    
    public boolean ungroupArmies(UUID playerId, UUID armyId, int strength, float deltaGeoPos)
    {
    	Army army = board.getArmy(armyId);
		Player player = getPlayer(playerId);
		
    	Player activePlayer = getActivePlayer();
		boolean isReserve = board.isArmyReserve(army);	
    	if (!(phase == GamePhase.INIT || phase == GamePhase.UPGRADE || phase == GamePhase.REDEEM || phase == GamePhase.ATTACK || phase == GamePhase.MOVE) || 
    		activePlayer == null || army == null || player == null ||
    		army.getOwner() != player || 
    		army.getStrength() <= 1 || 
    		isReserve)
    	{
    		return false; // game not started, army or player unknown, or modifying someone else' army, or army is reserve army
    	}
		
		this.attack = null;
    	this.moves = new ArrayList<MoveInterface>(ungroupArmies(army, strength, deltaGeoPos));
		
		return true;
    }
    
    private List<Move> ungroupArmies(Army army, int strength, float deltaGeoPos)
    {
    	ArrayList<Move> moves = new ArrayList<Move>();
		while (army.getStrength()-strength > 0) 
		{
			Army armyNew = board.createArmy(army.getPos(), army.getOwner(), army.getArea(), army.getOriginArea(), strength);
			army.setStrength(army.getStrength()-strength);
			moves.add(new Move(armyNew, armyNew.getArea(), army.getPos(), 0));
		}

		float dirDelta = moves.size() > 1 ? (360.0f/moves.size()) : 0.f; 
		for (int i = 0; i < moves.size(); ++i) {
			Army a = moves.get(i).army;
			a.setPos(a.getPos().movedTo(i*dirDelta, deltaGeoPos));
		}

		return moves;
    }
    
    
    public boolean moveArmy(UUID playerId, UUID armyId, String area, GeoPos pos, int strength) 
    {
    	Player activePlayer = getActivePlayer();
    	
		Army army = board.getArmy(armyId);
		Player player = getPlayer(playerId);
    	if (!(phase == GamePhase.INIT || phase == GamePhase.UPGRADE || phase == GamePhase.REDEEM || phase == GamePhase.ATTACK || phase == GamePhase.MOVE) || 
    		activePlayer == null || army == null || player == null ||
    		army.getOwner() != player)
    	{
    		return false; // game not started, army or player unknown, or moving someone else' army
    	}
		
		boolean isActivePlayer = activePlayer.getId().equals(playerId);
		if (!isActivePlayer)
		{
	    	// moves of inactive players (only within their own countries):
			Move move = board.moveArmy(army, area, pos, false, false);
			if (move != null) 
			{
				this.attack = null;
				this.moves = new ArrayList<MoveInterface>(Arrays.asList(move));
				return true;
			}
			return false;
		}
		else
		{
			// move army of active players:
			boolean isReserve = board.isArmyReserve(army);		
			boolean allowAttacks = (phase==GamePhase.ATTACK || phase==GamePhase.REDEEM) && 
								   isActivePlayer && 
								   player.getArmyReserveCount()==0 && player.getArmyReserveCount() < 6 &&
								   !isReserve;
			boolean allowMoveToNeigbour = (phase==GamePhase.ATTACK || phase==GamePhase.MOVE || phase==GamePhase.REDEEM) && 
										  isActivePlayer && 
										  player.getArmyReserveCount()==0 && player.getArmyReserveCount() < 6;
			
			if (isReserve)
			{
				if (strength-1 > activePlayer.getArmyReserveCount())
					return false;

				army.setStrength(strength);
			}

			Move move = board.moveArmy(army, area, pos, allowMoveToNeigbour, allowAttacks);
			if (move == null)
			{
				return false;  // move not allowed
			}
			this.moves = new ArrayList<MoveInterface>(Arrays.asList(move));
			this.attack = null;

	    	if (isReserve) 
			{
	    		// reserve army was placed in area
    			player.decArmyReserve(move.army.strength);  // decrement reserve of player 
			}
	    	
	    	this.message = null;
	    	
			if (phase == GamePhase.INIT)
			{
	        	if (countArmyReserves() == 0)
	        	{
	        		// all armies placed on board; let's start
					activePlayerIdx = 0;
					gameRound = 1;
					addArmyReserveFor(getActivePlayer());
	        	}
	        	else if (isReserve)
				{
					do {
						activePlayerIdx = (++activePlayerIdx) % players.size();
					}
					while(getActivePlayer().getArmyReserveCount() == 0);
					
					board.createArmyReserve(getActivePlayer());
				}
			}
	    	else if (phase == GamePhase.UPGRADE)
			{
	    		if (player.getArmyReserveCount() > 0) 
	    		{
					board.createArmyReserve(getActivePlayer());
	    		}
	    		else // all reserve armies placed:
	    		{
	    			if (player.getVictoryCardsValue() > 0) {
	    				this.phase = GamePhase.REDEEM;
		    			if (player.getVictoryCards().size() >= 5)
		    				redeemCards(playerId);
	    			}
	    			else
	    				this.phase = GamePhase.ATTACK;
	    		}
			}
	    	else if (phase == GamePhase.ATTACK || phase == GamePhase.REDEEM)
			{
	    		if (phase == GamePhase.REDEEM) // if no victory cards are redeemed ...
	    			phase = GamePhase.ATTACK;  // ... go on to attack phase
	    			
	    		if (move.army.getOriginArea().getOwner() == move.army.getArea().getOwner())
	    		{
	    			if (move.army.getOriginArea() != move.army.getArea())
	    			{
	    				if (areaConquered != null &&							// after attack ... 
	    					move.army.getArea() == areaConquered &&				// ... allow armies moving to the conquered area
	    					move.army.getOriginArea() == areaConqueredFrom)		// before ending the attack phase;
	    				{
	    					move.army.setOriginArea(army.getArea());
	    				}
	    				else
	    				{
	    					resetAttack(); 							    					
	    					move.army.setOriginArea(move.army.getArea());
	    					phase = GamePhase.MOVE; 							// no more attack possible 
	    				}
	    			}
	    			else if (!board.isAttackOngoing()) 
	    			{
						resetAttack();    				
	    			}
	    		}
	    		else 
	    		{
	    	    	areaConquered = null;		// reset previous attack
	    	    	areaConqueredFrom = null;
	    		}
			}
	    	else if (phase == GamePhase.MOVE)
			{
				resetAttack();
				if (move.army.getOriginAreaId() != move.army.getAreaId()) 
				{
					move.army.setOriginArea(move.army.getArea());
				}
			}
			
	    	board.printArmiesPerPlayer(players);
	    	
	    	return true;
		}
    }
    
}

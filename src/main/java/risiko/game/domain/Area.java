package risiko.game.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import risiko.game.common.GeoPos;

public class Area implements Cloneable
{	
	private String id = "";
	private String name = "";
	private String continent = "";
	private List<Area> neighbours = new ArrayList<>();
	private Player owner = null;

	public Area(String id) {
		this.id = id;
	}

	public Area(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public Area(String id, String name, GeoPos pos, List<Area> neighbours) {
		this.id = id;
		this.name = name;
		this.neighbours = neighbours;
	}


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Area(id, name);
    }
    
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getContinent() {
		return continent;
	}
	
	public void setContinent(String continent) {
		this.continent = continent;
	}

//	public GeoPos getPos() {
//		return pos;
//	}

	public List<Area> getNeighbours() {
		return neighbours;
	}

	public boolean isNeighbour(Area area) {
		Iterator<Area> it = neighbours.iterator();
		while(it.hasNext())
			if (it.next() == area)
				return true;
		Iterator<Area> it2 = area.neighbours.iterator();
		while(it2.hasNext())
			if (it2.next() == this)
				return true;
		return false;
	}
	
	public void addNeighbour(Area neighbourArea) {
		Iterator<Area> it = neighbours.iterator();
		while(it.hasNext()) 
			if (it.next().getId().contentEquals(neighbourArea.getId()))
				return;
		neighbours.add(neighbourArea);
	}
	
	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}
	
}

package risiko.game.domain;

import java.util.List;

public class Mission 
{
	String id; 
	String description;
	List<String> continents;
	int countContinents;
	int countAreas;
	String playerToDestroyColor;	
	int countArmiesPerArea;
	int countPlayerMin;
	
	public Mission(String id, 
			String description, 
			List<String> continents, 
			int countContinents, 
			int areasCount, 
			String playerToDestroyColor,
			int countArmiesPerArea, 
			int countPlayerMin) {
		this.id = id;
		this.description = description;
		this.continents = continents;
		this.countContinents = countContinents;
		this.countAreas = areasCount;
		this.playerToDestroyColor = playerToDestroyColor;
		this.countArmiesPerArea = countArmiesPerArea;
		this.countPlayerMin = countPlayerMin;
	}
	
	public String getId() {
		return id;
	}
	public String getDescription() {
		return description;
	}
	public List<String> getContinents() {
		return continents;
	}
	public int getCountContinents() {
		return countContinents;
	}
	public int getCountAreas() {
		return countAreas;
	}
	public String getPlayerToDestroyColor() {
		return playerToDestroyColor;
	}
	public int getCountArmiesPerArea() {
		return countArmiesPerArea;
	}
	public int getCountPlayerMin() {
		return countPlayerMin;
	}

	public boolean hasContinents(List<String> playerContinents) 
	{
		if (this.continents.isEmpty()) 
			return true;
		
		for (String missionContinent: this.continents) {
			boolean hasContinent = false;
			for (String playerContinent: playerContinents)
				if (missionContinent.contentEquals(playerContinent)) {
					hasContinent = true;
					break;
				}
			if (!hasContinent) return false;
		}
		return true;
	}

}

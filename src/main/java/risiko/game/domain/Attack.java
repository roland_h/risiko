package risiko.game.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import risiko.game.service.ArmyInterface;
import risiko.game.service.AttackInterface;


public class Attack implements AttackInterface {

	public static class DicesThrow implements risiko.game.service.AttackInterface.DicesThrow, Cloneable {
	    public List<Integer> dicesAttacking = new ArrayList<>();
	    public List<Integer> dicesDefending = new ArrayList<>();
	    public List<ArmyInterface> armiesLostAttacking = new ArrayList<ArmyInterface>();
	    public List<ArmyInterface> armiesLostDefending = new ArrayList<ArmyInterface>();
	    public List<ArmyInterface> armiesNotLost = new ArrayList<ArmyInterface>();
	    
	    public DicesThrow() {
	    }
	    
	    public DicesThrow(AttackInterface.DicesThrow dicesThrow, Board board, Game game) {
    		for (Iterator<ArmyInterface> itArmy = dicesThrow.getArmiesLostAttacking().iterator(); itArmy.hasNext();)
    			this.armiesLostAttacking.add(getArmy(itArmy.next(), board, game));
    		for (Iterator<ArmyInterface> itArmy = dicesThrow.getArmiesLostDefending().iterator(); itArmy.hasNext();)
    			this.armiesLostDefending.add(getArmy(itArmy.next(), board, game));
    		for (Iterator<ArmyInterface> itArmy = dicesThrow.getArmiesNotLost().iterator(); itArmy.hasNext();)
    			this.armiesNotLost.add(getArmy(itArmy.next(), board, game));
    		this.dicesAttacking.addAll(dicesThrow.getDicesAttacking());
    		this.dicesDefending.addAll(dicesThrow.getDicesDefending());
	    }
	    
	    Army getArmy(ArmyInterface armyState, Board board, Game game) {
	    	Army army = board.getArmy(armyState.getId());
	    	if (army == null)
	    		army = new Army(armyState, 
	    						board.getArea(armyState.getAreaId()), 
	    						board.getArea(armyState.getOriginAreaId()), 
	    						game.getPlayerByColor(armyState.getOwnerColor()));
	    	return army;
	    }

	    public List<Integer> getDicesAttacking() { return dicesAttacking; }
	    public List<Integer> getDicesDefending() { return dicesDefending; }
	    public List<ArmyInterface> getArmiesLostAttacking() { return armiesLostAttacking; }
	    public List<ArmyInterface> getArmiesLostDefending() { return armiesLostDefending; }
	    public List<ArmyInterface> getArmiesNotLost() { return armiesNotLost; }
	}
	
	public String areaId;
	public String attackingPlayerName;
	public String defendingPlayerName;
	public List<risiko.game.service.AttackInterface.DicesThrow> dicesThrows = new ArrayList<>();
	public int armyStrengthInAttackingArea;
	public int armyStrengthInDefendingArea;
	
    public Attack(String areaId, String attackingPlayerName, String defendingPlayerName) {
        this.areaId = areaId;
        this.attackingPlayerName = attackingPlayerName;
        this.defendingPlayerName = defendingPlayerName;
    }
    
    public Attack(AttackInterface attack, Board board, Game game) {
    	this.areaId = attack.getAreaId();
    	this.attackingPlayerName = attack.getAttackingPlayerName();
    	this.defendingPlayerName = attack.getDefendingPlayerName();    	
    	this.armyStrengthInAttackingArea = board.getArmyStrengthIn(board.getArea(areaId), game.getPlayerByName(attackingPlayerName));
    	this.armyStrengthInDefendingArea = board.getArmyStrengthIn(board.getArea(areaId), game.getPlayerByName(defendingPlayerName));
    	for (Iterator<AttackInterface.DicesThrow> it = attack.getDicesThrows().iterator(); it.hasNext();)
    		this.dicesThrows.add(new DicesThrow(it.next(), board, game));
    }
        

	public String getAreaId() {
		return areaId;
	}

	public String getAttackingPlayerName() {
		return attackingPlayerName;
	}

	public String getDefendingPlayerName() {
		return defendingPlayerName;
	}

	public List<risiko.game.service.AttackInterface.DicesThrow> getDicesThrows() {
		return dicesThrows;
	}

	public int getArmyStrengthInAttackingArea() {
		return armyStrengthInAttackingArea;
	}

	public int getArmyStrengthInDefendingArea() {
		return armyStrengthInDefendingArea;
	}


}

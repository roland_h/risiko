package risiko.game.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import risiko.game.service.PlayerState;

public class PlayerData implements PlayerState {
	public String name;
	public String id;
	public String color;
	public int reserveCount;
	public String mission;
	public List<Integer> victoryCards;
	
	public PlayerData(PlayerState player) {
		this.name = new String(player.getName());
		this.id = player.getId().toString();
		this.color = new String(player.getColor());
		this.reserveCount = player.getArmyReserveCount();
		this.victoryCards = new ArrayList<Integer>(player.getVictoryCards());
		this.mission = new String(player.getMissionId());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public UUID getId() {
		return UUID.fromString(id);
	}

	@Override
	public String getColor() {
		return color;
	}

	@Override
	public int getArmyReserveCount() {
		return reserveCount;
	}

	@Override
	public String getMissionId() {
		return mission;
	}

	@Override
	public List<Integer> getVictoryCards() {
		return new ArrayList<Integer>(this.victoryCards);
	}

}

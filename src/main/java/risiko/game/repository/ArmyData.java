package risiko.game.repository;

import java.util.List;
import java.util.UUID;

import risiko.game.common.GeoPos;
import risiko.game.service.ArmyInterface;

public class ArmyData implements ArmyInterface {
	List<Float> pos;
	String area;
	String id;
	String originArea;
	String ownerColor;
	int group;
	int strength;
	
	public ArmyData(ArmyInterface army) {
		this.pos = army.getPos().getLonLatCopy();
		this.area = new String(army.getAreaId());
		this.id = army.getId().toString();
		this.originArea = new String(army.getOriginAreaId());
		this.ownerColor = new String(army.getOwnerColor());
		this.group = army.getGroup();
		this.strength = army.getStrength();
	}

	@Override
	public GeoPos getPos() {
		return new GeoPos(pos.get(0), pos.get(1));
	}

	@Override
	public String getAreaId() {
		return area;
	}

	@Override
	public UUID getId() {
		return UUID.fromString(id);
	}

	@Override
	public String getOriginAreaId() {
		return originArea;
	}

	@Override
	public String getOwnerColor() {
		return ownerColor;
	}

	@Override
	public int getGroup() {
		return group;
	}

	@Override
	public int getStrength() {
		return strength;
	}
}

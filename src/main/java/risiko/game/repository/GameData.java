package risiko.game.repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import risiko.game.domain.GamePhase;
import risiko.game.service.ArmyInterface;
import risiko.game.service.AttackInterface;
import risiko.game.service.GameState;
import risiko.game.service.MoveInterface;
import risiko.game.service.PlayerState;

public class GameData implements GameState {
	
	static DateTimeFormatter isoFormatter = DateTimeFormatter.ISO_DATE_TIME;
	
	public String gameId;
	public int index;
	public int indexUpdate;
    public String createdOn;
    public String title;
	public String boardName;
	public String phase;
	public int gameRound;
	public String activePlayerName;
	public List<PlayerData> players = new ArrayList<PlayerData>();
	public String message;
	public List<ArmyData> armies = new ArrayList<ArmyData>();
	public ArmyData armyReserve = null;
	public List<MoveData> moves = new ArrayList<MoveData>();
	public AttackData attack = null;
	public String areaAttacked;
	public String areaConqueredId;
	public String areaConqueredFromId;
	public String winner;
    public String winnerMission;

	
	public GameData(GameState game) {
		this.gameId = game.getId().toString();
		this.index = game.getIndex();
		this.indexUpdate = game.getIndexUpdate();
		this.createdOn = game.getCreatedOn().format(isoFormatter);
		this.title = new String(game.getTitle());
		this.boardName = new String(game.getBoardName());
		this.phase = game.getPhase().toString();
		this.gameRound = game.getGameRound();
		this.activePlayerName = new String(game.getActivePlayerName());
		this.message = game.getMessage() != null ? new String(game.getMessage()) : "";
		
		for (Iterator<PlayerState> it=game.getPlayers().iterator(); it.hasNext(); )
			this.players.add(new PlayerData(it.next()));

		for (Iterator<ArmyInterface> it=game.getArmies().iterator(); it.hasNext();)
			this.armies.add(new ArmyData(it.next()));
		
		if (game.getArmyReserve() != null)
			this.armyReserve =  new ArmyData(game.getArmyReserve());
		
		if (game.getArmyMoves() != null)
			for (Iterator<MoveInterface> it = game.getArmyMoves().iterator(); it.hasNext();)
				this.moves.add(new MoveData(it.next()));
		
		if (game.getAttack() != null)
			this.attack = new AttackData(game.getAttack());
		
		this.areaAttacked = new String(game.getAreaAttacked());
		this.areaConqueredId = new String(game.getAreaConqueredId());
		this.areaConqueredFromId = new String(game.getAreaConqueredFromId());
		
		this.winner = new String(game.getWinner());
		this.winnerMission = new String(game.getWinnerMission());
	}
	

	public boolean addMoves(List<MoveInterface> moves) {
		for (Iterator<MoveInterface> it=moves.iterator(); it.hasNext();)
			this.moves.add(new MoveData(it.next()));
		return moves.size() > 0;			
	}
	

	@Override
	public UUID getId() {
		return UUID.fromString(gameId);
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public int getIndexUpdate() {
		return indexUpdate;
	}

	@Override
	public LocalDateTime getCreatedOn() {
		return LocalDateTime.parse(createdOn, isoFormatter);
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getBoardName() {
		return boardName;
	}

	@Override
	public List<PlayerState> getPlayers() {
		ArrayList<PlayerState> players = new ArrayList<PlayerState>();
		for (Iterator<PlayerData> it = this.players.iterator(); it.hasNext();)
			players.add(it.next());
		return players;
	}

	@Override
	public String getActivePlayerName() {
		return activePlayerName;
	}

	@Override
	public GamePhase getPhase() {
		return GamePhase.valueOf(phase);
	}

	@Override
	public int getGameRound() {
		return gameRound;
	}

	@Override
	public List<ArmyInterface> getArmies() {
		ArrayList<ArmyInterface> armies = new ArrayList<ArmyInterface>();
		for (Iterator<ArmyData> it=this.armies.iterator(); it.hasNext();)
			armies.add(it.next());
		return armies;
	}

	@Override
	public ArmyInterface getArmyReserve() {
		return armyReserve;
	}

	@Override
	public AttackInterface getAttack() {
		return attack;
	}

	@Override
	public List<MoveInterface> getArmyMoves() {
		ArrayList<MoveInterface> moves = new ArrayList<MoveInterface>();
		for (Iterator<MoveData> it=this.moves.iterator(); it.hasNext();)
			moves.add(it.next());
		return moves;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public String getAreaConqueredId() {
		return areaConqueredId;
	}

	@Override
	public String getAreaConqueredFromId() {
		return areaConqueredFromId;
	}


	@Override
	public String getAreaAttacked() {
		return areaAttacked;
	}


	@Override
	public String getWinner() {
		return winner;
	}	

	
	@Override
	public String getWinnerMission() {
		return winnerMission;
	}	
	
}

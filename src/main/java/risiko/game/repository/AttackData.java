package risiko.game.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import risiko.game.service.ArmyInterface;
import risiko.game.service.AttackInterface;


public class AttackData implements AttackInterface {
	
	public static class DicesThrow implements risiko.game.service.AttackInterface.DicesThrow {
	    public List<Integer> dicesAttacking = new ArrayList<>();
	    public List<Integer> dicesDefending = new ArrayList<>();
	    public List<ArmyData> armiesLostAttacking = new ArrayList<ArmyData>();
	    public List<ArmyData> armiesLostDefending = new ArrayList<ArmyData>();
	    public List<ArmyData> armiesNotLost = new ArrayList<ArmyData>();
	    
	    public DicesThrow(risiko.game.service.AttackInterface.DicesThrow dicesThrow) {
	    	this.dicesAttacking = new ArrayList<Integer>(dicesThrow.getDicesAttacking());
	    	this.dicesDefending = new ArrayList<Integer>(dicesThrow.getDicesDefending());	    	
	    	for (ArmyInterface army: dicesThrow.getArmiesLostAttacking())
	    		this.armiesLostAttacking.add(new ArmyData(army));
	    	for (ArmyInterface army: dicesThrow.getArmiesLostDefending())
	    		this.armiesLostDefending.add(new ArmyData(army));
	    	for (ArmyInterface army: dicesThrow.getArmiesNotLost())
	    		this.armiesNotLost.add(new ArmyData(army));
	    }
	    
		@Override
		public List<Integer> getDicesAttacking() {
			return dicesAttacking;
		}
		@Override
		public List<Integer> getDicesDefending() {
			return dicesDefending;
		}
		@Override
		public List<ArmyInterface> getArmiesLostAttacking() {
			return new ArrayList<ArmyInterface>(armiesLostAttacking);
		}
		@Override
		public List<ArmyInterface> getArmiesLostDefending() {
			return new ArrayList<ArmyInterface>(armiesLostDefending);
		}
		@Override
		public List<ArmyInterface> getArmiesNotLost() {
			return new ArrayList<ArmyInterface>(armiesNotLost);
		}
	}

	public String areaId;
	public String attackingPlayerName;
	public String defendingPlayerName;
	public List<DicesThrow> dicesThrows = new ArrayList<>();
	public int armyStrengthInAttackingArea;
	public int armyStrengthInDefendingArea;
	
	public AttackData(AttackInterface attack) {
		this.areaId = attack.getAreaId();
		this.attackingPlayerName = attack.getAttackingPlayerName();
		this.defendingPlayerName = attack.getDefendingPlayerName();
		this.dicesThrows = new ArrayList<DicesThrow>();
		for (Iterator<risiko.game.service.AttackInterface.DicesThrow> it = attack.getDicesThrows().iterator(); it.hasNext();)
			this.dicesThrows.add(new DicesThrow(it.next()));
		this.armyStrengthInAttackingArea = attack.getArmyStrengthInAttackingArea();
		this.armyStrengthInDefendingArea = attack.getArmyStrengthInDefendingArea();
	}
	
	@Override
	public String getAreaId() {
		return areaId;
	}
	
	@Override
	public String getAttackingPlayerName() {
		return attackingPlayerName;
	}
	
	@Override
	public String getDefendingPlayerName() {
		return defendingPlayerName;
	}
	
	@Override
	public List<risiko.game.service.AttackInterface.DicesThrow> getDicesThrows() {
		List<risiko.game.service.AttackInterface.DicesThrow> dicesThrows = new ArrayList<risiko.game.service.AttackInterface.DicesThrow>();
		for (Iterator<DicesThrow> it=this.dicesThrows.iterator(); it.hasNext();)
			dicesThrows.add(it.next());
		return dicesThrows;
	}
	
	@Override
	public int getArmyStrengthInAttackingArea() {
		return armyStrengthInAttackingArea;
	}
	
	@Override
	public int getArmyStrengthInDefendingArea() {
		return armyStrengthInDefendingArea;
	}
}

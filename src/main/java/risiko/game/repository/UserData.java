package risiko.game.repository;

import java.util.UUID;

import risiko.game.service.UserInterface;


public class UserData implements UserInterface {

	String name;
	String id;
	String pwd = null;

	public UserData(String userName, UUID userId) {
		this.name = new String(userName);
		this.id = userId.toString();
	}

	public UserData(String userName, UUID userId, String password) {
		this.name = new String(userName);
		this.id = userId.toString();
		this.pwd = new String(password);
	}

	public String getName() {
		return name;
	}

	public UUID getId() {
		return UUID.fromString(id);
	}

	public boolean authenticate(String userName, String userPwd) {
		return (name.equals(userName) &&
				pwd != null && (pwd.isEmpty() || pwd.equals(userPwd)));
	}

	public boolean authenticate(UUID adminToken) {
		return (adminToken.toString().equals(id));
	}

	public void resetPassword() {
		this.pwd = null;
	}

	public boolean setPassword(String userPwd) {
		if (this.pwd == null) {
			this.pwd = new String(userPwd);
			return true;
		}
		return false;
	}

	public boolean requirePassword() {
		return this.pwd == null || !this.pwd.isEmpty();
	}

	public boolean hasResetPassword() {
		return this.pwd == null;
	}

}

package risiko.game.repository;

import java.util.List;
import java.util.UUID;

import risiko.game.common.GeoPos;
import risiko.game.service.MoveInterface;

public class MoveData implements MoveInterface {
	public String armyId;
	public String armySrcAreaId;
	public String armyDstAreaId;
	public String message;
	public List<Float> armySrcLonLat;
	public List<Float> armyDstLonLat;
	public int armyStrengthSrc;
	public int armyStrengthDst;
	
	public MoveData(MoveInterface move) {
		this.armyId = move.getArmyId().toString();
		this.armySrcAreaId = new String(move.getArmyOldAreaId());
		this.armyDstAreaId = new String(move.getArmyNewAreaId());
		this.armySrcLonLat = move.getArmyOldPos().getLonLatCopy();
		this.armyDstLonLat = move.getArmyNewPos().getLonLatCopy();
		this.armyStrengthSrc = move.getArmyOldStrength();
		this.armyStrengthDst = move.getArmyNewStrength();
		this.message = new String(move.getMessage());
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	@Override
	public UUID getArmyId() {
		return UUID.fromString(armyId);
	}
	
	@Override
	public String getArmyOldAreaId() {
		return armySrcAreaId;
	}

	@Override
	public String getArmyNewAreaId() {
		return armyDstAreaId;
	}

	@Override
	public GeoPos getArmyOldPos() {
		return new GeoPos(armySrcLonLat.get(0), armySrcLonLat.get(1));
	}
	
	@Override
	public int getArmyOldStrength() {
		return armyStrengthSrc;
	}

	@Override
	public GeoPos getArmyNewPos() {
		return new GeoPos(armyDstLonLat.get(0), armyDstLonLat.get(1));
	}

	@Override
	public int getArmyNewStrength() {
		return armyStrengthDst;
	}

	public boolean withinArea() {
		return armySrcAreaId.equals(armyDstAreaId);
	}

	public boolean sameSrcDst(MoveInterface move) {
		return armySrcAreaId.contentEquals(move.getArmyOldAreaId()) && armyDstAreaId.contentEquals(move.getArmyNewAreaId()); 
	}

}

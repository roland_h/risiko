package risiko.game.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import risiko.game.service.GameState;
import risiko.game.service.PlayerState;
import risiko.game.service.UserInterface;


public class GameRepository {

	
	private Map<UUID, List<GameData>> gameStateHistory = new HashMap<UUID, List<GameData>>();
	private Map<String, UserData> users = new HashMap<String, UserData>();
	private UserData admin = new UserData("admin", UUID.randomUUID(), "blabla");

	
	//---------------------- start up --------------------------------
	

	public GameRepository() {
		// load gameStateHistory from DB
	}
	
	
	//---------------------- user management --------------------------------
	
	
	public UserInterface getAdmin(String password) {
		if (admin.authenticate("admin", password))
			return admin;
		return null;
	}

	public UserInterface getUser(String userName) {
		return users.get(userName);
	}

	public UserInterface addUser(String userName, String password) {
		UserData user = users.get(userName);
		if (user != null)
			return null;
		user = new UserData(userName, UUID.randomUUID(), password);
		users.put(userName, user);
		return user;
	}
	
	public UserInterface authenticateUser(String userName, String password) {
		UserData user = users.get(userName);
		if (user != null)
			if (user.authenticate(userName, password))
				return user;
		return null;
	}
	
	public UserInterface setUserPassword(String userName, String password) {
		UserData user = users.get(userName);
		if (user != null) {
			user.setPassword(password);
			return user;
		}
		return null;		
	}
	
	public UserInterface resetUserPassword(String userName) {
		UserData user = users.get(userName);
		if (user != null) {
			user.resetPassword();
			return user;
		}
		return null;		
	}
	
	public UserInterface deleteUser(String userName) {
		return (users.remove(userName));
	}
	
	public boolean resetPassword(UUID adminToken, String userName) {
		if (!admin.authenticate(adminToken))
			return false;
		
		UserData user = users.get(userName);
		if (user != null) {
			user.resetPassword();
			return true;
		}
		return false;
	}


	//---------------------- game management --------------------------------

	
	public boolean removeGame(UUID adminToken, UUID gameId) 
	{
		if (!admin.authenticate(adminToken))
			return false;
		
    	List<GameData> deletedGameStates = gameStateHistory.remove(gameId);
    	if (deletedGameStates != null)
    	{
    		// delete players if they are not playing in other games:
    		
        	List<String> deltedGamePlayers = new ArrayList<>();
        	List<PlayerState> deletedGamePlayers = deletedGameStates.get(0).getPlayers();
			for (PlayerState player: deletedGamePlayers)
				deltedGamePlayers.add(player.getName());

    		for (List<GameData> gameStates: gameStateHistory.values())
    			for (PlayerState player: gameStates.get(0).getPlayers())
					deltedGamePlayers.remove(player.getName());

    		for (String playerName: deltedGamePlayers)
    			deleteUser(playerName);

    		return true;
    	}
    	return false;
	}
	
	public boolean removePlayer(UUID adminToken, UUID gameId, String playerName) {
		if (!admin.authenticate(adminToken))
			return false;

    	List<GameData> states = gameStateHistory.get(gameId);
    	if (states != null && states.size() == 1)
    		for (PlayerData player: states.get(0).players)
    			if (player.getName().equals(playerName)) {
    				states.get(0).players.remove(player);
    				break;
    			}
    	
    	boolean found = false;
		for (List<GameData> gameStates: gameStateHistory.values())
			for (PlayerState player: gameStates.get(0).getPlayers())
    			if (player.getName().equals(playerName)) {
    				found = true;
    				break;
    			}
		if (!found)
			deleteUser(playerName);

		return true;
	}


	//---------------------- game state management --------------------------------
	

	public Map<UUID,GameState> getGames() 
	{
		HashMap<UUID,GameState> games = new HashMap<UUID,GameState>();
		for (Iterator<Entry<UUID, List<GameData>>> it = gameStateHistory.entrySet().iterator(); it.hasNext();) {
			Entry<UUID, List<GameData>> game = it.next();
			games.put(game.getKey(), game.getValue().get(game.getValue().size()-1));
		}
		return games;
	}
	
    public GameState loadGameState(UUID gameId) {
    	List<GameData> states = gameStateHistory.get(gameId);
    	if (states != null)
    		return states.get(states.size()-1);
    	return null;
    }

	
    public GameState loadGameState(UUID gameId, int index) {
    	List<GameData> states = gameStateHistory.get(gameId);
    	if (states != null)
    		return states.get(index);
    	return null;
    }

    
	public int getCountGameStates(UUID gameId) {
    	List<GameData> states = gameStateHistory.get(gameId);
    	if (states != null)
    		return states.size();
    	else
    		return 0;
	}

	
	public void addGameState(GameState gameState) 
	{
		if (gameState == null) return;
		
    	List<GameData> states = gameStateHistory.get(gameState.getId());
    	if (states == null) {
    		states = new ArrayList<GameData>();
    		gameStateHistory.put(gameState.getId(), states);
    	}
    	System.out.println("repo: add "+gameState.getId()+" "+gameState.getIndex()+"."+gameState.getIndexUpdate());
		states.add(new GameData(gameState));
	}
	
	
	public void updateGameState(GameState gameState) 
	{
		if (gameState == null) return;
		
    	List<GameData> states = gameStateHistory.get(gameState.getId());
    	if (states != null)
    		states.set(states.size()-1, new GameData(gameState));
    	else
    		addGameState(gameState);
	}

	
}

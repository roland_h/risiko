package risiko.game.controller.mapper;

import risiko.game.controller.dto.GameInfo;
import risiko.game.service.PlayerInterface;

public class GameInfoMapper {
	
    public static GameInfo map(PlayerInterface player)
    {
        if(player != null)
        {
        	GameInfo gameInfo = new GameInfo();

        	gameInfo.player = PlayerMapper.map(player);
        	gameInfo.boardFileName = player.getGame().getBoardName(); 
        	
//    		Iterator<Area> it = board.getAreas().iterator();
//            while (it.hasNext()) {
//            	Area area = it.next();
//            	List<String> neighbours = new ArrayList<String>();
//        		Iterator<Area> itNeighbor = area.getNeighbours().iterator();
//                while (itNeighbor.hasNext()) neighbours.add(itNeighbor.next().getId());
//                gameInfo.board.add(new AreaInfo(area.getId(), neighbours));
//            }

        	gameInfo.mission = player.getMissionDescription();
        	gameInfo.victoryCards = player.getVictoryCards();
        	gameInfo.victoryCardsValue = player.getVictoryCardsValue();
        	
            return gameInfo;
        }
        return null;
    }
}

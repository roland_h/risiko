package risiko.game.controller.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import risiko.game.controller.dto.ArmyInfo;
import risiko.game.controller.dto.AttackInfo;
import risiko.game.controller.dto.GameStateInfo;
import risiko.game.controller.dto.MoveInfo;
import risiko.game.controller.dto.PlayerInfo;
import risiko.game.service.ArmyInterface;
import risiko.game.service.AttackInterface;
import risiko.game.service.GameState;
import risiko.game.service.MoveInterface;
import risiko.game.service.PlayerInterface;
import risiko.game.service.PlayerState;

public class GameStateMapper {

    public static GameStateInfo map(GameState game)
    {
        if(game != null)
        {
        	GameStateInfo gameState = new GameStateInfo();
        	
        	gameState.gameId = game.getId();
        	gameState.index = game.getIndex();
        	gameState.indexUpdate = game.getIndexUpdate();
        	gameState.title = game.getTitle();
        	gameState.phase = game.getPhase();
        	gameState.activePlayerName = game.getActivePlayerName();
        	gameState.gameRound = game.getGameRound();
        	gameState.winner = game.getWinner();
        	gameState.winnerMission = game.getWinnerMission();
        	gameState.message = game.getMessage();
        	
        	gameState.players = new ArrayList<PlayerInfo>();
            Iterator<PlayerState> itP = game.getPlayers().iterator();
            while (itP.hasNext()) gameState.players.add(PlayerMapper.map(itP.next()));

            gameState.armies = new ArrayList<ArmyInfo>(); 
            Iterator<ArmyInterface> it = game.getArmies().iterator();
            while (it.hasNext()) gameState.armies.add(mapToArmyInfo(it.next()));
            
       		gameState.armyReserve = mapToArmyInfo(game.getArmyReserve());

       		List<MoveInterface> moves = game.getArmyMoves();
            if(moves != null) 
            {
            	gameState.moves = new ArrayList<MoveInfo>();
            	for (Iterator<MoveInterface> itMove=moves.iterator(); itMove.hasNext();)
            	{
            		MoveInterface move = itMove.next();
            		MoveInfo moveInfo = new MoveInfo();
                	moveInfo.armyId = move.getArmyId();
                	moveInfo.armySrcAreaName = move.getArmyOldAreaId(); 
                	moveInfo.armyDstAreaName = move.getArmyNewAreaId();
                	moveInfo.armySrcLonLat = Arrays.asList(move.getArmyOldPos().getLon(), move.getArmyOldPos().getLat());
                	moveInfo.armyDstLonLat = Arrays.asList(move.getArmyNewPos().getLon(), move.getArmyNewPos().getLat());
                	gameState.moves.add(moveInfo);
            	}
            }

            gameState.areaAttacked = game.getAreaAttacked();
            AttackInterface attack = game.getAttack();
            if(attack != null) 
            {
            	gameState.attack = new AttackInfo();
            	gameState.attack.areaId = attack.getAreaId();
            	gameState.attack.attackingPlayerName = attack.getAttackingPlayerName();
            	gameState.attack.defendingPlayerName = attack.getDefendingPlayerName();
            	gameState.attack.armyStrengthInAttackingArea = attack.getArmyStrengthInAttackingArea();
            	gameState.attack.armyStrengthInDefendingArea = attack.getArmyStrengthInDefendingArea();
            	for (int i=0; i < attack.getDicesThrows().size(); ++i)
            	{
                	gameState.attack.addDicesThrow();
                	
                	gameState.attack.dicesThrows.get(i).dicesAttacking = attack.getDicesThrows().get(i).getDicesAttacking();
                	gameState.attack.dicesThrows.get(i).dicesDefending = attack.getDicesThrows().get(i).getDicesDefending();
                	
                	Iterator<ArmyInterface> itArmy = attack.getDicesThrows().get(i).getArmiesNotLost().iterator();
                	while (itArmy.hasNext())
                		gameState.attack.dicesThrows.get(i).armiesNotLost.add(mapToArmyInfo(itArmy.next()));
                	
                	itArmy = attack.getDicesThrows().get(i).getArmiesLostAttacking().iterator();
                	while (itArmy.hasNext())
                		gameState.attack.dicesThrows.get(i).armiesLostAttacking.add(mapToArmyInfo(itArmy.next()));
                	
                	itArmy = attack.getDicesThrows().get(i).getArmiesLostDefending().iterator();
                	while (itArmy.hasNext())
                		gameState.attack.dicesThrows.get(i).armiesLostDefending.add(mapToArmyInfo(itArmy.next()));
            	}
            }
            
        	return gameState;
        }
    	return null;
    }


    public static GameStateInfo map(PlayerInterface player)
    {
        if(player != null)
        {
            ArrayList<String> names = new ArrayList<String>();
            Iterator<PlayerState> it = player.getGame().getPlayers().iterator();
            while (it.hasNext()) names.add(it.next().getName());

            ArrayList<PlayerInfo> playerInfos = new ArrayList<PlayerInfo>();
            Iterator<PlayerState> itP = player.getGame().getPlayers().iterator();
            while (itP.hasNext()) playerInfos.add(PlayerMapper.map(itP.next()));

            GameStateInfo state = new GameStateInfo();
        	state.activePlayerName = player.getName();
        	state.players = playerInfos;
            return state;
        }
        else
        	return null;
    }


    
    private static ArmyInfo mapToArmyInfo(ArmyInterface army)
    {
    	if (army != null)
	    	return new ArmyInfo(army.getId(), army.getPos(), army.getAreaId(),  
	    						army.getOwnerColor(), army.getOriginAreaId(),
	    						army.getGroup(), army.getStrength());
    	else
    		return null;
    }
    
}

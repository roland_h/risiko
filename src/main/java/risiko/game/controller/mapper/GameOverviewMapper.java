package risiko.game.controller.mapper;

import risiko.game.controller.dto.GameOverview;
import risiko.game.service.GameState;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class GameOverviewMapper {
	
    public static GameOverview map(GameState game)
    {
        if(game != null)
        {
            String players = game.getPlayers().stream()
                    .map(player -> player.getName())
                    .collect(Collectors.joining(","));
            String info = game.getTitle();
            GameOverview gameOverview = new GameOverview(game.getId(), game.getPhase(), info, game.getCreatedOn(), players);
            return gameOverview;
        }
        return null;
    }

    public static List<GameOverview> map(Collection<GameState> games)
    {
        return games.stream()
                .map(game -> GameOverviewMapper.map(game))
                .collect(Collectors.toList());
    }

}

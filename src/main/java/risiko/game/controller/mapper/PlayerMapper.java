package risiko.game.controller.mapper;

import risiko.game.controller.dto.PlayerInfo;
import risiko.game.service.PlayerState;
import risiko.game.service.UserInterface;

public class PlayerMapper {

    public static PlayerInfo map(PlayerState player)
    {
    	PlayerInfo playerInfo = new PlayerInfo();
    	playerInfo.name = player.getName();
    	playerInfo.id = player.getId().toString();
    	playerInfo.color = player.getColor();
    	playerInfo.reserveCount = player.getArmyReserveCount();
    	playerInfo.cardsCount = player.getVictoryCards().size();
    	return playerInfo;
    }

    public static PlayerInfo map(UserInterface user)
    {
    	return new PlayerInfo(user);
    }
    
}

package risiko.game.controller;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import risiko.game.common.GeoPos;
import risiko.game.controller.dto.GameInfo;
import risiko.game.controller.dto.GameOverview;
import risiko.game.controller.dto.GameStateInfo;
import risiko.game.controller.dto.PlayerInfo;
import risiko.game.controller.mapper.GameInfoMapper;
import risiko.game.controller.mapper.GameOverviewMapper;
import risiko.game.controller.mapper.GameStateMapper;
import risiko.game.controller.mapper.PlayerMapper;
import risiko.game.service.GameService;
import risiko.game.service.GameInterface;
import risiko.game.service.GameState;
import risiko.game.service.PlayerInterface;
import risiko.game.service.UserInterface;

@RestController
public class GameController {

	@Autowired
	private GameService gameService;

	private final Map<DeferredResult<GameStateInfo>, UUID> updateRequestsGameState = new ConcurrentHashMap<>();


    @PostMapping("admin/newGame")
    public GameOverview newGame(@RequestParam(value = "name", defaultValue = "Kampf um die Welt") String name) 
    {
    	GameOverview game = GameOverviewMapper.map(gameService.newGame(name));
        
    	gameService.updateGameOverviewListeners();

        return game;
    }
    
	@GetMapping("game/list")
	List<GameOverview> currentGames() {
		return GameOverviewMapper.map(gameService.getAllGames());
	}

	@GetMapping("game/info")
	GameInfo getPlayerInfo(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId) 
	{
		return GameInfoMapper.map(gameService.getPlayer(gameId, playerId));
	}


	@PostMapping("game/addPlayer")
	PlayerInfo addPlayer(@RequestParam(value = "game") UUID gameId, 
						 @RequestParam(value = "name") String name, 
						 @RequestParam(value = "pwd") String password) 
	{
		PlayerInterface player = gameService.addPlayer(gameId, name, password);
		if (player != null) {
			GameStateInfo gameState = GameStateMapper.map(player);
			gameService.updateGameOverviewListeners();
			updateGameStateListeners(gameState, gameId);
			return PlayerMapper.map(player);
		}
		return null;
	}
	
	@PostMapping("game/joinGame")
	PlayerInfo joinGame(@RequestParam(value = "game") UUID gameId, 
						@RequestParam(value = "player") String playerName, 
						@RequestParam(value = "pwd", defaultValue = "") String password)
	{
		UserInterface user = gameService.getUser(gameId, playerName);
		if (user == null)
			return null;
		
		PlayerInterface player = gameService.joinGame(gameId, playerName, password);
		if (player != null) {
			GameStateInfo state = GameStateMapper.map(player);
			gameService.updateGameOverviewListeners();
			updateGameStateListeners(state, gameId);
			return PlayerMapper.map(player);
		}
		else {
			return new PlayerInfo(playerName, user.hasResetPassword());			
		}
	}
	
	@GetMapping("game/state")
	GameStateInfo getGameState(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId,
							   @RequestParam(value = "index", defaultValue = "-1") Integer index) 
	{
		GameState state = gameService.getGameState(gameId, playerId, index);
		if (state != null) {
			System.out.println("game/state: index: "+index+" game state index: "+state.getIndex());
			return GameStateMapper.map(state);
		}
		return null;
	}

	@PutMapping("game/start")
	GameStateInfo startGame(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId) 
	{
		GameStateInfo state = GameStateMapper.map(gameService.startGame(gameId, playerId));
		updateGameStateListeners(state, gameId);
//		System.out.println("start: hist index max: "+(state != null ? state.index : -1)+" (hist: "+gameStateHistory.size()+")");
//		System.out.println("  #armies: "+(state != null ? state.armies.size() : -1)+", active player: "+state.activePlayerName);
		return state;
	}

	@PutMapping("game/next")
	GameStateInfo nextPlayer(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId) 
	{
		GameStateInfo state = GameStateMapper.map(gameService.nextPlayer(gameId, playerId));
		updateGameStateListeners(state, gameId);
//		System.out.println("start: hist index max: "+(state != null ? state.index : -1)+" (hist: "+gameStateHistory.size()+")");
//		System.out.println("  #armies: "+(state != null ? state.armies.size() : -1)+", active player: "+state.activePlayerName);
		return state;
	}

	@PutMapping("game/attack")
	GameStateInfo executeAttack(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId,
			 					@RequestParam(value = "delta") Float delta) 
	{
		GameInterface state = gameService.executeAttack(gameId, playerId, delta);
		if (state != null) {
			GameStateInfo stateInfo = GameStateMapper.map(state);
			updateGameStateListeners(stateInfo, gameId);
			return stateInfo;
		}
		else
			return null;
	}

	@PutMapping("game/move")
	public GameStateInfo move(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId,
			@RequestParam(value = "army") UUID armyId, @RequestParam(value = "area") String area,
			@RequestParam(value = "lon") Float lon, @RequestParam(value = "lat") Float lat,
			@RequestParam(value = "strength", defaultValue = "1") Integer count) 
	{
		GameInterface state = gameService.moveArmy(gameId, playerId, armyId, area, new GeoPos(lon, lat), count);
		if (state != null) {
			GameStateInfo stateInfo = GameStateMapper.map(state);
			updateGameStateListeners(stateInfo, gameId);
			return stateInfo;
		} 
		else
			return null;
	}

	@PutMapping("game/redeem")
	GameStateInfo redeemCards(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId) 
	{
		GameInterface game = gameService.redeemCards(gameId, playerId);
		if (game != null) {
			GameStateInfo state = GameStateMapper.map(game);
			updateGameStateListeners(state, gameId);
			return state;
		}
		else
			return null;
	}

	@PutMapping("game/group")
	public GameStateInfo group(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId,
						   @RequestParam(value = "army") UUID armyId, @RequestParam(value = "strength") Integer strength) 
	{
		GameInterface state = gameService.groupArmies(gameId, playerId, armyId, strength);
		if (state != null) {
			GameStateInfo stateInfo = GameStateMapper.map(state);
			updateGameStateListeners(stateInfo, gameId);
			return stateInfo;
		} 
		else
			return null;
	}

	
	@PutMapping("game/ungroup")
	public GameStateInfo ungroup(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId,
						     @RequestParam(value = "army") UUID armyId,  @RequestParam(value = "strength") Integer strength,
							 @RequestParam(value = "delta") Float delta) 
	{
		GameInterface state = gameService.ungroupArmies(gameId, playerId, armyId, strength, delta);
		if (state != null) {
			GameStateInfo stateInfo = GameStateMapper.map(state);
			updateGameStateListeners(stateInfo, gameId);
			return stateInfo;
		} 
		else
			return null;
	}

	
	@GetMapping("game/state/subscribe")
	public DeferredResult<GameStateInfo> currentGameStateUpdate(@RequestParam(value = "game") UUID gameId) {
		final DeferredResult<GameStateInfo> result = new DeferredResult<GameStateInfo>(10000L, Collections.emptyList());

		addGameStateUpdateRequest(result, gameId);

		return result;
	}

	
	// -------------------------------------------------------------------------------------


	@GetMapping("game/list/subscribe")
	public DeferredResult<List<GameOverview>> currentGamesUpdate() {
		final DeferredResult<List<GameOverview>> result = new DeferredResult<List<GameOverview>>(10000L,
				Collections.emptyList());

		gameService.addGameOverviewUpdateRequest(result);

		return result;
	}
	
	
	// -------------------------------------------------------------------------------------

	
	public void addGameStateUpdateRequest(DeferredResult<GameStateInfo> request, UUID gameId) {
		this.updateRequestsGameState.put(request, gameId);

		request.onCompletion(new Runnable() {
			@Override
			public void run() {
				updateRequestsGameState.remove(request, gameId);
			}
		});
	}

	private class GameStateNotifier extends Thread {
		public GameStateInfo gameState;
		public UUID gameId;

		public GameStateNotifier(GameStateInfo gameState, UUID gameId) {
			this.gameState = gameState;
			this.gameId = gameId;
		}

		public void run() 
		{
			System.out.println("updateGameStateListeners: game: " + gameId);
			Iterator<Entry<DeferredResult<GameStateInfo>, UUID>> it = updateRequestsGameState.entrySet().iterator();
			while (it.hasNext()) {
				Entry<DeferredResult<GameStateInfo>, UUID> entry = it.next();
				if (entry.getValue().equals(this.gameId))
				{
					System.out.println("  send deferred state request");
					entry.getKey().setResult(gameState);
				}
			}
		}
	}

	private void updateGameStateListeners(GameStateInfo gameState, UUID gameId) 
	{
		if (gameState != null)
		{
			GameStateNotifier notifier = new GameStateNotifier(gameState, gameId);
			notifier.start();
		}
	}

}

package risiko.game.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import risiko.game.controller.dto.GameInfo;
import risiko.game.controller.dto.GameOverview;
import risiko.game.controller.dto.PlayerInfo;
import risiko.game.controller.mapper.GameInfoMapper;
import risiko.game.controller.mapper.GameOverviewMapper;
import risiko.game.controller.mapper.PlayerMapper;
import risiko.game.service.GameService;
import risiko.game.service.UserInterface;

//import java.util.UUID;


@RestController
public class GameAdminController {
	
    @Autowired
    GameService gameService;


    @PostMapping("admin/login")
    public PlayerInfo login(@RequestParam(value = "pwd") String password) 
    {
    	UserInterface admin = gameService.getAdmin(password);
    	if (admin != null)
    		return PlayerMapper.map(admin);
    	return null;
    }

	@GetMapping("admin/list")
	List<GameOverview> currentGames() {
		return GameOverviewMapper.map(gameService.getAllGames());
	}

	@GetMapping("admin/info")
	GameInfo getPlayerInfo(@RequestParam(value = "game") UUID gameId, @RequestParam(value = "player") UUID playerId)
	{
		return GameInfoMapper.map(gameService.getPlayer(gameId, playerId));
	}

	@PostMapping("admin/deleteGame")
	List<GameOverview> deleteGame(@RequestParam(value = "admin") UUID adminToken, 
								  @RequestParam(value = "game") UUID gameId)
	{
		if (gameService.removeGame(adminToken, gameId)) 
		{
			gameService.updateGameOverviewListeners();
			
			return GameOverviewMapper.map(gameService.getAllGames());
		}
		return null;
	}
	
	@PostMapping("game/deletePlayer")
	List<GameOverview> deletePlayer(@RequestParam(value = "admin") UUID adminToken, 
			  						@RequestParam(value = "game") UUID gameId, 
			  						@RequestParam(value = "player") String playerName)
	{
		if (gameService.removePlayer(adminToken, gameId, playerName))
		{
	    	gameService.updateGameOverviewListeners();
	    	
			return GameOverviewMapper.map(gameService.getAllGames());
		}
		return null;
	}
	
	@PostMapping("game/resetPwd")
	List<GameOverview> resetPlayerPwd(@RequestParam(value = "admin") UUID adminToken, 
			  						  @RequestParam(value = "player") String playerName)
	{
		if (gameService.resetPassword(adminToken, playerName)) 
		{
			return GameOverviewMapper.map(gameService.getAllGames());
		}
		return null;
	}
	
}

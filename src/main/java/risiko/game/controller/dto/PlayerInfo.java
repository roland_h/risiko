package risiko.game.controller.dto;

import risiko.game.service.UserInterface;

public class PlayerInfo {
	public String name;
	public String id = "";
	public String color = "";
	public int reserveCount = 0;
	public int cardsCount = 0;
	public boolean resetPassword = false; 
	
	public PlayerInfo() {
	}

	public PlayerInfo(UserInterface user) {
		this.name = user.getName();
		this.id = user.getId().toString();
	}

	public PlayerInfo(String name, boolean resetPassword) {
		this.name = name;
		this.resetPassword = resetPassword;
	}

}

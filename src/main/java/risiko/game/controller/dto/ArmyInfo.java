package risiko.game.controller.dto;

import java.util.UUID;

import risiko.game.common.GeoPos;

public class ArmyInfo 
{
	GeoPos pos;
	String area;
	UUID id;
	String originArea;
	String color = "";
	int group = -1;
	int strength = 1;

	public ArmyInfo(UUID id, GeoPos pos, String area, String color, String originArea, int group, int strength) {
		this.id = id;
		this.pos = pos;
		this.area = area;
		this.color = color;
		this.originArea = originArea;
		this.group = group;
		this.strength = strength;
	}

	public GeoPos getPos() {
		return pos;
	}

	public String getArea() {
		return area;
	}

	public UUID getId() {
		return id;
	}

	public String getOriginArea() {
		return originArea;
	}

	public String getColor() {
		return color;
	}
	
	public Integer getGroup() {
		return group;
	}
	
	public Integer getStrength() {
		return strength;
	}
	
}

package risiko.game.controller.dto;

import java.util.List;
import java.util.UUID;


public class MoveInfo {
	public UUID armyId = null;
	public String armySrcAreaName = "";
	public String armyDstAreaName = "";
	public List<Float> armySrcLonLat;
	public List<Float> armyDstLonLat;
	public Integer armyStrengthSrc = 1;
	public Integer armyStrengthDst = 1;
	

	public UUID getArmyId() {
		return armyId;
	}

	public String getArmySrcAreaName() {
		return armySrcAreaName;
	}

	public String getArmyDstAreaName() {
		return armyDstAreaName;
	}
	
	public List<Float> getArmySrcLonLat() {
		return armySrcLonLat;
	}
	
	public List<Float> getArmyDstLonLat() {
		return armyDstLonLat;
	}

}

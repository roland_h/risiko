package risiko.game.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class AttackInfo {

	public static class DicesThrow {
	    public List<Integer> dicesAttacking = new ArrayList<>();
	    public List<Integer> dicesDefending = new ArrayList<>();
	    public List<ArmyInfo> armiesLostAttacking = new ArrayList<ArmyInfo>();
	    public List<ArmyInfo> armiesLostDefending = new ArrayList<ArmyInfo>();
	    public List<ArmyInfo> armiesNotLost = new ArrayList<ArmyInfo>();
	}

	public String areaId;
	public String attackingPlayerName;
	public String defendingPlayerName;
	public List<DicesThrow> dicesThrows = new ArrayList<>();
	public int armyStrengthInAttackingArea;
	public int armyStrengthInDefendingArea;

	public String getAreaId() {
		return areaId;
	}
	public String getAttackingPlayerName() {
		return attackingPlayerName;
	}
	public String getDefendingPlayerName() {
		return defendingPlayerName;
	}
	public List<DicesThrow> getDicesThrows() {
		return dicesThrows;
	}
	public int addDicesThrow() {
		dicesThrows.add(new DicesThrow());
		return dicesThrows.size()-1;
	}

}

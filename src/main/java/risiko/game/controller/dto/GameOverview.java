package risiko.game.controller.dto;

import risiko.game.domain.GamePhase;

import java.time.LocalDateTime;
import java.util.UUID;

public class GameOverview {
    private UUID id;
    private GamePhase phase = GamePhase.UNDEFINED;
    private String info;
    private LocalDateTime createdOn;
    private String players;

    public GameOverview() {
    }

    public GameOverview(UUID id, GamePhase phase, String info, LocalDateTime createdOn, String players) {
        this.id = id;
        this.phase = phase;
        this.info = info;
        this.createdOn = createdOn;
        this.players = players;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public String getPlayers() {
        return players;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setPhase(GamePhase phase) {
        this.phase = phase;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public GamePhase getPhase() {
        return phase;
    }

    public String getInfo() {
        return info;
    }

    public UUID getId() {
        return id;
    }
}

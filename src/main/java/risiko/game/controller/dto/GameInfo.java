package risiko.game.controller.dto;

import java.util.List;


public class GameInfo 
{
	public PlayerInfo player;
	public String boardFileName;
	public String mission = "";
	public List<Integer> victoryCards = null;
	public int victoryCardsValue = 0;


    public GameInfo() {
	}
    
	public PlayerInfo getPlayer() {
		return player;
	}
	
	public String getBoardFileName() {
		return boardFileName;
	}

	public String getMission() {
		return mission;
	}
	
	public List<Integer> getVictoryCards() {
		return victoryCards;
	}
	
	public int getVictoryCardsValue() {
		return victoryCardsValue;
	}

}

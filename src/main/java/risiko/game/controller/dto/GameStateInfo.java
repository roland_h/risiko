package risiko.game.controller.dto;

import java.util.List;
import java.util.UUID;

import risiko.game.domain.GamePhase;

public class GameStateInfo 
{
	public UUID gameId;
	public String title = "";
	public GamePhase phase = GamePhase.UNDEFINED;
	public String activePlayerName =  "";
	public List<PlayerInfo> players;
	public String areaAttacked = "";
	public String message =  "";
	public List<ArmyInfo> armies = null;
	public ArmyInfo armyReserve = null;
	public Integer index = -1;
	public Integer indexUpdate = 0;
	public List<MoveInfo> moves;
	public AttackInfo attack;
	public Integer gameRound = -1;
	public String winner = "";
	public String winnerMission = "";

	public GameStateInfo() {
    }

	public UUID getGameId() {
		return gameId;
	}
	
	public String getTitle() {
		return title;
	}

	public GamePhase getPhase() {
		return phase;
	}

	public String getActivePlayerName() {
		return activePlayerName;
	}

	public List<PlayerInfo> getPlayers() {
		return players;
	}

	public List<ArmyInfo> getArmies() {
		return armies;
	}

    public String getAreaAttacked() {
		return areaAttacked;
	}

	public String getMessage() {
		return message;
	}

    public Integer getIndex() {
		return index;
	}
    
    public Integer getIndexUpdate() {
		return indexUpdate;
	}
    
    public List<MoveInfo> getMove() { 
    	return moves; 
    }
	
    public AttackInfo getAttack() { 
    	return attack; 
    }

    public Integer getGameRound() {
		return gameRound;
	}
    

}

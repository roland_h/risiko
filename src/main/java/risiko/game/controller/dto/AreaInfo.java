package risiko.game.controller.dto;

import java.util.ArrayList;
import java.util.List;

//import risiko.game.domain.GeoPos;

public class AreaInfo {
	
	private String id;
	private List<String> neighbourIds = new ArrayList<>();
	
	public AreaInfo(String id, List<String> neighbourIds) {
		this.id = id;
		this.neighbourIds = neighbourIds;
	}

	public String getId() {
		return id;
	}

	public List<String> getNeighbourIds() {
		return neighbourIds;
	}
	
}



ReserveOverlay.prototype = new google.maps.OverlayView();
TextOverlay.prototype = new google.maps.OverlayView();


//--------------------- reserve_overlay --------------------


/** @constructor */
function ReserveOverlay(map_) 
{
  this.map = map_;

  this.div = null;
  this.marker_width = 36;
  this.marker_height = 36;
  this.marker_dist = 10;
  this.div_width = 0;
  this.div_left = 0;

  this.setMap(map_);
}

ReserveOverlay.prototype.onAdd = function() 
{
	this.div = document.createElement('span');
	this.div.className = 'canvasReserve';
	this.div.style.position = 'absolute';
	this.div.style.visibility = "hidden";
	this.div.style.width = this.marker_width.toString()+"px";
	this.div.style.height = this.marker_height.toString()+"px";
	this.getPanes().markerLayer.appendChild(this.div);
};

ReserveOverlay.prototype.draw = function() 
{
  updateArmiesInReserve();
};

ReserveOverlay.prototype.onRemove = function() 
{
	if (this.div != null) {
		  this.div.parentNode.removeChild(this.div);
		  this.div = null;
	}
};

ReserveOverlay.prototype.setArmyCount = function(count_armies)
{
	if (this.div != null) 
	{
		this.div_width = this.marker_width*count_armies + this.marker_dist*(count_armies-1);
		this.div_left = -this.div_width/2;
		this.div_top = this.map.getDiv().clientHeight/2 - this.marker_height - this.marker_dist - 10;
		
		this.div.style.left = this.div_left.toString()+'px';
		this.div.style.top  = this.div_top.toString()+'px';
		this.div.style.width = this.div_width.toString()+"px";
	}
};


ReserveOverlay.prototype.getMarkerPos = function(i) 
{
	if (this.div != null) 
	{
		var pos_screen = new google.maps.Point(
				this.div_left + this.marker_dist/2 + i * this.marker_width + 13,
				this.div_top  + this.marker_height/2 + 7
		);
		var pos_geo = this.getProjection().fromDivPixelToLatLng(pos_screen);
		return pos_geo;
	}
	else
		return null;
};

ReserveOverlay.prototype.setVisible = function(visible) 
{
	if (this.div != null) 
	{
		if (visible)
			this.div.style.visibility = "visible";
		else	
			this.div.style.visibility = "hidden";
	}
};



//-----------------------------------------


/** @constructor */
function TextOverlay(map_, position_ = 'CENTER') 
{
	this.map = map_;
	this.div = null;
	this.setMap(map_);
	if (position_ === 'CENTER') {
		
	}
};

TextOverlay.prototype.onAdd = function() 
{
	this.div = document.createElement('div');
	this.div.className = 'textOverlay';
	this.div.style.display = "none";	

	this.div_text = document.createElement('div');
	this.div_text.className = 'overlayText';
		
	this.div_close = document.createElement('a');
	this.div_close.setAttribute('href','#');
	this.div_close.className = "close";
	let this_obj = this;
	this.div_close.onclick =function(){ this_obj.hide(); return false; };
	
	this.div.appendChild(this.div_text);
	this.div.appendChild(this.div_close);
};

TextOverlay.prototype.show = function(txt, overlayClass=null, textClass=null) 
{
	if (overlayClass != null)
		this.div.className = overlayClass;
	if (textClass != null)
		this.div_text.className = textClass;
	
	this.div_text.innerHTML = txt;
	if (!this.isVisible()) 
	{
		this.div.style.display = "block";
		this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.div);		
	}
};

TextOverlay.prototype.hide = function() 
{
	this.map.controls[google.maps.ControlPosition.TOP_LEFT].clear();
	this.div.style.display = "none";
};

TextOverlay.prototype.isVisible = function()
{
	return (this.div.style.display !== "none");
};



function showCountryOverlay()
{
	if (country_overlay != null)
	{
		var area_infos = {};
		for (var area in game_board_areas) 
		{
			area_infos[area] = { "army_strength_owner": 0, "army_strength_attacker": 0, "owner":"", "attacker":"", "owner_color":"", "attacker_color":"" };
		}
		for (let i=0; i < game_state.armies.length; i++) 
		{
			let army = game_state.armies[i];
			if (army.area in game_board_areas) 
			{
				var army_owner = player_from_color[army.color].name;
				var area_army_info = area_infos[army.area];
				if (army.area != army.originArea)
				{
					area_army_info.army_strength_attacker += army.strength;
					area_army_info.attacker = army_owner;
					area_army_info.attacker_color = army.color;
				}
				else
				{
					area_army_info.army_strength_owner += army.strength;
					area_army_info.owner = army_owner;
					area_army_info.owner_color = army.color;
				}
			}
		}
		
		var txt = "<table width=90%>";
		for (continent_id in game_board_continents)
		{
			var continent = game_board_continents[continent_id];
			var continent_style = "style=\"background-color:"+continent.color+";\"";
			//txt += "<tr> <th width=30% colspan=2 "+continent_style+">"+continent.name+" (Wert: "+continent.value+")</th> </tr>";
			txt += "<tr> <th width=30% "+continent_style+">"+continent.name+" (Wert: "+continent.value+")</th> <th></th> <th></th> </tr>";
			for (area_id in game_board_areas)
			{
				var area = game_board_areas[area_id];
				if (area.continent == continent_id)
				{
					var owner_str = "0";
					var attacker_str = "";
					if (area_id in area_infos)
					{
						var area_army_info = area_infos[area_id];
						if (area_army_info.army_strength_owner > 0) 
							owner_str = "&emsp;&emsp;"+area_army_info.army_strength_owner+" &ensp; ("+area_army_info.owner+")";
						if (area_army_info.army_strength_attacker > 1)
							attacker_str += "angegriffen von "+area_army_info.army_strength_attacker+" Armeen ("+area_army_info.attacker+")"
						else if (area_army_info.army_strength_attacker > 0)
								attacker_str += "angegriffen von einer Armee ("+area_army_info.attacker+")"
					}
					txt += "<tr> <td width=35% "+continent_style+">"+area.name+"</td> <td width=25%> "+owner_str+"</td> <td width=40%> "+attacker_str+"</td> </tr>\n";
				}
			}
		}
		txt += "</table>";
		
		country_overlay.show(txt);		
	}
};


function toggleCountryOverlay()
{
	if (country_overlay.isVisible())
	{
		country_overlay.hide();
	}
	else
	{
		info_overlay.hide();
		
		showCountryOverlay();
	}
};


function showInfoOverlay(_)
{
	if (info_overlay != null)
	{
		console.log("showInfoOverlay");
		
		var txt = "";
		
		txt += "Spieler: ".bold()+"&nbsp;"+my_player.name+"<br>";
		txt += "Mission: ".bold()+"&nbsp;"+my_player.mission+"<br>";
		txt += "Siegeskarten: ".bold();
		if (my_player.cards.length > 0) {
			txt += "&nbsp;"
			for (var i=0; i < my_player.cards.length; ++i)
				txt += "<img style=\"vertical-align:middle;padding:2px\" display=\"inline-block\" width=25 height=25 "+
				       "src=\"images/army_"+my_player.cards[i]+".svg\" style=\"fill:#000\" onload=\"SVGInject(this)\">";
			if (my_player.cards_value > 0)
				txt += "&nbsp;&nbsp;(Eintauschwert: "+my_player.cards_value+" Armeen)";
		}
		else
			txt += "-";
		txt += "<br>";
		if (info_overlay_attack_msg.length > 0)
			txt += "Letzter Angriff: ".bold()+"&nbsp;"+info_overlay_attack_msg;
		txt += "<br>";
		
		info_overlay.show(txt);
	}
};

function toggleInfoOverlay()
{
	if (info_overlay.isVisible())
	{
		info_overlay.hide();
	}
	else
	{
		country_overlay.hide();
		
		requestGameInfo(null, showInfoOverlay);
	}
};

function showWinnerOverlay(state)
{
	if (winner_overlay != null)
	{
		console.log("showWinnerOverlay");
		
		var txt = "<img height=100  src=\"images/trophy.svg\" onload=\"SVGInject(this)\"/> " +
				  "<br><br>" +
		          (state.winner).bold() +" hat seine Mission erfüllt:" +
		          "<br><br>" +
		          "\""+state.winnerMission+"\"";

		winner_overlay.show(txt, 'winnerOverlay', 'winnerText');
	}
}

//--------------------- Marker context menu --------------------


function getMarkerContextMenuContent(army_strength, armies_in_area_strength,
									 function_group, function_ungroup)
{
	var div = document.createElement('div');
	div.className = 'contextMenuPane';
	context_menu_list = document.createElement("ul");
	context_menu_list.className = "contextMenuList";
	if (army_strength > 1) {
		  button = document.createElement("span");
		  button.onclick = function() { function_ungroup(1); };
		  button.appendChild(document.createTextNode("Aufteilen in Infanterie"));
		  list_elem = document.createElement("li");
		  list_elem.className = 'contextMenuEntry';
		  list_elem.appendChild(button);
		  context_menu_list.appendChild(list_elem);					    	
	}
	if (army_strength > 5) {
		  button = document.createElement("span");
		  button.onclick = function() { function_ungroup(5); };
		  button.appendChild(document.createTextNode("Aufteilen in Kavallerie"));
		  list_elem = document.createElement("li");
		  list_elem.className = 'contextMenuEntry';
		  list_elem.appendChild(button);
		  context_menu_list.appendChild(list_elem);					    	
	}
	if (armies_in_area_strength >= 5 && army_strength != 5) {
		  button = document.createElement("span");
		  button.onclick = function() { function_group(5); };
		  button.appendChild(document.createTextNode("Gruppieren zu Kavallerie"));
		  list_elem = document.createElement("li");
		  list_elem.className = 'contextMenuEntry';
		  list_elem.appendChild(button);
		  context_menu_list.appendChild(list_elem);					    	
	}
	if (armies_in_area_strength >= 10 && army_strength != 10) {
		  button = document.createElement("span");
		  button.onclick = function() { function_group(10); };
		  button.appendChild(document.createTextNode("Gruppieren zu Artillerie"));
		  list_elem = document.createElement("li");
		  list_elem.className = 'contextMenuEntry';
		  list_elem.appendChild(button);
		  context_menu_list.appendChild(list_elem);
	}
	div.appendChild(context_menu_list);
	return div;
}

//
//class ContextMenuOverlay extends google.maps.OverlayView 
//{
//	constructor(position, content) 
//	{
//		super();
//		this.position = position;
//		content.classList.add("popup-bubble");
//		// This zero-height div is positioned at the bottom of the bubble.
//		const bubbleAnchor = document.createElement("div");
//		bubbleAnchor.classList.add("popup-bubble-anchor");
//		bubbleAnchor.appendChild(content);
//		// This zero-height div is positioned at the bottom of the tip.
//		this.containerDiv = document.createElement("div");
//		this.containerDiv.classList.add("popup-container");
//		this.containerDiv.appendChild(bubbleAnchor);
//		// Optionally stop clicks, etc., from bubbling up to the map.
//		ContextMenuOverlay.preventMapHitsAndGesturesFrom(this.containerDiv);
//	}
//	
//	onAdd() 
//	{
//		this.getPanes().floatPane.appendChild(this.containerDiv);
//	}
//	
//	onRemove() 
//	{
//		if (this.containerDiv.parentElement)
//			this.containerDiv.parentElement.removeChild(this.containerDiv);
//	}
//
//	draw() 
//	{
//		const divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
//		
//		// Hide the popup when it is far out of view.
//		const display = Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ? "block" : "none";
//
//		if (display === "block") {
//		  this.containerDiv.style.left = divPosition.x + "px";
//		  this.containerDiv.style.top = divPosition.y + "px";
//		}
//		
//		if (this.containerDiv.style.display !== display)
//			this.containerDiv.style.display = display;
//	}
//}
//
//
///** @constructor */
//function ContextMenuOverlay(map_) 
//{
//	this.map = map_;	
//	this.div = null;
//	this.setMap(map_);
//};
//
//ContextMenuOverlay.prototype.onAdd = function() 
//{
//	this.div = document.createElement('span');
//	this.div.className = 'contextMenuPane';
////	this.div.style.position = 'absolute';
//	this.div.style.visibility = "visible";
//	this.div.style.display = "none";
//	this.getPanes().markerLayer.appendChild(this.div);
//};
//
//ContextMenuOverlay.prototype.draw = function() 
//{
//};
//
//ContextMenuOverlay.prototype.onRemove = function() 
//{
//	if (this.div != null) {
//	  this.div.parentNode.removeChild(this.div);
//	  this.div = null;
//	}
//};
//
//ContextMenuOverlay.prototype.show = function (buttons, pos_x, pos_y)
//{
////	var context_menu_list = document.createElement("ul");
////	for (txt in buttons)
////	{
////	  var list_elem = document.createElement("li");
////	  var button = document.createElement("span");
////	  button.className = "contextMenuButton";
////	  button.onclick = buttons[txt];
////	  button.appendChild(document.createTextNode(txt));
////	  list_elem.appendChild(button);	  
////	  context_menu_list.appendChild(list_elem);
////	}
////	this.div.appendChild(context_menu_list);
////	this.div.style.left = pos_x;
////	this.div.style.top  = pos_y;
//	this.div.style.display = "block";
//};
//
//ContextMenuOverlay.prototype.hide = function () 
//{
//	this.div.style.display = "none";
//};



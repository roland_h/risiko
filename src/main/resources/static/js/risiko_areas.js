
var map_country_boarders = null;
var shapes_per_country = {};


var area_labels_font_size = [ 
	        // zoom level
	"",     //  0 
	"",     //  1 
	"7px",  //  2 
	"9px",  //  3 
	"14px", //  4 
	"18px", //  5
	"24px", //  6
	"30px", //  7
	""
];



function updateBoardAndGame(gameInfo, setGameBoardArea)
{
	board_file = "boards/"+gameInfo.boardFileName+".json";
	console.log("load board: "+board_file);
	fetch(board_file)
		.then(response => {
			if (!response.ok) {
			    throw new Error("HTTP error " + response.status);
			}
		  return response.json();
		})
		.then(board_ => {
			var game_board_areas = board_.areas;
			game_board_continents = board_.continents;
			console.log("board loaded: #areas: "+Object.keys(game_board_areas).length+", #continents: "+Object.keys(game_board_continents).length);
	
			setGameBoardArea(game_board_areas);
			
			updateAreaLabels();
			
		})
		.then(function() {
	
			requestGameState(0, true);
	
		});
}



function updateAreaLabels()
{
	if (map != null && game_board_areas != null)
	{
		var zoom_level = map.getZoom();
		var font_size = area_labels_font_size[area_labels_font_size.length-1];
		if (zoom_level < area_labels_font_size.length) {
			font_size = area_labels_font_size[zoom_level];
		}

		for (var i=0; i < area_labels.length; ++i) {
			area_labels[i].setMap(null);
		}
		area_labels = [];

		if (font_size !== '')
		{
			//console.log("create labels at zoom "+zoom_level+" with font size: "+font_size);
	   	for (let area_id in game_board_areas)
	 		{
	   		var area = game_board_areas[area_id];
	   		var pos = new google.maps.LatLng(area.pos[1]-1.0, area.pos[0]);
	   		area_labels.push(addLabel(map, game_board_areas[area_id].name, pos, font_size));
		  }		
		}
	}
}


function getArea(region, pos)
{
	if (region != null && region.cc != null) 
	{
		// CC identifies area:
		var area = risiko_areas[region.cc];
		if (area != null && area !== '')
			return area;
		
		// CC+RC identify area:
		if (region.rc != null) {
			area = risiko_areas[region.cc+region.rc];
			if (area != null && area !== '')
				return area;			
		}
		
		// find area by point-in-polygon test:
		var shapes_per_area = shapes_per_country[region.cc];
		if (shapes_per_area != null) {
			var latlng = new google.maps.LatLng(pos.lat, pos.lng);	
			for (var area in shapes_per_area) {
				var shapes = shapes_per_area[area];
				for (var i=0; i < shapes.length; i++)
					if (google.maps.geometry.poly.containsLocation(latlng, shapes[i]))
						return area;
			}
		}
	}
	return null;
}

function getCountry(geocoder_results)
{
	for (var i=0; i < geocoder_results.length; i++) 
	{
		var cc = null;
		var rc = null;
		for (var j=0; j < geocoder_results[i].address_components.length; j++) 
		{
			if (geocoder_results[i].address_components[j].types.length >= 2 &&
				geocoder_results[i].address_components[j].types[1] === 'political')
			{
				if (geocoder_results[i].address_components[j].types[0] === 'administrative_area_level_1') 
					rc = geocoder_results[i].address_components[j].short_name;
				else if (geocoder_results[i].address_components[j].types[0] == 'country')
					cc = geocoder_results[i].address_components[j].short_name;
			}
		}
		if (cc != null) return {"cc":cc, "rc":rc};
	}
	return null;
}


function addPoly(coords, country, area)
{
	if (coords.length >= 3) {
		//console.log(`draw ${country} (${area})`);
		let area_props = game_board_areas[area];
		if (area_props != null) {
			var poly = new google.maps.Polygon({
			  paths: coords,
			  strokeWeight: 0,
			  strokeColor: String(area_props.color),
			  strokeOpacity: 0.1,
			  strokeWeight: 1,
			  fillColor: String(area_props.color),
			  fillOpacity: 0.5,
			  cursor: ''
			});
			poly.setMap(map);
			return poly;
		}
	}
	return null;
}

function loadCountryBorders(game_board_areas)
{
	if (map_country_boarders != null) {
		return;
	}
	
	console.log("load borders and draw polygons");
	
	// world map:
	shp('geo/WORLD_BORDERS_s.zip').then(
			function(borders) {
				draw_borders(borders, 'ISO2', game_board_areas, risiko_areas)
			}, 
			function(e){
				console.log(e);
			}
		);	
	
	// Canada
	shp('geo/CAN_s.zip').then(
			function(borders) {
				draw_borders(borders, 'PREABBR', game_board_areas,
							 {
								'N.L.'  : 'CAQE',
								'P.E.I.': 'CAQE',
								'N.S.'  : 'CAQE',
								'N.B.'  : 'CAQE',
								'Que.'  : 'CAQE',
								'Ont.'  : 'CAON',
								'Man.'  : 'CAON',
								'Sask.' : 'CAAB',
								'Alta.' : 'CAAB',
								'B.C.'  : 'CAAB',
								'Y.T.'  : 'CANT',
								'N.W.T.': 'CANT',
								'Nvt.'  : 'CANT'
							 })
			}, 
			function(e) {
				console.log(e);
			}
		);	

	// Australia
	shp('geo/AUS_s.zip').then(
			function(borders) {
				draw_borders(borders, 'NAME_1', game_board_areas,
							 {
								'Queensland' : 'AUE',
								'New South Wales' : 'AUE',
								'Northern Territory' : 'AUW',
								'South Australia' : 'AUE',
								'Victoria' : 'AUE',
								'Western Australia' : 'AUW'
							 })
			}, 
			function(e) {
				console.log(e);
			}
		);	

	// USA
	shp('geo/USA_s.zip').then(
			function(borders) {
				draw_borders(borders, 'NAME_1', game_board_areas,
							 {
								'Alaska' : 'USAK',
								'California' : 'USW',
								'Colorado' : 'USW',
								'Washington' : 'USW',
								'Utah' : 'USW',
								'New Mexico' : 'USW',
								'Nevada' : 'USW',
								'Arizona' : 'USW',
								'Montana' : 'USW',
								'Oregon' : 'USW',
								'Wyoming' : 'USW',
								'Idaho' : 'USW',
								'North Dakota' : 'USE',
								'South Dakota' : 'USE',
								'Nebraska' : 'USE',
								'Kansas' : 'USE',
								'Iowa' : 'USE',
								'Oklahoma' : 'USE',
								'Georgia' : 'USE',
								'Louisiana' : 'USE',
								'New York' : 'USE',
								'Rhode Island' : 'USE',
								'Minnesota' : 'USE',
								'Michigan' : 'USE',
								'Missouri' : 'USE',
								'Tennessee' : 'USE',
								'Maryland' : 'USE',
								'Virginia' : 'USE',
								'District of Columbia' : 'USE',
								'North Carolina' : 'USE',
								'Illinois' : 'USE',
								'Delaware' : 'USE',
								'Florida' : 'USE',
								'Maine' : 'USE',
								'Massachusetts' : 'USE',
								'Mississippi' : 'USE',
								'South Carolina' : 'USE',
								'New Jersey' : 'USE',
								'West Virginia' : 'USE',
								'Alabama' : 'USE',
								'Kentucky' : 'USE',
								'Pennsylvania' : 'USE',
								'Wisconsin' : 'USE',
								'Connecticut' : 'USE',
								'New Hampshire' : 'USE',
								'Vermont' : 'USE',
								'Ohio' : 'USE',
								'Indiana' : 'USE',
								'Texas' : 'USE',
								'Arkansas' : 'USE'
							 })
			}, 
			function(e) {
				console.log(e);
			}
		);	
	

	shapes_per_country['RU'] = {};

	// Russia
	shp('geo/RUS_RU.zip').then(
			function(borders) {
				shapes_per_country['RU']['RUS'] = draw_borders(borders, 'NAME_1', game_board_areas, "RUS");
			});
	// Ural
	shp('geo/RUS_UR.zip').then(
			function(borders) {
				shapes_per_country['RU']['RUUR'] = draw_borders(borders, 'NAME_1', game_board_areas, "RUUR");
			});	
	// Sibirien
	shp('geo/RUS_SI.zip').then(
			function(borders) {
				shapes_per_country['RU']['RUSI'] = draw_borders(borders, 'NAME_1', game_board_areas, "RUSI");
			});	
	// Irkutsk
	shp('geo/RUS_IK.zip').then(
			function(borders) {
				shapes_per_country['RU']['RUIK'] = draw_borders(borders, 'NAME_1', game_board_areas, "RUIK");
			});	
	// Jakutsk
	shp('geo/RUS_YK.zip').then(
			function(borders) {
				shapes_per_country['RU']['RUYK'] = draw_borders(borders, 'NAME_1', game_board_areas, "RUYK");
			});	
	// Kamtschatka
	shp('geo/RUS_KA.zip').then(
			function(borders) {
				shapes_per_country['RU']['RUKA'] = draw_borders(borders, 'NAME_1', game_board_areas, "RUKA");
			});
	
	
	
	for (var id in game_board_areas)
	{
		let area = game_board_areas[id];
		if ('connections' in area) 
		{
			//console.log("draw connections: "+id)
			for (i=1; i < area.connections.length; i+=2)
			{
				const line = new google.maps.Polyline({
				    path: [ area.connections[i-1], area.connections[i] ],
				    geodesic: false,
				    strokeColor: "#101010",
				    strokeOpacity: 1.0,
				    strokeWeight: 2,
				});
				line.setMap(map);
			}
		}
	}

}


function draw_borders(borders, prop_key, game_board_areas, region_to_area)
{
	var polys = []
	map_country_boarders = borders.features;
	for (let s=0; s < map_country_boarders.length; s++) 
	{
		let shape = map_country_boarders[s];
		let country = shape.properties[prop_key];
		//console.log(country);
		
		let area;
		if (typeof region_to_area === "string")
			area = region_to_area;
		else
			area = region_to_area[country];
		
		if (area != null && area !== '' && area in game_board_areas) 
		{
			let geom = shape.geometry.coordinates;
			for (let i=0; i < geom.length; i++) 
			{
				var coords1 = [];
				for (let j=0; j < geom[i].length; j++) {
					if (geom[i][j].length > 0) {
						if (typeof geom[i][j][0] == "number") {
								//console.log(`${country}: ${s}:${i}:${j}: lon: ${geom[i][j][0]}, lat: ${geom[i][j][1]}`);
							coords1.push( { lng: geom[i][j][0], lat: geom[i][j][1] } );																		
						}
						else {
							var coords2 = [];
							for (let k=0; k < geom[i][j].length; k++) {
								if (geom[i][j][k].length > 0) {
									if (typeof geom[i][j][k][0] == "number") {
										coords2.push( { lng: geom[i][j][k][0], lat: geom[i][j][k][1] } );																		
									}
								}
							}
							var poly = addPoly(coords2, country, area);
							if (poly != null)
								polys.push(poly);
						}
					}
				}
				var poly = addPoly(coords1, country, area);
				if (poly != null)
					polys.push(poly);
			}
		}
	}
	return polys;
}


risiko_areas = {
		
		'CAQC' : 'CAQE',
		'CANL' : 'CAQE',
		'CANS' : 'CAQE',
		'CANB' : 'CAQE',
		'CAON' : 'CAON',
		'CAMB' : 'CAON',
		'CAAB' : 'CAAB',
		'CASK' : 'CAAB',
		'CABC' : 'CAAB',
		'CANT' : 'CANT',
		'CAYT' : 'CANT',
		'CANU' : 'CANT',
		
		'AUNT'  : 'AUW',
		'AUWA'  : 'AUW',
		'AUQLD' : 'AUE',
		'AUNSW' : 'AUE',
		'AUVIC' : 'AUE',
		'AUSA'  : 'AUE',
		
		'USCA' : 'USW',
		'USNV' : 'USW',
		'USOR' : 'USW',
		'USWA' : 'USW',
		'USMT' : 'USW',
		'USID' : 'USW',
		'USUT' : 'USW',
		'USAZ' : 'USW',
		'USNM' : 'USW',
		'USCO' : 'USW',
		'USWY' : 'USW',
		'USND' : 'USE',
		'USSD' : 'USE',
		'USNE' : 'USE',
		'USKS' : 'USE',
		'USOK' : 'USE',
		'USTX' : 'USE',
		'USMN' : 'USE',
		'USIA' : 'USE',
		'USMO' : 'USE',
		'USAR' : 'USE',
		'USLA' : 'USE',
		'USMS' : 'USE',
		'USAL' : 'USE',
		'USGA' : 'USE',
		'USFL' : 'USE',
		'USSC' : 'USE',
		'USTN' : 'USE',
		'USNC' : 'USE',
		'USIL' : 'USE',
		'USWI' : 'USE',
		'USMI' : 'USE',
		'USIN' : 'USE',
		'USKY' : 'USE',
		'USOH' : 'USE',
		'USWV' : 'USE',
		'USVA' : 'USE',
		'USPA' : 'USE',
		'USNY' : 'USE',
		'USMD' : 'USE',
		'USDE' : 'USE',
		'USNJ' : 'USE',
		'USCT' : 'USE',
		'USRI' : 'USE',
		'USMA' : 'USE',
		'USNH' : 'USE',
		'USME' : 'USE',
		'USVT' : 'USE',
		'USAK' : 'USAK',			
		
		'AD' : '',
		'AE' : 'MEA',
		'AF' : 'MEA',
		'AG' : '',
		'AI' : '',
		'AL' : 'EUS',
		'AM' : 'MEA',
		'AO' : 'CG',
		'AQ' : '', //'NN',
		'AR' : 'AR',
		'AS' : '',
		'AT' : 'EUS',
		'AU' : '', //'AU',
		'AW' : '',
		'AX' : '',
		'AZ' : 'MEA',
		'BA' : 'EUS',
		'BB' : 'IN', 
		'BD' : 'IN', 
		'BE' : 'EUN',
		'BF' : 'AFN',
		'BG' : 'EUS',
		'BH' : 'MEA', 
		'BI' : 'AFE',
		'BJ' : 'AFN',
		'BL' : '',
		'BM' : '',
		'BN' : 'ID',
		'BO' : 'PE',
		'BQ' : '',
		'BR' : 'BR',
		'BS' : '',
		'BT' : 'IN',
		'BV' : '',
		'BW' : 'AFS',
		'BY' : 'UA',
		'BZ' : 'CAM',
		'CA' : '', //'CA',
		'CC' : '',
		'CD' : 'CG',
		'CF' : 'CG',
		'CG' : 'CG',
		'CH' : 'EUS',
		'CI' : 'AFN',
		'CK' : '',
		'CL' : 'AR',
		'CM' : 'AFN',
		'CN' : 'CN',
		'CO' : 'VE',
		'CR' : 'CAM',
		'CU' : '',
		'CV' : '',
		'CW' : '',
		'CX' : '',
		'CY' : '',
		'CZ' : 'EUN',
		'DE' : 'EUN',
		'DJ' : 'AFE',
		'DK' : 'EUN',
		'DM' : '',
		'DO' : '',
		'DZ' : 'AFN',
		'EC' : 'PE',
		'EE' : 'UA',
		'EG' : 'EG',
		'EH' : '',
		'ER' : 'AFE',
		'ES' : 'EUW',
		'ET' : 'AFE',
		'FI' : 'SCA',
		'FJ' : '',
		'FK' : '',
		'FM' : '',
		'FO' : '',
		'FR' : 'EUW',
		'GA' : 'CG',
		'GB' : 'GB',
		'GD' : '',
		'GE' : 'MEA',
		'GF' : 'VE',
		'GG' : '',
		'GH' : 'AFN',
		'GI' : '',
		'GL' : 'GL',
		'GM' : 'AFN',
		'GN' : 'AFN',
		'GP' : '',
		'GQ' : 'CG',
		'GR' : 'EUS',
		'GS' : '',
		'GT' : 'CAM',
		'GU' : '',
		'GW' : 'AFN',
		'GY' : 'VE',
		'HK' : '',
		'HM' : '',
		'HN' : 'CAM',
		'HR' : 'EUS',
		'HT' : '',
		'HU' : 'EUS',
		'ID' : 'ID',
		'IE' : 'GB',
		'IL' : 'MEA',
		'IM' : '',
		'IN' : 'IN',
		'IO' : '',
		'IQ' : 'MEA',
		'IR' : 'MEA',
		'IS' : 'ISL',
		'IT' : 'EUS',
		'JE' : '',
		'JM' : '',
		'JO' : 'MEA',
		'JP' : 'JP',
		'KE' : 'AFE',
		'KG' : 'KZ',
		'KH' : 'SIA',
		'KI' : '',
		'KM' : '',
		'KN' : '',
		'KP' : 'CN',
		'KR' : 'CN',
		'KW' : 'MEA',
		'KY' : '',
		'KZ' : 'KZ',
		'LA' : 'SIA',
		'LB' : 'MEA',
		'LC' : '',
		'LI' : '',
		'LK' : '',
		'LR' : 'AFN',
		'LS' : '',
		'LT' : 'UA',
		'LU' : 'EUN',
		'LV' : 'UA',
		'LY' : 'EG',
		'MA' : 'AFN',
		'MC' : '',
		'MD' : 'UA',
		'ME' : 'EUS',
		'MF' : '',
		'MG' : 'MG',
		'MH' : '',
		'MK' : 'EUS',
		'ML' : 'AFN',
		'MM' : 'SIA',
		'MN' : 'MN',
		'MO' : '',
		'MP' : '',
		'MQ' : '',
		'MR' : 'AFN',
		'MS' : '',
		'MT' : '',
		'MU' : '',
		'MV' : '',
		'MW' : 'AFS',
		'MX' : 'CAM',
		'MY' : 'SIA',
		'MZ' : 'AFS',
		'NA' : 'AFS',
		'NC' : '',
		'NE' : 'AFN',
		'NF' : '',
		'NG' : 'AFN',
		'NI' : 'CAM',
		'NL' : 'EUN',
		'NO' : 'SCA',
		'NP' : 'IN',
		'NR' : '',
		'NU' : '',
		'NZ' : '',
		'OM' : 'MEA',
		'PA' : 'CAM',
		'PE' : 'PE',
		'PF' : '',
		'PG' : 'PNG',
		'PH' : 'PNG',
		'PK' : 'MEA',
		'PL' : 'EUN',
		'PM' : '',
		'PN' : '',
		'PR' : '',
		'PS' : '',
		'PT' : 'EUW',
		'PW' : '',
		'PY' : 'AR',
		'QA' : 'MEA',
		'RE' : '',
		'RO' : 'EUS',
		'RS' : 'EUS',
		'RU' : '',
		'RW' : 'AFE',
		'SA' : 'MEA',
		'SB' : 'PNG',
		'SC' : '',
		'SD' : 'AFE',
		'SE' : 'SCA',
		'SG' : '',
		'SH' : '',
		'SI' : 'EUS',
		'SJ' : '',
		'SK' : 'EUN',
		'SL' : 'AFN',
		'SM' : '',
		'SN' : 'AFN',
		'SO' : 'AFE',
		'SR' : 'VE',
		'SS' : 'CG',
		'ST' : '',
		'SV' : 'CAM',
		'SX' : '',
		'SY' : 'MEA',
		'SZ' : 'AFS',
		'TC' : '',
		'TD' : 'AFE',
		'TF' : '',
		'TG' : 'AFN',
		'TH' : 'SIA',
		'TJ' : 'MEA',
		'TK' : '',
		'TL' : 'ID',
		'TM' : 'MEA',
		'TN' : 'AFN',
		'TO' : '',
		'TR' : 'MEA',
		'TT' : '',
		'TV' : '',
		'TW' : 'CN',
		'TZ' : 'AFE',
		'UA' : 'UA',
		'UG' : 'AFE',
		'UM' : '',
		'US' : '',
		'UY' : 'AR',
		'UZ' : 'MEA',
		'VA' : '',
		'VC' : '',
		'VE' : 'VE',
		'VG' : '',
		'VI' : '',
		'VN' : 'SIA',
		'VU' : '',
		'WF' : '',
		'WS' : '',
		'YE' : 'MEA',
		'YT' : '',
		'ZA' : 'AFS',
		'ZM' : 'CG',
		'ZW' : 'AFS'
};



google_map_style = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.province",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "weight": 7
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "weight": 6
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape.natural.landcover",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape.natural.terrain",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "weight": 1
      }
    ]
  },
  {
    "featureType": "landscape.natural.terrain",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "weight": 1
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]

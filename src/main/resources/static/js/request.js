

//--------------------- game state  ---------------------


function requestGameInfo(setMyPlayer, onSuccessFunction = null, setBoardAreas = null) 
{
	var url = base_url+'/game/info?game='+my_game_uid+'&player='+my_player_uid;
	console.log('requestGameInfo: '+url);
	fetch(url, {
				headers: { 'Content-Type': 'application/json' }
			})
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((gameInfo) => 
	  	{
	  		if (setMyPlayer != null)
	  		{
				setMyPlayer({
						"name": gameInfo.player.name,
						"color": gameInfo.player.color,
						"cards": gameInfo.victoryCards,
						"mission": gameInfo.mission,
						"cards_value": gameInfo.victoryCardsValue
					});
				console.log('received game info: I am player: '+my_player.name+', board: '+gameInfo.boardFileName+', mission: '+gameInfo.mission);				
			}
			
			if (onSuccessFunction != null)
			{
				onSuccessFunction(gameInfo, setBoardAreas);
			}
		})
// 	  .catch((error) => 
// 	  	{
// 				console.log('request game info failed');
// 				gotoStartPage();
// 			});
}


function requestGameState(game_state_index, reload, attack_animation_fast=false)
{
	var url = base_url+'/game/state?game='+my_game_uid+'&player='+my_player_uid;
	if (game_state_index != null)
		url += "&index="+(-1-game_state_index).toString();
	console.log('updateGame: '+url);
	fetch(url, {
			method: 'GET',
			headers: { 'Content-Type': 'application/json' }
		})
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((response) => 
	  	{
				game_state = response;
				console.log('game state received');
				
				update(game_state, reload, true, true, attack_animation_fast);

				if (reload)
					subscribeGameState();				// subscribe game state 
		  })
// 	  .catch((error) => 
// 	  	{
// 				console.log('request game state failed: '+error);
// 				gotoStartPage();
// 			});
}


async function subscribeGameState() 
{
	url = base_url+'/game/state/subscribe?game='+my_game_uid;
	//console.log('subscribeGameState: '+url);
	let response = await fetch(url, {
			method: 'GET',
			headers: { 'Content-Type': 'application/json' }
		});
//	try
//	{
	  if (response.status == 502) { // connection timeout
	    await subscribeGameState();
	  }
	  else if (response.status != 200) { // error
	    //console.log('response error: '+response.statusText);
	    // reconnect in 3 sec.:
	    await new Promise(resolve => setTimeout(resolve, 500));
	    await subscribeGameState();
	  }
	  else 
	  {
	    let game_state = await response.json();
	    if (game_state != null && Object.keys(game_state).length > 0) 
	    {
		    console.log('subscription update');	    	
		    update(game_state);
	    }
	    else
    	{
		    //console.log('update: nothing new');	    	
    	}
	    // request next update:
	    await subscribeGameState();
	  }
//	}
//  	catch (err)
//  	{
//  		console.error('fetch error: '+err);
//		// catches errors both in fetch and response.json
//		// reconnect in 3 sec.:
//  		await new Promise(resolve => setTimeout(resolve, 1000));
//		await subscribeGameState();
//	}
}



//--------------------- play ---------------------


function startGame() 
{
	url = base_url+'/game/start?game='+my_game_uid+'&player='+my_player_uid;
//	url = base_url+'/game/start';
	console.log('startGame: '+url);					
//	const params = new FormData();
//	params.set('game', my_game_uid);
//	params.set('player', my_player_uid);
	fetch(url, {
		method: 'PUT',
//		body: params,
		headers: { 'Content-Type': 'application/json' }
    })
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((state) => 
	  	{
				if (state.activePlayerName !== '') {
					console.log('game started: active player: '+state.activePlayerName);
				}
				else {
					console.log('game not yet started');
				}
				
				update(state, false, true, false);
		  })
// 	  .catch((error) => 
// 	  	{
// 				console.log('starting game failed');
// 			});
}

function nextPlayer() 
{
//	url = base_url+'/game/next';
	url = base_url+'/game/next?game='+my_game_uid+'&player='+my_player_uid;
	console.log('nextPlayer: '+url);					
//	const params = new FormData();
//	params.set('game', my_game_uid);
//	params.set('player', my_player_uid);
	fetch(url, {
		method: 'PUT',
//		body: params,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    })
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((state) => 
	  	{
				if (state.activePlayerName !== '') {
					console.log('next player: '+state.activePlayerName);
					update(state, false, true, false);
				}
				else {
					console.log('next player failed');
				}
		  })
	  .catch((error) => 
	  	{
				console.log('next player failed');
			});
}


function attackArea()
{
	if (army_attacking != null)
		pos = new google.maps.LatLng(army_attacking.pos.lat, army_attacking.pos.lon);
	else
		pos = new google.maps.LatLng(0.0, 0.0);
	var geo_pos_delta = getGeoPosDelta(pos, 5);
	
	url = base_url+'/game/attack?game='+my_game_uid+'&player='+my_player_uid+'&delta='+geo_pos_delta;
//	url = base_url+'/game/attack';
	console.log('attack: '+url);					
//	const params = new FormData();
//	params.set('game', my_game_uid);
//	params.set('player', my_player_uid);
	fetch(url, {
		method: 'PUT',
//		body: params,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      })
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((state) => 
	  	{
			if (state.activePlayerName !== '') 
			{
				console.log('attack executed successfully');					
				if (menu != null && state.message != null && state.message !== "") {
					menu.setPopupMessage(state.message, 6);
					state.message = "";
				}
				update(state, false, true, true);
			}
			else 
			{
				console.log('attack execution failed');
				if (menu != null) 
					menu.setPopupMessage("Zug nicht möglich!", 5);
				update(game_state, false, true, false);
			}
		  })
	  .catch((error) => 
	  	{
			console.log('attack execution failed');	
	  	});
}


function moveMarker(pos_lon, pos_lat, army_id, army_strength=1)
{
	if (my_game_uid != null && my_player_uid !== '' && army_id !== '') 
	{
		var pos = {lat: pos_lat, lng: pos_lon};
		geocoder.geocode(
			{'location': pos}, 
			function(geocoder_results, status)
			{
				var area = '';
				
				if (status === 'OK' && geocoder_results.length > 0) 
				{
					console.log(`marker moved: lat: ${pos_lon} lon: ${pos_lat} -> ${geocoder_results[0].formatted_address}`);
				 	
					var country = getCountry(geocoder_results);
				 	if (country != null) 
				 	{
						current_marker_id = army_id;
						area = getArea(country, pos);
				 	}
				 }
				
				url = base_url+'/game/move?game='+my_game_uid+'&player='+my_player_uid+'&army='+army_id+'&area='+area+'&lon='+pos_lon+'&lat='+pos_lat;
				if (army_strength != 1)
					url += "&strength="+army_strength;
				console.log('moveMarker: '+url);
//				params = new FormData();
//				params.set('game', my_game_uid);
//				params.set('player', my_player_uid);
//				params.set('army', army_id);
//				params.set('area', String(area));
//				params.set('lon' , pos_lon);
//				params.set('lat' , pos_lat);
				fetch(url, {
					method: 'PUT',
//				    body: params,
				    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				  })
				  .then((response) => 
				  	{
						markers_reserve_draged = null;
						  
				    	return response.json();							  
				  	})
				  .then((state) => 
				  	{
						console.log('moving marker succeeded: '+state.message);
						if (menu != null && state.message != null && state.message !== "") {
							menu.setPopupMessage(state.message, 6);
							state.message = "";
						}
						update(state, false, true, false);
				  	})
				  .catch((error) => 
				  	{
				  		console.log('moving marker failed');
						if (menu != null)
							menu.setPopupMessage("Zug nicht möglich!", 5);
						update(game_state, false, true, false);
					});
			});
	}
}

function ungroupArmy(army_id, strength, geo_pos_delta, fallback_state) 
{ 
//	url = base_url+'/game/ungroup';
	url = base_url+'/game/ungroup?game='+my_game_uid+'&player='+my_player_uid+'&army='+army_id+'&strength='+strength+'&delta='+geo_pos_delta;
	console.log('ungroup: '+url);					
//	params = new FormData();
//	params.set('game', my_game_uid);
//	params.set('player', my_player_uid);
//	params.set('army', army_id);
//	params.set('strength', strength);
	fetch(url, {
		method: 'PUT',
//		body: params,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      })
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((state) => 
	  	{
			console.log('ungroup successful');	
			update(state, false, true, false);
		})
	  .catch((error) => 
	  	{
			console.log('ungroup failed');
			update(fallback_state, false, true, false);
	  	});
}

function groupArmies(army_id, strength, fallback_state) 
{ 
//	url = base_url+'/game/group';
	url = base_url+'/game/group?game='+my_game_uid+'&player='+my_player_uid+'&army='+army_id+'&strength='+strength;
	console.log('group: '+url);					
//	params = new FormData();
//	params.set('game', my_game_uid);
//	params.set('player', my_player_uid);
//	params.set('army', army_id);
//	params.set('strength', strength);
	fetch(url, {
		method: 'PUT',
//		body: params,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	  })
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((state) => 
	  	{
			console.log('group successful');	
			update(state, false, true, false);
		})
	  .catch((error) => 
	  	{
			console.log('group failed');
			update(fallback_state, false, true, false);
		});
}


function redeemCards() 
{
//	url = base_url+'/game/redeem';
	url = base_url+'/game/redeem?game='+my_game_uid+'&player='+my_player_uid;
	console.log('redeem: '+url);					
//	params = new FormData();
//	params.set('game', my_game_uid);
//	params.set('player', my_player_uid);
	fetch(url, 
		{
			method: 'PUT',
//			body: params,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      	}
	  )
	  .then((response) => 
	  	{
	    	return response.json();
	  	})
	  .then((state) => 
	  	{
			console.log('redeem successful');	
			update(state, false, true, false);
		})
	  .catch((error) => 
	  	{
	  		console.log('redeem failed');
			update(game_state, false, true, false);
		});
}


//--------------------- menu --------------------


class Menu 
{
	constructor(div_menu, map) 
	{
		this.buttons_players = {};

		this.message = null;
		this.popup_message = null;
		this.deferred_message = null;
	
		// menu area:
			
		this.div = div_menu;
	    this.div.className = 'menuPanel';
	    
		// menu area left:
		
	    this.div_players = document.createElement('div');
	    this.div_players.className = 'playersPanel';
		this.div.appendChild(this.div_players);
	
		// menu area center:
			
	    this.div_center = document.createElement('div');
	    this.div_center.className = 'messagePanel';
	    this.div_msg = document.createElement('div');
	    this.div_msg.className = 'message';
		this.div_center.appendChild(this.div_msg);
		this.div.appendChild(this.div_center);
	
		// menu area right:
				
	    this.div_buttons = document.createElement('div');
	    this.div_buttons.className = 'buttonPanel';
		this.div.appendChild(this.div_buttons);
	
		// backward/forward buttons:
		
		this.button_backward = document.createElement("span");
		this.button_backward.innerHTML = "<";
		this.setBackwardButtonActive(false);
		this.div_buttons.appendChild(this.button_backward);
			
		this.button_forward = document.createElement("span");
		this.button_forward.innerHTML = ">";
		this.setForwardButtonActive(false);
		this.div_buttons.appendChild(this.button_forward);
	
		// message button:
			
		this.button_redeem = document.createElement("span");
		this.button_redeem.innerHTML = "Eintausch";
		this.button_redeem.className = "buttonInfoInactive";
		this.button_redeem.onclick = null;
		this.div_buttons.appendChild(this.button_redeem);
	
	
		// message button:
			
		this.button_msg = document.createElement("span");
		this.button_msg.innerHTML = "Info";
		this.button_msg.className = "buttonInfo";
		this.button_msg.onclick = function() { toggleInfoOverlay();	};
		this.div_buttons.appendChild(this.button_msg);
		
		// countries button:
			
		this.button_info = document.createElement("span");
		this.button_info.innerHTML = "Gebiete";
		this.button_info.className = "buttonInfo";
		this.button_info.onclick = function() { toggleCountryOverlay();	};
		this.div_buttons.appendChild(this.button_info);
		  
		// action button:
			  
		this.button_action = document.createElement("span");
		this.div_buttons.appendChild(this.button_action);
		
		// game round info:
		
	    this.div_round = document.createElement('span');
	    this.div_round.className = 'round';
	}
	
	activateStartButton()
	{
		this.button_action.innerHTML = "Start";
		this.button_action.className = "buttonStart";
		this.button_action.onclick = function() { startGame();	};
	}
	
	disableActionButton()
	{
		this.button_action.className = "buttonActionDisabled";
		this.button_action.onclick = null;
	}
	
	setActionButtonText(button_text, attack=null)
	{
		this.button_action.innerHTML = button_text;
		this.button_action.className = "buttonStart";
		if (attack == null)
		{
			this.disableActionButton();
		}
		else
		{
		  if (attack)
			  this.button_action.onclick =  function(){ attackArea(); };
		  else
			  this.button_action.onclick =  function(){ nextPlayer(); };		  
		}
	}

	setForwardButtonActive(active)
	{
		if (active) {
			this.button_forward.className = "buttonInfo";			
			this.button_forward.onclick = function() { gameStateNext();	};
		}
		else {
			this.button_forward.className = "buttonInfoInactive";
			this.button_forward.onclick = null;
		}
	}

	setBackwardButtonActive(active)
	{
		if (active) {
			  this.button_backward.className = "buttonInfo";
			  this.button_backward.onclick =  function(){ gameStatePrevious(); };
		}
		else {
			  this.button_backward.className = "buttonInfoInactive";			
			  this.button_backward.onclick =  null;
		}
	}

	setRedeemButtonActive(active)
	{
		if (active) {
			  this.button_redeem.className = "buttonRedeemActive";
			  this.button_redeem.onclick =  function(){ redeemCards(); };
		}
		else {
			  this.button_redeem.className = "buttonRedeemInactive";			
			  this.button_redeem.onclick =  null;
		}
	}

	setMessage(msg, deferred_sec=0)
	{
		if (deferred_sec == 0)
		{
			if (msg == null) msg = "";
			this.message = msg;
			//console.log("set message: "+msg);
			this.div_msg.innerHTML = msg;			
		}
		else
		{
			this.deferred_message = msg;
			setTimeout( function() {
					if (this.deferred_message != null)
						this.setMessage(this.deferred_message); 
				}, 
				deferred_sec*1000 );			
		}
	}

	resetDeferredMessage()
	{
		this.deferred_message = null;
	}
	
	addMessage(msg, sep="")
	{
		if (msg == null) msg = "";
		if (this.message !== "")
			this.message += sep;
		this.message += msg;
		//console.log("set message: "+this.message);
		this.div_msg.innerHTML = this.message;
	}
	
	getMessage()
	{
		return this.message;
	}

	setPopupMessage(msg_, sec)
	{
		if (sec == null) sec = 10;
		let msg = msg_;
		//console.log("set popup message: "+msg);
		this.message = ""+this.div_msg.innerHTML;
		this.div_msg.innerHTML = msg;
		this.popup_message = msg;
		let div_msg = this.div_msg;
		setTimeout(function() { 
				//console.log("set back message: "+msg);
				div_msg.innerHTML = menu.message;
			}, sec*1000);
	}
	
	
	update(state, game_state_index, cnt_reserve = 0)
	{
		// fill players panel:
		
		while (this.div_players.lastElementChild) { this.div_players.removeChild(this.div_players.lastElementChild); }
		this.buttons_players = {}
		for (let i=0; i < state.players.length; i++) 
		{
			let player = state.players[i];
			if (player.name !== "") 
			{
			    var button = document.createElement('span');
			    
			    var cards_str = "*".repeat(player.cardsCount)
			    if (player.reserveCount > 0)
				    button.innerHTML = player.name+cards_str+" +"+player.reserveCount;
			    else
				    button.innerHTML = player.name+cards_str;
			    
			    if (player.color !== '') 
			    {
			    	button.className = 'playerButtonInactive';
			    	button.style.borderColor = player.color;
			    	player_from_color[player.color] = player;
			    } 
			    else 
			    {
					if (player.name === my_player.name)
				    	button.className = 'playerButtonIamNotStarted';
					else
					    button.className = 'playerButtonNotStarted';						
			    	button.style.borderColor = '#EFEFEF';	    	
			    }
			    
			    if (player.name === my_player.name) {
			    	button.style.color = "#000000";
					button.style.fontWeight = "bold";
			    } 
			    else
					button.style.color = "#000000";
			    
			    this.div_players.appendChild(button);
			    
			    this.buttons_players[player.name] = button;
			}
		}
		this.div_players.appendChild(this.div_round);

		
		// update player states:
		
		var status_msg = "";
		
		for (var player_name in this.buttons_players) 
		{
			button = this.buttons_players[player_name];
			
			if (button != null)
			{
				if (player_name === state.activePlayerName) 
				{
					if (player_name === my_player.name) {
						button.className = 'playerButtonIamActive';
					} else {
						button.className = 'playerButtonActive';
					}
				}
				else 
				{
					if (player_name === my_player.name) {
						button.className = 'playerButtonIamInactive';
					} else {
						button.className = 'playerButtonInactive';
					}
				}
			}
		}
		
				
		// update round info:
		
		var round = Number(state.gameRound);
		if (round > 0)
			this.div_round.innerHTML = "Runde "+round;
		else
			this.div_round.innerHTML = "";
		
		
		// update status message:
		
		if (state.phase === 'JOIN')
		{
			this.setBackwardButtonActive(false);
			this.setForwardButtonActive(false);

			if (my_player.color === '') 
			{
				this.activateStartButton();
				status_msg = "Drücke \"Start\" wenn alle da sind.";						
			}
			else 
			{
				this.setActionButtonText("Fertig");
				status_msg = "Warte auf Mitspieler.";
			}

		}
		else if (state.phase === 'INIT') 
		{
			this.setActionButtonText("Fertig");
			
			if (game_state.activePlayerName === my_player.name)
				status_msg = "Platziere Armee!";
			else
				status_msg = "Warte auf Platzierung von "+game_state.activePlayerName+"'s Armee";	
		}
		else if (state.phase === 'UPGRADE')
		{
			this.setActionButtonText("Fertig");

			if (game_state.activePlayerName === my_player.name)
				status_msg = "Platziere Nachschub.";
			else
				status_msg = game_state.activePlayerName+" platziert Nachschub.";
		}
		else if (state.phase === 'REDEEM')
		{
			if (game_state.activePlayerName === my_player.name)
			{
				if (my_player.cards.length == 5) {
					this.setActionButtonText("Fertig");
					status_msg = "Löse Gewinnkarten ein.";					
				}
				else {
					this.setActionButtonText("Fertig", false);
					status_msg = "Löse Gewinnkarten ein, verschiebe, greif an, oder drücke Fertig.";					
				}
			}
			else {
				this.setActionButtonText("Fertig");
				status_msg = game_state.activePlayerName+" ist dran.";
			}
		}
		else if (state.phase === 'ATTACK' || state.phase === 'MOVE') 
		{
			if (game_state.activePlayerName === my_player.name)
			{
				if (state.areaAttacked)
					this.setActionButtonText("Angriff", true);
				else if (cnt_reserve == 0)
					this.setActionButtonText("Fertig", false);
				else
					this.setActionButtonText("Fertig");
			}
			else
				this.setActionButtonText("Fertig");
			
			console.log("state.areaAttacked: "+state.areaAttacked);
			if (state.areaAttacked)
			{
				if (game_state.activePlayerName === my_player.name)
					status_msg = "Angriff auf "+game_board_areas[state.areaAttacked].name;
				else
					status_msg = game_state.activePlayerName+" greift "+game_board_areas[state.areaAttacked].name+" an.";
			}
			else
			{
				if (game_state.activePlayerName === my_player.name)
				{
					if (state.phase === 'ATTACK')
						status_msg = "Verschiebe, greif an, oder drücke Fertig.";
					else
						status_msg = "Verschiebe oder drücke Fertig.";
				}
				else
					status_msg = game_state.activePlayerName+" ist dran.";
			}
		}
		else if (state.phase == 'END')
		{
			this.setActionButtonText("Fertig");
			status_msg = state.winner +" hat seine Mission erfüllt:<br>\""+ state.winnerMission+"\"";
		}
		
		return status_msg;
	}
	
}


function updateRedeemButton()
{
	if (menu != null)
	{
		console.log("cards value: "+my_player.cards_value+", phase: "+game_state.phase);
		menu.setRedeemButtonActive(game_state.phase === 'REDEEM' && my_player.cards_value > 0);
	}
}




# Risikio online #


### Build
mvn clean install

### Start Server
java -jar target/rest-service-0.0.1-SNAPSHOT.jar

### Start Client
http://localhost:8080/risiko?name=roland


##Rest Interfaces

###List all running games
GET http://127.0.0.1:8080/game/

return a list of GameOverview

###Create a new game
POST http://127.0.0.1:8080/admin/newGame?name=Spiel_um_die_Weltherrschaft

returns GameOverview

###Join a game
POST http://127.0.0.1:8080/game/joinGame?game=98a5a948-88c4-491d-b261-b3a47d971b12&name=Walter

returns Player

## Data Structures
GameOverview
 {
        "id": "98a5a948-88c4-491d-b261-b3a47d971b12",
        "phase": "NEW",
        "info": "Test1",
        "createdOn": "2020-03-22T15:57:09.458",
        "players": "Roland,Walter"
 }
 
 Player
 {"id":"73d0a6a1-12c2-43af-a98a-e99e2e04bc08","playerName":"Walter"}


## Docker Build

docker build -t rwrocketscience/risiko .
docker run --name risiko -p 8080:8080 rwrocketscience/risiko